<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="{{ asset('front/inner/css/custom.css') }}">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('front/inner/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('front/inner/css/font-awesome.min.css') }}">

  <title>{{ config('app.name', 'Root Planner') }}</title>
</head>

<body>
  <div class="userara">

    <div class="container-fluid">
	<div class="movebut"><a href="{{ url('/login') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i>
 Back</a></div>
      <div class="row">
        <div class="col-lg-6 offset-lg-6">

          <div class="toplogo1"><a href="{{ url('/') }}"><img src="{{ asset('front/inner/img/logo-sprwt-2.png') }}" alt="" /></a></div>
          <div class="resara">
            <h2>{{ __('Reset Password') }}</h2>
			  @include('front.particals.flash-message')	
			<form method="POST" action="{{ route('password.update') }}">
				@csrf
            <div class="row">
				<input type="hidden" name="token" value="{{ $token }}">
              <div class="col-sm-12 mb-3">
                <label>{{ __('Email') }}</label>
                <div class="input-group">
				  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                  <div class="input-group-append">
                    <div class="input-group-text"><i class="fa fa-envelope" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>				
				@if($errors->has('email'))
					<div class="invalid-feedback ml-3">{{ $errors->first('email') }}</div>
				@endif
			</div>
				
				<div class="col-sm-12 mb-3">
                <label>{{ __('Password') }}</label>
                <div class="input-group @error('password') is-invalid @enderror">
				  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                  <div class="input-group-append">
                    <div class="input-group-text"><i class="fa fa-key" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>				
				@if($errors->has('password'))
					<div class="invalid-feedback ml-3">{{ $errors->first('password') }}</div>
				@endif
			</div>
				
				<div class="col-sm-12 mb-3">
                <label>{{ __('Confirm Password') }}</label>
                <div class="input-group @error('password_confirmation') is-invalid @enderror">
				  <input id="password-confirm" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password">
                  <div class="input-group-append">
                    <div class="input-group-text"><i class="fa fa-key" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>				
				@if($errors->has('password_confirmation'))
					<div class="invalid-feedback ml-3">{{ $errors->first('password_confirmation') }}</div>
				@endif
			</div>
              <div class="col-sm-12 butpos"><button type="submit" class="green_btn">{{ __('Reset Password') }}</button></div>

            </div>
			</form>
          </div>



        </div>


      </div>


    </div>

  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="{{ asset('front/inner/js/jquery-3.4.1.slim.min.js') }}"></script>
  <script src="{{ asset('front/inner/js/popper.min.js') }}"></script>
  <script src="{{ asset('front/inner/js/bootstrap.min.js') }}"></script>

</body>

</html>