<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="{{ asset('front/inner/css/custom.css') }}">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('front/inner/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('front/inner/css/font-awesome.min.css') }}">

  <title>{{ config('app.name', 'Root Planner') }}</title>
<!-- Favicon-->
<link rel="shortcut icon" href="{{ asset('admin/img/favicon.jpg') }}">
</head>

<body>
  <div class="userara">

    <div class="container-fluid">
		<div class="movebut"><a href="http://rootplanner.io"><i class="fa fa-arrow-left" aria-hidden="true"></i>
 Back</a></div>
      <div class="row">
        <div class="col-lg-6 offset-lg-6">

          <div class="toplogo"><a href="{{ url('/') }}"><img src="{{ asset('front/inner/img/logo-sprwt-2.png') }}" alt="" /></a></div>
          <div class="resara">
            <h2>{{ __('Login') }}</h2>	
			@include('front.particals.flash-message')
			@if (session('confirmation'))
				<div class="alert alert-info" role="alert">
					{!! session('confirmation') !!}
				</div>
			@endif
			@if ($errors->has('confirmation') > 0 )
				<div class="alert alert-danger" role="alert">
					{!! $errors->first('confirmation') !!}
				</div>
			@endif
			<form method="POST" action="{{ route('login') }}">
				@csrf
            <div class="row">

              <div class="col-sm-12 mb-3">
                <label>{{ __('Email') }}</label>
                <div class="input-group  @error('email') is-invalid @enderror">
				  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
                  <div class="input-group-append">
                    <div class="input-group-text"><i class="fa fa-envelope" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
				@if($errors->has('email'))
					<div class="invalid-feedback ml-3">{{ $errors->first('email') }}</div>
				@endif


              </div>

              <div class="col-sm-12 mb-5">
                <label>{{ __('Password') }}</label>
                <div class="input-group  @error('password') is-invalid @enderror">
				  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password">
                  <div class="input-group-append">
                    <div class="input-group-text"><i class="fa fa-key" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
				@if($errors->has('password'))
					<div class="invalid-feedback ml-3">{{ $errors->first('password') }}</div>
				@endif

              </div>
              <div class="col-sm-6 forgot">
				  @if (Route::has('password.request'))
				  	<a href="{{ route('password.request') }}">{{ __('Forgot password?') }}</a>
				  @endif
			  </div>
              <div class="col-sm-6 butpos"><button type="submit" class="green_btn">{{ __('Login') }}</button></div>

            </div>
			</form>
          </div>



        </div>


      </div>


    </div>

  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="{{ asset('front/inner/js/jquery-3.4.1.slim.min.js') }}"></script>
  <script src="{{ asset('front/inner/js/popper.min.js') }}"></script>
  <script src="{{ asset('front/inner/js/bootstrap.min.js') }}"></script>

</body>

</html>