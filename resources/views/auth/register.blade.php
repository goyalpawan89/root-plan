<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="{{ asset('front/inner/css/custom.css') }}">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('front/inner/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('front/inner/css/font-awesome.min.css') }}">

  <title>{{ config('app.name', 'Root Planner') }}</title>
<!-- Favicon-->
<link rel="shortcut icon" href="{{ asset('admin/img/favicon.jpg') }}">
</head>

<body>
  <div class="userara">

    <div class="container-fluid">
		<div class="movebut"><a href="http://rootplanner.io"><i class="fa fa-arrow-left" aria-hidden="true"></i>
 Back</a></div>
      <div class="row">
        <div class="col-lg-6 offset-lg-6">

          <div class="toplogo"><a href="{{ url('/') }}"><img src="{{ asset('front/inner/img/logo-sprwt-2.png') }}" alt="" /></a></div>
			<form method="POST" action="{{ route('register') }}">
                        @csrf
          <div class="resara">
            <h2>{{ __('Register') }}</h2>
			  @include('front.particals.flash-message')
            <div class="row">
              <div class="col-sm-6 mb-3">
                <label>{{ __('First name') }}</label>
                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" autocomplete="first_name" autofocus>				
				@if($errors->has('first_name'))
					<div class="invalid-feedback ml-3">{{ $errors->first('first_name') }}</div>
				@endif
              </div>
              <div class="col-sm-6 mb-3">
                <label>{{ __('Last name') }}</label>
                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" autocomplete="last_name" autofocus>
				@if($errors->has('last_name'))
					<div class="invalid-feedback ml-3">{{ $errors->first('last_name') }}</div>
				@endif
              </div>
              <div class="col-sm-12 mb-3">
                <label>{{ __('Email') }}</label>
                <div class="input-group  @error('email') is-invalid @enderror">					
					<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email">
                  	<div class="input-group-append">
                    	<div class="input-group-text"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                  	</div>
                </div>				
				@if($errors->has('email'))
					<div class="invalid-feedback ml-3">{{ $errors->first('email') }}</div>
				@endif
              </div>
              <div class="col-sm-12 mb-3">
                <label>{{ __('Phone') }}</label>
                <div class="input-group  @error('phone') is-invalid @enderror">
                  <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" autocomplete="phone">
                  <div class="input-group-append">
                    <div class="input-group-text"><i class="fa fa-phone" aria-hidden="true"></i></div>
                  </div>
                </div>							
				@if($errors->has('phone'))
					<div class="invalid-feedback ml-3">{{ $errors->first('phone') }}</div>
				@endif
              </div>
              <div class="col-sm-12 mb-3">
                <label>{{ __('Password') }}</label>
                <div class="input-group  @error('password') is-invalid @enderror">
                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
                  <div class="input-group-append">
                    <div class="input-group-text"><i class="fa fa-key" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>							
				@if($errors->has('password'))
					<div class="invalid-feedback ml-3">{{ $errors->first('password') }}</div>
				@endif
              </div>
              <div class="col-sm-6 captch">
				  <div class="g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_SITE_KEY')}}"></div>
				  <script src='https://www.google.com/recaptcha/api.js'></script>
				  	<p class="@error('g-recaptcha-response') is-invalid @enderror">This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply.</p>
					@if($errors->has('g-recaptcha-response'))
						<div class="invalid-feedback ml-3">{{ $errors->first('g-recaptcha-response') }}</div>
					@endif
				</div>
              <div class="col-sm-6 butpos"><button type="submit" class="green_btn">Register</button></div>

            </div>
          </div>

			</form>

        </div>


      </div>


    </div>

  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="{{ asset('front/inner/js/jquery-3.4.1.slim.min.js') }}"></script>
  <script src="{{ asset('front/inner/js/popper.min.js') }}"></script>
  <script src="{{ asset('front/inner/js/bootstrap.min.js') }}"></script>
  <script>	
	document.getElementById('phone').addEventListener('input', function (e) {
		var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
		e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
	});
  </script>
</body>

</html>