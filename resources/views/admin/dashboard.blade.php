@extends( 'layouts.app' )
@section( 'content' )
<div class="container-fluid">
 <section class="py-2">
    <div class="row">
      <div class="col-lg-12 mb-4">
         <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="col-sm-8">
                <ul class="nav nav-tabs" id="myTab" role="tablist">				
                  <li class="nav-item" role="presentation"> <a class="nav-link active" id="map-tab" data-toggle="tab" href="#map" role="tab" aria-controls="map" aria-selected="true">Map</a> </li>
                  <li class="nav-item" role="presentation"> <a class="nav-link" id="routes-tab" data-toggle="tab" href="#routes" role="tab" aria-controls="routes" aria-selected="false">Routes</a> </li>
                  <li class="nav-item" role="presentation"> <a class="nav-link" id="orders-tab" data-toggle="tab" href="#orders" role="tab" aria-controls="orders" aria-selected="false">Orders</a> </li>
                </ul>
              </div>
             <!-- <div class="col-sm-4 plansrch">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i> </div>
                  </div>
                  <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Searching ...">
                </div>
              </div>-->
            </div>
          </div>
          <div class="card-body settingtab" style="padding-top:10px !important;">
            <div class="tab-content" id="myTabContent">
			  @include('admin.dashboard.map')
			  @include('admin.dashboard.routes')
			  @include('admin.dashboard.orders')              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@include('admin.dashboard.planroute')
@endsection

@section('footer_scripts')
	@include('admin.dashboard.scripts')
@endsection