@extends( 'layouts.app' )
@section( 'content' )
<div class="container-fluid">
  <section class="py-2">
    <div class="row">
      <div class="col-lg-12 mb-4">        
        <div class="card">
          <div class="card-header">
            <h6 class="mb-0">Add Driver</h6>
          </div>
          <div class="card-body settingtab">
           
           {{Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['admin.drivers.store']])}}
						  <div class="row">
							<div class="col-md-6">
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Name</label>
								<div class="col-md-6">
									<input class="form-control" name="name" type="text" data-validation="required">
								</div>
							  </div>
							  <div class="form-group row d-none">
								<label class="col-md-4 form-control-label">Serial Number</label>
								<div class="col-md-6">
									<input class="form-control" name="serial_no" type="hidden" data-validation="required">
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Email</label>
								<div class="col-md-6">
									<input class="form-control" name="email" type="text" data-validation="required email">
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Phone Number</label>
								<div class="col-md-6">
								  <input class="form-control" name="phone" id="phone" type="text" data-validation="required">
								</div>
							  </div>	 
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Default vehicle</label>
								<div class="col-md-6">
									<input class="form-control" name="vehicle" type="text" data-validation="required">
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Work time</label>
								<div class="col-md-6">
									{!! Form::text('worktime_start', '00:00', ['data-validation' => 'required', 'class' => 'form-control worktime ' . $errors->first('worktime_start', 'is-invalid')]) !!}
									<span class="worktime form-control">-</span>
									{!! Form::text('worktime_end', '00:00', ['data-validation' => 'required', 'class' => 'form-control worktime ' . $errors->first('worktime_end', 'is-invalid')]) !!}
								</div>
							  </div> 	
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Color Theme</label>
								<div class="col-md-6">
									<input name="colortheme" type="color" data-validation="required">
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Status</label>
								<div class="col-md-6">
									{!! Form::select('status_id', $status, 1, ['data-validation' => 'required', 'class' => 'form-control ' . $errors->first('status_id', 'is-invalid')]) !!}
								</div>
							  </div>
							  @if(!empty($customFields))
							  @foreach($customFields as $customfield)	
							  <div class="form-group row d-none">
								<label class="col-md-4 form-control-label">{{$customfield}}</label>
								<div class="col-md-6">
									<input name="customfields[{!! $customfield !!}]" type="text" class="form-control">
								</div>
							  </div>
							  @endforeach
							  @endif
							</div>
							  <div class="col-md-6">								  
							  <div class="form-group row">
							<label class="col-md-4 form-control-label">Driver Image</label>
							<div class="col-md-6">
								<input class="form-control" name="image" type="file">
							</div>
						  </div>								  
							  <div class="form-group row">
							<label class="col-md-4 form-control-label">Fixed cost for driver</label>
							<div class="col-md-6">
								<input class="form-control" name="fix_cost" type="text">
							</div>
						  </div>
							  <div class="form-group row">
							<label class="col-md-4 form-control-label">Cost p/mile</label>
							<div class="col-md-6">
							  <input class="form-control" name="cost_per_mile" type="text">
							</div>
						  </div>
							  <div class="form-group row">
							<label class="col-md-4 form-control-label">Cost p/hour</label>
							<div class="col-md-6">
							 <input class="form-control" name="cost_per_hour" type="text">
							</div>
						  </div>
								  <div class="form-group row">
							<label class="col-md-4 form-control-label">Cost p/hour for overtime</label>
							<div class="col-md-6">
							  <input class="form-control" name="cost_per_hour_overtime" type="text">
							</div>
						  </div>
								  <div class="form-group row">
							<label class="col-md-4 form-control-label">Start Location</label>
							<div class="col-md-6">
							{!! Form::select('start_location', $shubs, '', ['data-validation' => 'required', 'class' => 'form-control ' . $errors->first('start_location', 'is-invalid')]) !!}
							</div>
						  </div>
						  <div class="form-group row">
							<label class="col-md-4 form-control-label">End Location</label>
							<div class="col-md-6">
							 {!! Form::select('end_location', $ehubs, '', ['data-validation' => 'required', 'class' => 'form-control ' . $errors->first('end_location', 'is-invalid')]) !!}
							</div>
						  </div>
						  <div class="form-group row">
							<label class="col-md-4 form-control-label">Home Address</label>
							<div class="col-md-6">
								{!! Form::text('home_address', '', ['data-validation' => 'required', 'id' => 'home_address', 'class' => 'form-control ' . $errors->first('home_address', 'is-invalid')]) !!}
								{!! Form::hidden('home_lat_lng_poly','', ['class' => 'home_lat_lng_poly']) !!}
								{!! Form::hidden('home_lat_lng','', ['class' => 'home_lat_lng']) !!}
							</div>
						  </div>
								  		  
							</div>
							<div class="col-md-12"><div id="map"></div></div>
						  </div>
              <div class="form-group pt-3">
                <button type="submit" class="btn btn-success">Update</button>
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>

    </div>
  </section>
</div>
@endsection

@section( 'footer_scripts' )
<style>
	input.worktime{
		width: 40%;
		float: left;
	}
	span.worktime {
		width: 20%;
		float: left;
		box-shadow: none;
		border: 0;
	}
	#map {
		height: 600px;
	}
</style>
<script type="text/javascript">
	document.getElementById('phone').addEventListener('input', function (e) {
		var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
		e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
	});
	$('[name="worktime_start"], [name="worktime_end"]').datetimepicker({
	  	datepicker:false,
  		format:'H:i',
		step: 15
	});
</script>
<script>
	  var markers = [];
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 37.090240, lng: -95.712891},
          zoom: 13
        });
		
		var input = document.getElementById('home_address');
		var searchBox = new google.maps.places.SearchBox(input);
		google.maps.event.addListener(searchBox, 'places_changed', function() {
			var places = searchBox.getPlaces();
			for (var i = 0, marker; marker = markers[i]; i++) {
			  marker.setMap(null);
			}
			markers = [];
			var bounds = new google.maps.LatLngBounds();
			var place = null;
			var viewport = null;
			for (var i = 0; place = places[i]; i++) {
			  var image = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25)
			  };
			  var marker = new google.maps.Marker({
				map: map,
				title: place.name,
				draggable: true,
				editable: true,
				position: place.geometry.location
			  });			
			  google.maps.event.addListener(marker, 'dragend', function() {
					var addrsLat = marker.getPosition().lat();
					var addrsLng = marker.getPosition().lng();
					var latlng = {};
					latlng.lat = addrsLat;
					latlng.lng = addrsLng;
					var latlangstr = JSON.stringify(latlng, null, 2);
					$('.home_lat_lng').val(latlangstr);
			  });
			  viewport = place.geometry.viewport;
			  markers.push(marker);
			  var addrsLat = marker.getPosition().lat();
			  var addrsLng = marker.getPosition().lng();
				var latlng = {};
				latlng.lat = addrsLat;
				latlng.lng = addrsLng;
				var latlangstr = JSON.stringify(latlng, null, 2);
				$('.home_lat_lng').val(latlangstr);
			  bounds.extend(place.geometry.location);
			}
			map.setCenter(bounds.getCenter());
		});
		  
		var polyOptions = {
          strokeWeight: 0,
          fillOpacity: 0.45,
          editable: true
        };
        var drawingManager = new google.maps.drawing.DrawingManager({
          drawingMode: google.maps.drawing.OverlayType.POLYGON,
          drawingControl: true,
          drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: ['polygon']
          },
          markerOptions: {
            draggable: true,
            editable: true,
          },
          polylineOptions: {
            editable: true
          },
          rectangleOptions: polyOptions,
          circleOptions: polyOptions,
          polygonOptions: polyOptions,
        });
        drawingManager.setMap(map);
		var home_lat_lng = {}; 
		google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {
			home_lat_lng.mtype = e.type;
			home_lat_lng.latlng = [];
			if (e.type == google.maps.drawing.OverlayType.MARKER) {
				var lat = e.overlay.getPosition().lat();
				var lng = e.overlay.getPosition().lng();
				var latlng = {};
				latlng.lat = lat;
				latlng.lng = lng;
				home_lat_lng.latlng.push(latlng);
				//console.log(lat);								
				//console.log(lng);								
			} else if(e.type == google.maps.drawing.OverlayType.POLYGON){
				var coordinatesArray = e.overlay.getPath().getArray();
				coordinatesArray.forEach(function(item, index){
					var lat = item.lat();
					var lng = item.lng();
					var latlng = {};
					latlng.lat = lat;
					latlng.lng = lng;
					home_lat_lng.latlng.push(latlng);
					//console.log(lat);								
					//console.log(lng);								
				})
				//console.log(coordinatesArray);				
			} else if(e.type == google.maps.drawing.OverlayType.CIRCLE){
				var lat = e.overlay.getCenter().lat();
				var lng = e.overlay.getCenter().lng();
				var radius = e.overlay.getRadius();
				var latlng = {};
				latlng.lat = lat;
				latlng.lng = lng;
				latlng.radius = radius;
				home_lat_lng.latlng.push(latlng);
				//console.log(lat);
				//console.log(lng);
				//console.log(radius);
			} /*else if(e.type == google.maps.drawing.OverlayType.RECTANGLE){
				var coordinatesArray = e.overlay.getBounds();
				console.log(coordinatesArray);
			}*/
			//console.log(home_lat_lng);
			var home_lat_lng_str = JSON.stringify(home_lat_lng, null, 2);
			//console.log(home_lat_lng_str);
			$('.home_lat_lng_poly').val(home_lat_lng_str);
        });
      }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&libraries=places,drawing&callback=initMap"></script>
@endsection