@extends('layouts.app')
@section('content')	
<div class="container-fluid">
  <section class="py-2">
    <div class="row">
      <div class="col-lg-12 mb-4">
		
        <div class="card">
          <div class="card-header col-md-12">
            <div class="card-headerT"><h6 class="mb-0">Drivers</h6></div>
			<div class="card-headerB" ><a href="{{route('admin.drivers.create')}}" class="btn btn-info pull-right"><i class="fa fa-plus" aria-hidden="true"></i>
Add Driver</a></div>
          </div>

          <div class="card-body settingtab">
            <div class="tab-pane fade show active settingtab" id="billng" role="tabpanel" aria-labelledby="billng-tab">
							<table class="table card-text table-md" id="users">
								<thead>
									<tr>
										<th>S No</th>
										<th>&nbsp;</th>
										<th>Active</th>
										<!--<th>Serial Number</th>-->
										<th>Name</th>
										<th>Work Hours</th>
										<th>Email</th>
										<th>Default vehicle</th>
										<th>Phone number</th>
										<th>Action</th>
										<!--<th>Meals left to deliver</th>
										<th>Meals delivered</th>
										<th>Live status</th>-->
									</tr>
								</thead>								
						  	</table>
						</div>            
          </div>
        </div>
      </div>

    </div>
  </section>
</div>


@endsection

@section('footer_scripts') 
<style>
	input.worktime{
		width:40%;
		float: left;
	}
	span.worktime {
		width: 20%;
		float: left;
		box-shadow: none;
		border: 0;
	}
</style>
<script>
$(document).ready(function(){
	$('[name="worktime_start"], [name="worktime_end"]').datetimepicker({
	  	datepicker:false,
  		format:'H:i',
		step: 15
	});
    var dataTable = $('#users').DataTable({
		"scrollX": true,
		"dom": '<"float-left"B><"float-right"f>rt<"row pt-3"<"col-sm-3"i><"col-sm-3"l><"col-sm-6"p>>',
        "ajax": {
            url: "{{ url('admin/drivers') }}",
			"dataType": "json",
			"type": "GET",
			"data":{ _token: "{{csrf_token()}}"}
        },
        "processing": true,
        "serverSide": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "columns": [
            { "data": "id" },
            { "data": "colortheme" },
            { "data": "status_id" },
            { "data": "name" },
            { "data": "worktime" },
            { "data": "email" },
            { "data": "vehicle" },
            { "data": "phone" },
            { "data": "options" }
        ],
        columnDefs: [
            { orderable: false, targets: [1,2,3,5,7] } //This part is ok now
        ],
		order: [[ 0, "desc" ]],
		language: {
			paginate: {
			  first: '<i class="fa fa-fw fa-fast-backward">',
			  last: '<i class="fa fa-fw fa-fast-forward">',
			  next: '<i class="fa fa-fw fa-long-arrow-right">',
			  previous: '<i class="fa fa-fw fa-long-arrow-left">'  
			}
		}
    });	           
	dataTable.columns().every( function () {
		var that = this;
		$( 'select', this.header() ).on( 'keyup change', function () {
			if ( that.search() !== this.value ) {
				that
					.search( this.value )
					.draw();
			}
		} );
	} );
	/*$(document).delegate('#users tbody tr', 'click', function() { 
		window.location = $(this).find('.editLink').attr('href');
	});	*/	
	$(document).delegate('.custom-switch .custom-control-input', 'change', function(){	
		var status = ($(this).prop('checked') == true ? 1 : 0);
		$.ajax({
			url: "{{url('admin/status-update')}}",
			type:"POST",
			data:{
				"_token": "{{ csrf_token() }}",
				id:$(this).attr('name'),
				status:status,
				table:'user'
			}
		 });
	});
});
</script>
@endsection