<div class="modal planmod fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content stepara">
      <div class="modal-header">
        <h5 class="modal-title" id="planName">Seed Plan</h5>
        <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget eros efficitur, efficitur urna tincidunt, ultrices dolor. Sed tristique ex et lacus ultricies vestibulum.</p>-->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <!-- MultiStep Form -->
      <div class="row justify-content-center mt-0">
        <div class="col-md-12">
          <div class="col-md-12 mx-0">
            <form id="msform">
              <!-- progressbar -->
              <ul id="progressbar">
                <li class="active" id="step1"><span>1</span></li>
                <li id="step2"><span>2</span></li>
                <li id="step3"><span>3</span></li>
                <!--<li id="step4"><span>4</span></li>-->
              </ul>
              <!-- fieldsets -->
              <fieldset class="stepOne formSteps">
              <div class="form-group mb-2" style="margin-left:218px !important;">
                <label for="">How many drivers you have?<span class="nofdriver"></span></label>
                <br />
                <div class="row">
                  <div class="col-md-2">
                    <div class="qtybox">
                      <div class="input-group">
                        <input type="text" id="nosdriver" name="nosdriver" class="form-control input-number" value="1" min="1" max="{{config('global.max_drivers')}}">
                        <span class="input-group-btn">
                        <button type="button" class="quantity-right-plus " data-type="plus" data-field=""> <i class="fa fa-plus" aria-hidden="true"></i> </button>
                        <button type="button" class="quantity-left-minus "  data-type="minus" data-field=""> <i class="fa fa-minus" aria-hidden="true"></i> </button>
                        </span> </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <input type="range" class="custom-range" min="1" max="{{config('global.max_drivers')}}" value="1">
                  </div>
                </div>
              </div>
              <p class="totalPrice text-center">Total : $<span class="totalPlanAmount">0</span></p>
              <p class="text-center">
                <input type="button" name="next" class="next btn btn-success" value="Next" />
              </p>
              </fieldset>              
              <fieldset class="stepTwo formSteps">
              <div class="row">
                <div class="form-group mb-2 col-md-12">
                  <table class="table  table-borderless table-sm" width="100%">
                    <tr>
                      <th colspan="2"><label for="label">Receipt</label></th>
                    </tr>
                    <tr>
                      <th>Plan</th>
                      <td class="planName">Plan</td>
                    </tr>
                    <tr>
                      <th>Drivers</th>
                      <td class="totalDrivers">Plan</td>
                    </tr>
                    <tr>
                      <th>Modules</th>
                      <td class="allModules">Plan</td>
                    </tr>
                    <tr>
                      <th>Coupons</th>
                      <td class="couponCode"><input type="text" class="form-control" name="couponcode" />
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
              <p class="totalPrice text-center">Total : $<span class="totalPlanAmount">0</span></p>
              <p class="text-center">
                <input type="button" name="previous" class="previous btn btn-primery" value="Previous" />
                <input type="button" name="make_payment" class="next btn btn-success" value="Next" />
              </p>
              </fieldset>
              <fieldset class="stepThree formSteps">
              <div class="row">
                <div class="form-group mb-2 col-md-12">
                  <table class="table table-borderless table-sm">
                    <tr>
                      <th colspan="2"><label for="label">Billing information</label>
                        </thh>
                    </tr>
                    <tr>
                      <th>Street Address</th>
                      <td><input type="text" class="form-control" value="{{is_null(auth()->user()->billing) ? '' : auth()->user()->billing->address}}" name="address" data-validation="required"/></td>
                    </tr>
                    <tr>
                      <th>City</th>
                      <td><input type="text" class="form-control" value="{{is_null(auth()->user()->billing) ? '' : auth()->user()->billing->city}}" name="city" data-validation="required"/></td>
                    </tr>
                    <tr>
                      <th>Zip Code</th>
                      <td><input type="text" class="form-control" value="{{is_null(auth()->user()->billing) ? '' : auth()->user()->billing->postcode}}" name="postcode" data-validation="required"/></td>
                    </tr>
                    <tr>
                      <th>State</th>
                      <td><input type="text" class="form-control" value="{{is_null(auth()->user()->billing) ? '' : auth()->user()->billing->state}}" name="state" data-validation="required"/></td>
                    </tr>
                    <tr>
                      <th>Country</th>
                      <td>
						  {!! Form::select('country', $countries, (is_null(auth()->user()->billing) ? '' : auth()->user()->billing->country_id), ['class' => 'form-control', 'data-validation' => 'required']) !!}
                    </tr>
                  </table>
                </div>
                <div class="col-md-12 mb-3">
                  <div class="sr-combo-inputs-row">
                    <div id="card-element" class="sr-input sr-card-element col-md-12"></div>
                  </div>
                </div>
              </div>
              <p class="totalPrice text-center">Total : $<span class="totalPlanAmount">0</span></p>
              <p class="text-center">
                <input type="hidden" name="planAmt" id="planAmt" value=""/>
                <input type="hidden" name="planId" id="planId" value=""/>
                <input type="hidden" name="planAddons" id="planAddons" value=""/>
                <input type="hidden" name="totalPlanAmount" id="totalPlanAmount" value=""/>
                <input type="button" name="previous" class="previous btn btn-primery" value="Previous" />
                <input type="submit" name="submit" class="btn btn-success" value="Confirm" />
              </p>
              </fieldset>
			  <fieldset class="addonsData stepTwo11111 formSteps">
              <div class="row">
                <label class="col-md-12">Choose the addons you want:</label>
                @foreach($modules as $module)
                <div class="col-md-6">
                  <div class="custom-control custom-checkbox">
                    <input id="customCheck_{{$module->id}}" type="checkbox" name="addons[{{$module->id}}]" class="custom-control-input" value="{{$module->planname}}" addon-id="{{$module->id}}" addon-amount="{{$module->amount}}" addon-isperdriver="{{$module->isperdriver}}">
                    <label for="customCheck_{{$module->id}}" class="custom-control-label">{{$module->planname}}</label>
                  </div>
                </div>
                @endforeach </div>
              <p class="totalPrice text-center">Total : $<span class="totalPlanAmount">0</span></p>
              <p class="text-center">
                <input type="button" name="previous" class="previous btn btn-primery" value="Previous" />
                <input type="button" name="next" class="next btn btn-success" value="Next" />
              </p>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>