<div class="modal planmod fade" tabindex="-1" role="dialog" id="updateSubscriptionModal" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-xl" role="document" style="max-width: 500px!important;"> 
    <div class="modal-content stepara">
      <div class="modal-header">
        <h5 class="modal-title" id="planName">Update Subscription</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <!-- MultiStep Form -->
      <div class="row justify-content-center mt-0">
        <div class="col-md-12">
          <div class="col-md-12 mx-0">
            {{Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['admin.stripe-pay-update']])}}
              <fieldset class="stepFour formSteps">
              <div class="row">                
                <div class="col-md-12 mb-3 mt-3">
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                  </p>
                </div>
				<div class="col-md-12 mb-3 mt-3">
					<p class="text-center">		
						<input type="hidden" id="planIdUpdate" name="planId" value=""/>
						<input type="hidden" id="nosDriverUpdate" name="nosDriver" value=""/>
						<input type="submit" name="submit" class="btn btn-success" value="Update subscription"/>
					</p>
              	</div>          
              </div>          
              </fieldset>
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>