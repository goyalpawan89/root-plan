<style>
#msform fieldset {
    background: white;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
}

#msform fieldset:not(:first-of-type) {
    display: none
}
#progressbar {
    margin: 30px 0;
    overflow: hidden;
    color: #000;
    padding: 0;
    text-align: center;
}
#progressbar li span {
    width: 30px;
    height: 30px;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 50%;
    font-size: 16px;
    line-height: 28px;
	background: #fff;
}
#progressbar li.active span {
    color: #fff;
	border: 1px solid #28a745;
	background: #28a745;
}
#progressbar li {
    list-style-type: none;
    width: 33.33%;
    float: left;
    position: relative
}
#progressbar li:not(:last-child):after {
    top: 15px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-index: 0;
}
#progressbar li.active:not(:last-child):after {
    background-color: #28a745;
}
.sr-combo-inputs-row {
    -webkit-box-shadow: 0 0 0 0.5px rgba(50,50,93,.1), 0 2px 5px 0 rgba(50,50,93,.1), 0 1px 1.5px 0 rgba(0,0,0,.07);
    box-shadow: 0 0 0 0.5px rgba(50,50,93,.1), 0 2px 5px 0 rgba(50,50,93,.1), 0 1px 1.5px 0 rgba(0,0,0,.07);
}
.sr-input.sr-card-element {
    padding-top: 12px;
}
.sr-input, input[type=text] {
    border-radius: var(--radius);
    padding: 5px 12px;
    height: 44px;
    width: 100%;
    -webkit-transition: -webkit-box-shadow .2s ease;
    transition: -webkit-box-shadow .2s ease;
    transition: box-shadow .2s ease;
    transition: box-shadow .2s ease,-webkit-box-shadow .2s ease;
    background: #fff;
    -moz-appearance: none;
    -webkit-appearance: none;
    appearance: none;
}
	.addonsData .custom-checkbox{
		pointer-events: none;
	}
	.planbox.editPlan{
		cursor: pointer;
	}
	
</style>