<div class="addon">
              <h2>SMS Credits </h2>
              <p>Manually purchase additional SMS credits for your delivery notification system or automatically refill your balance with automated deposits.</p>
              <div class="smscredit">
                <h3><span>SMS credits remaining:</span> ${{auth()->user()->userSubscription->sms_credit}} USD</h3>
                <div>
				  {{Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['admin.stripe-pay-credit'], 'class' => 'form-inline'])}}
                    <div class="form-group mb-2">
                      <label for="" >Add credits (in USD)</label>
					  {!! Form::select('creditamount', $credtVals, 10, ['class' => 'form-control ml-2 mr-2', 'data-validation' => 'required']) !!}
                      <button type="submit" class="btn btn-success addCredit">Add Credits Now</button>
                    </div>
                  {{ Form::close() }}
                </div>
				<div>
                  {{Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['admin.stripe-pay-autorc'], 'class' => 'form-inline'])}}
                    <div class="form-group mb-2">
						<div class="custom-control custom-checkbox">
							<input id="auto-status" name="status" value="1" type="checkbox" class="custom-control-input" {{(!is_null(auth()->user()->autorc) && auth()->user()->autorc->status == 1 ? "checked" : '')}}>
							<label for="auto-status" class="custom-control-label mr-2">Auto Recharge</label>
					  	</div>
						{!! Form::select('creditamount', $credtVals, (is_null(auth()->user()->autorc) ? 10 : auth()->user()->autorc->amount), ['class' => 'form-control ml-2 mr-2', 'data-validation' => 'required']) !!}
                      	<label class=" ml-2 mr-2">when balance is below</label>
					  	{!! Form::select('min_balance', $autoOptns, (is_null(auth()->user()->autorc) ? 10 : auth()->user()->autorc->min_balance), ['class' => 'form-control ml-2 mr-2', 'data-validation' => 'required']) !!}
                      	<button class="btn btn-success addAutoRc">SAVE</label>
                    </div>
                  {{ Form::close() }}
                </div>
                <small>* When your funds run low, we will automatically recharge your funds with the amount you specify above.</small> </div>
            </div>