<div class="modal planmod fade" tabindex="-1" role="dialog" id="updateCardModal" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-xl" role="document" style="max-width: 500px!important;"> 
    <div class="modal-content stepara">
      <div class="modal-header">
        <h5 class="modal-title" id="planName">Update Card</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <!-- MultiStep Form -->
      <div class="row justify-content-center mt-0">
        <div class="col-md-12">
          <div class="col-md-12 mx-0">
            <form id="msform-update">
              <fieldset class="stepFour formSteps">
              <div class="row">                
                <div class="col-md-12 mb-3 mt-3">
                  <div class="sr-combo-inputs-row">
                    <div id="card-element-update" class="sr-input sr-card-element col-md-12"></div>
                  </div>
                </div>
				<div class="col-md-12 mb-3 mt-3">
					<p class="text-center">					
						<input type="submit" name="submit" class="btn btn-success" value="Update Card"/>
					</p>
              	</div>          
              </div>          
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>