<div class="addon mb-4">
              <h2>Add drivers</h2>
              <p>Select the amount of drivers you would like to have active in your current subscription.</p>
              <div class="row subshead">
                <div class="col-md-5">
                  <div class="row">
                    <div class="col-md-6">Total drivers </div>
                    <div class="col-md-6">
                      <div class="qtybox">
                        <div class="input-group">
                          <input type="text" id="quantity" name="quantity" class="form-control input-number planEditQty" value="{{auth()->user()->userSubscription->drivers}}" min="1" max="100">
                          <span class="input-group-btn">
                          <button type="button" class="quantity-right-plus " data-type="plus" data-field=""> <i class="fa fa-plus" aria-hidden="true"></i> </button>
                          <button type="button" class="quantity-left-minus "  data-type="minus" data-field=""> <i class="fa fa-minus" aria-hidden="true"></i> </button>
                          </span> </div>
                      </div>
                    </div>
                  </div>
                  <div class="row" style="padding-top:15px;">
                    <div class="col-md-6">Total monthly cost</div>
                    <div class="col-md-6">$<span class="priceByDriver">{{auth()->user()->userSubscription->plan->amount*auth()->user()->userSubscription->drivers}}</span></div>
                  </div>
                </div>
                <div class="col-md-7">
                  <input type="range" class="custom-range planEditQty" min="1" max="1000" value="{{auth()->user()->userSubscription->drivers}}">
                </div>
              </div>
              <button type="button" class="grnbtn mt-3 butpd updateSubscription">Change Subscription</button>
            </div>