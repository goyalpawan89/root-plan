<div class="addon">
  <!--<h2>Manage Cards</h2>
  <p>Manually purchase additional SMS credits for your delivery notification system or automatically refill your balance with automated deposits.</p>-->
  <div class="smscredit">
    <h3><span>Manage Cards</span></h3>
    <div>
      <table class="table table-md table-bordered table-striped">
		  <tbody>
		  	@foreach($cards['data'] as $card)
			  <tr>
				<?php
				  $iconClass = (in_array($card['card']['brand'], $fontawsom) ? "fa fa-cc-".$card['card']['brand'] : "fa fa-credit-card");
				?>
			  	<td><a><i class="{{$iconClass}}"></i></a></td>
			  	<td>**** **** **** {{$card['card']['last4']}}</td>
			  	<td>Expires :{{$card['card']['exp_month']}}/{{$card['card']['exp_year']}}</td>
			  	<td>
					@if($card['id'] != $stripeCustomer['invoice_settings']['default_payment_method'])
					<form action="{{route('admin.stripe-pay-remove-card')}}" method="post">
						<a class="deletecard" card-id="{{$card['id']}}"><i class="fa fa-trash"></i></a>
						<input name="card_id" type="hidden" value="{{$card['id']}}">
						{{ csrf_field() }}
					</form>	
					@else
						Default Card
					@endif
				</td>
			  </tr>
			@endforeach
		  </tbody>
      </table>
    </div>
    <!--<small>* When your funds run low, we will automatically recharge your funds with the amount you specify above.</small>-->
  </div>
</div>