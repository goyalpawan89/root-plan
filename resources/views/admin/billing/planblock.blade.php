<div class="planarea">
              <div class="row">
                <div class="col-lg-4">
                  <div class="planbox {{in_array(auth()->user()->userSubscription->plan_id, [0, $plans[0]->id]) ? 'active' : 'editPlan'}}">
                    <div class="seedplan">
                      <figure><img src="{{ asset('admin/img/icon1.jpg') }}" alt=""/></figure>
                      <h2>{{$plans[0]->planname}}<span>${{floatval($plans[0]->amount)}} per Driver</span></h2>
                    </div>
                    <div class="clearfix"></div>
                    <ul>
                      <li> <i class="fas fa-check"></i> {{$plans[0]->order_limit}} orders</li>
                      <li><i class="fas fa-check"></i> Import Deliveries</li>
                      <li><i class="fas fa-check"></i> Dispatch Routes to App</li>
                      <li><i class="fas fa-check"></i> $20 for SMS Delivery Notification</li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="text-center mt-3 mb-3"> @if(auth()->user()->userSubscription->plan_id > 0)
                      <button type="button" class="brwnbtn updatePlan d-none" plan-id="{{$plans[0]->id}}" plan-name="{{$plans[0]->planname}}" plan-amount="{{floatval($plans[0]->amount)}}" plan-addons="{{$plans[0]->addons}}">Change plan</button>
                      @else
                      <button type="button" class="brwnbtn subscribePlan" plan-id="{{$plans[0]->id}}" plan-name="{{$plans[0]->planname}}" plan-amount="{{floatval($plans[0]->amount)}}" plan-addons="{{$plans[0]->addons}}">Choose plan</button>
                      @endif </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="planbox {{in_array(auth()->user()->userSubscription->plan_id, [0, $plans[1]->id]) ? 'active' : 'editPlan'}}">
                    <div class="rootplan">
                      <figure><img src="{{ asset('admin/img/icon2.jpg') }}" alt=""/></figure>
                      <h2>{{$plans[1]->planname}}<span>${{floatval($plans[1]->amount)}} per Driver</span></h2>
                    </div>
                    <div class="clearfix"></div>
                    <ul>
                      <li> <i class="fas fa-check"></i> {{$plans[1]->order_limit}} orders</li>
                      <li><i class="fas fa-check"></i> Starter plus</li>
                      <li><i class="fas fa-check"></i> Live Delivery Status</li>
                      <li><i class="fas fa-check"></i> Signature Capture</li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="text-center mt-3 mb-3"> @if(auth()->user()->userSubscription->plan_id > 0)
                      <button type="button" class="grnbtn updatePlan d-none" plan-id="{{$plans[1]->id}}" plan-name="{{$plans[1]->planname}}" plan-amount="{{floatval($plans[1]->amount)}}" plan-addons="{{$plans[1]->addons}}">Change plan</button>
                      @else
                      <button type="button" class="grnbtn subscribePlan" plan-id="{{$plans[1]->id}}" plan-name="{{$plans[1]->planname}}" plan-amount="{{floatval($plans[1]->amount)}}" plan-addons="{{$plans[1]->addons}}">Choose plan</button>
                      @endif </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="planbox {{in_array(auth()->user()->userSubscription->plan_id, [0, $plans[2]->id]) ? 'active' : 'editPlan'}}">
                    <div class="sprwtplan">
                      <figure><img src="{{ asset('admin/img/icon3.jpg') }}" alt=""/></figure>
                      <h2>{{$plans[2]->planname}}<span>${{floatval($plans[2]->amount)}} per Driver</span></h2>
                    </div>
                    <div class="clearfix"></div>
                    <ul>
                      <li> <i class="fas fa-check"></i> {{$plans[2]->order_limit}} orders</li>
                      <li><i class="fas fa-check"></i> Base plus</li>
                      <li><i class="fas fa-check"></i> Analytics</li>
                      <li><i class="fas fa-check"></i> Signature Capture</li>
                      <li><i class="fas fa-check"></i> Weekly Orders Planning</li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="text-center mt-3 mb-3"> @if(auth()->user()->userSubscription->plan_id > 0)
                      <button type="button" class="redbtn updatePlan d-none" plan-id="{{$plans[2]->id}}" plan-name="{{$plans[2]->planname}}" plan-amount="{{floatval($plans[2]->amount)}}" plan-addons="{{$plans[2]->addons}}">Change plan</button>
                      @else
                      <button type="button" class="redbtn subscribePlan" plan-id="{{$plans[2]->id}}" plan-name="{{$plans[2]->planname}}" plan-amount="{{floatval($plans[2]->amount)}}" plan-addons="{{$plans[2]->addons}}">Choose plan</button>
                      @endif </div>
                  </div>
                </div>
              </div>
            </div>