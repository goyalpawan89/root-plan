<script>	
	var dataTable = $('#invoices').DataTable({
		"aLengthMenu": [[5, 10, 20, 50, 100], [5, 10, 20, 50, 100]],
    	"pageLength": 5,
		"scrollX": true,
		"dom": '<"float-left"B><"float-right"f>rt<"row pt-3"<"col-sm-3"i><"col-sm-3"l><"col-sm-6"p>>',
        "ajax": {
            url: "{{ url('admin/billing-invoices') }}",
			"dataType": "json",
			"type": "POST",
			"data":{ _token: "{{csrf_token()}}"}
        },
        "processing": true,
        "serverSide": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "columns": [
            { "data": "type" },
            { "data": "date" },
            { "data": "invoice_num" },
            { "data": "amount" },
            { "data": "invoice" }
        ],
        columnDefs: [
            { orderable: false, targets: [2,4] } //This part is ok now
        ],
		order: [[ 1, "desc" ]],
		language: {
			paginate: {
			  first: '<i class="fa fa-fw fa-fast-backward">',
			  last: '<i class="fa fa-fw fa-fast-forward">',
			  next: '<i class="fa fa-fw fa-long-arrow-right">',
			  previous: '<i class="fa fa-fw fa-long-arrow-left">'  
			}
		}
    });	  
	function calculateAmountPlan()
	{
		var planAmt = parseFloat($('#planAmt').val());
		var nosdriver = parseInt($('#nosdriver').val());
		var planAddons = $('#planAddons').val().split(",");
		var driverBasePlan = planAmt;
		var nonDriverPlan = 0;
		//console.log(planAddons);
		$('.addonsData .custom-control-input:checked').each(function(){
			var isDriver = $(this).attr('addon-isperdriver');
			var addOnAmt = parseFloat($(this).attr('addon-amount'));
			var addOnId = $(this).attr('addon-id');
			if($.inArray(addOnId, planAddons) == -1){
				if(isDriver == 1){
				   driverBasePlan = driverBasePlan + addOnAmt;
				} else {
				   nonDriverPlan = nonDriverPlan + addOnAmt;
				}
			}
		});
		var totalPlanAmount = (driverBasePlan*nosdriver) + nonDriverPlan;		
		var checkedVals = $('.custom-control-input:checkbox:checked').map(function() {
			return $(this).siblings('label').text();
		}).get();
		var allMods = checkedVals.join(", ");
		$('.allModules').text(allMods);
		$('.totalPlanAmount').text(totalPlanAmount);
		$('#totalPlanAmount').val(totalPlanAmount);
		$('.totalDrivers').val(nosdriver);
		$('.allModules').val(allMods);
	}
	$(document).ready(function(){
		var current_fs, next_fs, previous_fs; //fieldsets
		var opacity;
		$(".next").click(function(){
			current_fs = $(this).parent().parent();
			next_fs = $(this).parent().parent().next();
			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
			next_fs.show();
			current_fs.animate({opacity: 0}, {
				step: function(now) {
					opacity = 1 - now;
					current_fs.css({
						'display': 'none',
						'position': 'relative'
					});
					next_fs.css({'opacity': opacity});
				},
				duration: 600
			});
		});
		$(".previous").click(function(){
			current_fs = $(this).parent().parent();
			previous_fs = $(this).parent().parent().prev();
			$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
			previous_fs.show();
			current_fs.animate({opacity: 0}, {
				step: function(now) {
					opacity = 1 - now;
					current_fs.css({
						'display': 'none',
						'position': 'relative'
					});
					previous_fs.css({'opacity': opacity});
				},
				duration: 600
			});
		});
		$(document).delegate('.subscribePlan', 'click', function() {
			var planId = $(this).attr('plan-id');
			var planName = $(this).attr('plan-name');
			var planAmt = $(this).attr('plan-amount');
			var planAddons = $(this).attr('plan-addons').split(",");
			$('.addonsData .custom-control-input').prop('checked', false).removeAttr('readonly');
			$.each(planAddons, function(i, addonId){
				$('.addonsData .custom-control-input#customCheck_'+addonId).prop('checked', true).attr('readonly', 'true');
			});
			$('#planAmt').val(planAmt);
			$('#planId').val(planId);
			$('#planAddons').val(planAddons);
			$('#planName, .planName').text(planName);
			$(".bd-example-modal-xl").modal();
			$("#progressbar li").removeClass('active');
			$("#progressbar li#step1").addClass('active');
			$("fieldset.formSteps").css({'display':'none','opacity':0,'position':'relative'});
			$("fieldset.stepOne").css({'display':'block','opacity':1,'position':'relative'});
			calculateAmountPlan();
		});		
		$(document).delegate('.custom-range, .input-number', 'change', function() {
			var numVal = $(this).val();
			$('.custom-range, .input-number').val(numVal);
		});	
		$(document).delegate('#nosdriver, #msform .custom-range', 'change', function() {
			calculateAmountPlan();
		});	
		$(document).delegate('.addonsData .custom-control-input', 'change', function() {
			calculateAmountPlan();
		});		
		$(document).delegate('.quantity-left-minus', 'click', function() {
			var driverVal = parseInt($('.input-number').val());
			if(driverVal > 1){
				driverVal = driverVal - 1;
				$('.input-number').val(driverVal).change();
			}
		});		
		$(document).delegate('.quantity-right-plus', 'click', function() {
			var driverVal = parseInt($('.input-number').val());
			if(driverVal < 1000){
				driverVal = driverVal + 1;
				$('.input-number').val(driverVal).change();
			}
		});			
		$(document).delegate('.planbox.editPlan', 'click', function() {
			$(this).addClass('active').removeClass('editPlan');
			$(".planbox").not(this).removeClass('active').addClass('editPlan');			
			var nosDriver = parseInt($('.planEditQty').val());
			var planCost  = parseFloat($('.planbox.active').find('.updatePlan').attr('plan-amount'));
			var totalCost = nosDriver*planCost;
			$('.priceByDriver').text(totalCost);
		});			
		$(document).delegate('.updateCard', 'click', function() {
			$("#updateCardModal").modal();
		});			
		$(document).delegate('.cancelSubscribe', 'click', function() {
			$("#cancelSubscriptionModal").modal();
		});			
		$(document).delegate('.updateSubscription', 'click', function() {	
			$('#updateSubscriptionModal').modal();
			var currentPlan = $('.planbox.active').find('.updatePlan');
			var planId = currentPlan.attr('plan-id');
			$('#updateSubscriptionModal #planIdUpdate').val(planId);
			$('#updateSubscriptionModal #nosDriverUpdate').val($('#quantity').val());
		});				
		$(document).delegate('.planEditQty', 'change', function() {
			var nosDriver = parseInt($(this).val());
			var planCost  = parseFloat($('.planbox.active').find('.updatePlan').attr('plan-amount'));
			var totalCost = nosDriver*planCost;
			$('.priceByDriver').text(totalCost);
		});	
	});	
	$(document).on("click", ".addCredit", function(e) {
		e.preventDefault();
		var currentForm = $(this).closest('form');
		var credtAmount = currentForm.find('[name="creditamount"]').val();
		bootbox.confirm({
			title: "Purchase additional credits",
			message: "Are you sure you'd like to purchase an additional $"+credtAmount+" credits?",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if(result){
					currentForm.submit();
				}
			}
		});
	});
	$(document).on("click", ".addAutoRc", function(e) {
		e.preventDefault();
		var currentForm = $(this).closest('form');
		bootbox.confirm({
			message: "Are you sure? to add auto credit request",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if(result){
					currentForm.submit();
				}
			}
		});
	});
	$(document).on("click", ".deletecard", function(e) {
		e.preventDefault();
		var currentForm = $(this).closest('form');
		bootbox.confirm({
			message: "Are you sure? to remove this card",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if(result){
					currentForm.submit();
				}
			}
		});
	});
</script>
<script>
var stripe = Stripe("{{env('STRIPE_KEY')}}");
var elements = stripe.elements();
var style = {
  base: {
    color: "#32325d",
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: "antialiased",
    fontSize: "16px",
    "::placeholder": {
      color: "#aab7c4"
    }
  },
  invalid: {
    color: "#fa755a",
    iconColor: "#fa755a"
  },
};
@if(auth()->user()->userSubscription->plan_id == 0)
	var cardElement = elements.create('card', {style: style, hidePostalCode: true});
	cardElement.mount('#card-element');

	var form = document.getElementById('msform');
	form.addEventListener('submit', function(event) {
	  event.preventDefault();
	  //console.log(cardElement);
	  stripe.createPaymentMethod({
		type: 'card',
		card: cardElement,
		billing_details: {
		  email: '{{auth()->user()->email}}',
		  name: '{{auth()->user()->name}}',
		},
	  }).then(stripePaymentMethodHandler);
	});

	function stripePaymentMethodHandler(result) {
  console.log(result);
  if (result.error) {
    console.log(result);
  } else {
    console.log(form);
    fetch('{{url("admin/stripe-pay")}}', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
		_token: "{{csrf_token()}}",
        payment_method_id: result.paymentMethod.id,
		formData: $("#msform").serialize()
      })
    }).then(function(result) {
      // Handle server response (see Step 4)
      result.json().then(function(json) {
		  	alert(json.msg);
        	if(json.status == 'success')
			{
				window.location.reload();
			}
      })
    });
  }
}
@endif
@if(auth()->user()->userSubscription->plan_id > 0)
	var elementsUpdate = stripe.elements();
	var cardElementUpdate = elementsUpdate.create('card', {style: style, hidePostalCode: true});
	cardElementUpdate.mount('#card-element-update');
	var formUpdate = document.getElementById('msform-update');
	formUpdate.addEventListener('submit', function(event) {
	  event.preventDefault();
	  stripe.createPaymentMethod({
		type: 'card',
		card: cardElementUpdate,
		billing_details: {
		  email: '{{auth()->user()->email}}',
		  name: '{{auth()->user()->name}}',
		},
	  }).then(stripePaymentMethodUpdateHandler);
	});

	function stripePaymentMethodUpdateHandler(result) {
  if (result.error) {
    console.log(result);
  } else {
    console.log(formUpdate);
    fetch('{{url("admin/stripe-pay-update-card")}}', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
		_token: "{{csrf_token()}}",
        payment_method_id: result.paymentMethod.id,
		formData: $("#msform-update").serialize()
      })
    }).then(function(result) {
      // Handle server response (see Step 4)
      result.json().then(function(json) {
		  	alert(json.msg);
        	if(json.status == 'success')
			{
				window.location.reload();
			}
      })
    });
  }
}
@endif
</script>