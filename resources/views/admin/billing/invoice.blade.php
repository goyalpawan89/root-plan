<div class="addon">
  <h2>Invoices </h2>
  <p>View your payments with Sprwt Root Planner</p>
  <div class="table-responsive">
	<table class="table  card-text table-md" id="invoices">
	  <thead>
		<tr>
		  <th>Type</th>
		  <th>Date</th>
		  <th>Invoice Number</th>
		  <th>Amount</th>
		  <th>Invoice</th>
		</tr>
	  </thead>
	  <tbody>
		@foreach($phistories as $invoice)
		<tr>
		  <th scope="row">{{$invoice->payment_type}}</th>
		  <td>{{$invoice->created_at->format('d/m/Y')}}</td>
		  <?php
			$invoveData = unserialize($invoice->stripe_invoice);
			if($invoveData['object'] == 'payment_intent'){
				$invoveData = $invoveData['charges']['data'][0];
			}
			//echo '<pre>'; print_r($invoveData); die;
		  ?>
		  <td>{{is_null($invoveData['receipt_number']) ? '-' : $invoveData['receipt_number']}}</td>
		  <td> ${{$invoice->amount/100}}</td>
		  <td class="grntxt"><a target="_blank" href="{{isset($invoveData['hosted_invoice_url']) ? $invoveData['hosted_invoice_url'] : $invoveData['receipt_url']}}">View Invoice</a></td>
		</tr>
		@endforeach
	  </tbody>
	</table>
  </div>
</div>