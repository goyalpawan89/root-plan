@extends( 'layouts.app' )
@section( 'content' )
<div class="container-fluid px-xl-5">
  <section class="py-2">
    <div class="row">

      <div class="col-lg-12 mb-4">        
        <div class="card">
          <div class="card-header">
            <h6 class="mb-0">Edit FAQ</h6>
          </div>

          <div class="card-body settingtab">
           
            {{Form::model($faq, ['method' => 'PUT', 'enctype' => 'multipart/form-data', 'route' => ['superadmin.faqs.update', $faq->id]])}}
			  <div class="row">
			  	<div class="col-md-12">
				  <div class="form-group">
					<label class="form-control-label text-uppercase">Question</label>
					  {!! Form::text('question', old('question'), ['class' => 'form-control ' . $errors->first('question', 'is-invalid')]) !!}
					@if($errors->has('question'))
						<div class="invalid-feedback ml-3">{{ $errors->first('question') }}</div>
					@endif
				  </div>
				</div>
			  </div>
			  <div class="row">
			  	<div class="col-md-6">
				  <div class="form-group">
					<label class="form-control-label text-uppercase">FAQ Type</label>
					{!! Form::select('faqtype', $faqtypes, old('faqtype'), ['class' => 'form-control ' . $errors->first('faqtype', 'is-invalid')]) !!}
					@if($errors->has('faqtype'))
						<div class="invalid-feedback ml-3">{{ $errors->first('faqtype') }}</div>
					@endif
				  </div>				  
				</div>
				  <div class="col-md-6">					  
					  <div class="form-group">
						<label class="form-control-label text-uppercase">Status</label>
					{!! Form::select('status', $statulabels, old('status'), ['class' => 'form-control ' . $errors->first('status', 'is-invalid')]) !!}
					@if($errors->has('status'))
						<div class="invalid-feedback ml-3">{{ $errors->first('status') }}</div>
					@endif
					  </div>
				  </div>
			  </div>
			  <div class="row">
			  	<div class="col-md-12">
				  <div class="form-group">
					<label class="form-control-label text-uppercase">Answer</label>
					  {!! Form::textarea('answer', old('answer'), ['class' => 'form-control ' . $errors->first('answer', 'is-invalid')]) !!}
					@if($errors->has('answer'))
						<div class="invalid-feedback ml-3">{{ $errors->first('answer') }}</div>
					@endif
				  </div>
				</div>
			  </div>
              <div class="form-group">
                <button type="submit" class="btn btn-success">Update</button>
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>

    </div>
  </section>
</div>
@endsection

@section( 'footer_scripts' )
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('answer', {
        filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
</script>
@endsection