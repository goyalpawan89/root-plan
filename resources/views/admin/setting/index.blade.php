@extends( 'layouts.app' )
@section( 'content' )
<div class="container-fluid">
   <section class="py-2">
    <div class="row">
      <div class="col-lg-12 mb-4">
        <!--<div class="tabhed">
          <h2>Plan View</h2>
        </div>-->
        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="col-sm-8">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
				
                  <li class="nav-item">
						<a class="nav-link active" id="billng-tab" data-toggle="tab" href="#billng" role="tab" aria-controls="billng" aria-selected="true">Hub</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" id="config-tab" data-toggle="tab" href="#config" role="tab" aria-controls="config" aria-selected="false">Config</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" id="zones-tab" data-toggle="tab" href="#zones" role="tab" aria-controls="zones" aria-selected="false">Zones</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" id="notification-tab" data-toggle="tab" href="#notification" role="tab" aria-controls="notification" aria-selected="false">Order notification</a>
					  </li>
                </ul>
              </div>
             <!-- <div class="col-sm-4 plansrch">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i> </div>
                  </div>
                  <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Searching ...">
                </div>
              </div>-->
            </div>
          </div>
          <div class="card-body ">
            <div class="tab-content" id="myTabContent">
			
              <div class="tab-pane fade show active" id="billng" role="tabpanel" aria-labelledby="billng-tab">
                <div id="map"></div>
              </div>
              <div class="tab-pane fade" id="config" role="tabpanel" aria-labelledby="config-tab">
			  	{{Form::model($user, ['method' => 'PUT', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal pt-2', 'route' => ['admin.settings.update', $user->id]])}}
			  	<div class="row">
							<div class="col-md-6">
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Company</label>
								<div class="col-md-6">
									{!! Form::text('userDetail[company]', $user->userDetail->company, ['class' => 'form-control']) !!}
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Address</label>
								<div class="col-md-6">
									{!! Form::text('userDetail[address]', $user->userDetail->address, ['class' => 'form-control']) !!}
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">City</label>
								<div class="col-md-6">
									{!! Form::text('userDetail[city]', $user->userDetail->city, ['class' => 'form-control']) !!}
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">State</label>
								<div class="col-md-6">
									{!! Form::text('userDetail[state]', $user->userDetail->state, ['class' => 'form-control']) !!}
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Postal code</label>
								<div class="col-md-6">
								  {!! Form::text('userDetail[postalcode]', $user->userDetail->postalcode, ['class' => 'form-control']) !!}
								</div>
							  </div>	 
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Country</label>
								<div class="col-md-6">
									{!! Form::select('userDetail[country]', $countries, $user->userDetail->country, ['class' => 'form-control']) !!}
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Email Address</label>
								<div class="col-md-6">
									{!! Form::text('user[email]', $user->email, ['class' => 'form-control', 'readonly']) !!}
								</div>
							  </div> 
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Phone</label>
								<div class="col-md-6">
									{!! Form::text('user[phone]', $user->phone, ['id' => 'phone', 'class' => 'form-control']) !!}
								</div>
							  </div> 
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">VAT ID</label>
								<div class="col-md-6">
									{!! Form::text('userDetail[vatid]', $user->userDetail->vatid, ['class' => 'form-control']) !!}
								</div>
							  </div>
							  <div class="form-group row customfield">
								<label class="col-md-4 form-control-label">Custom field #<span class="fieldnum d-none"></span></label>
								<div class="col-md-6">
									<div class="entry input-group col-xs-3">
										<span class="input-group-btn">
											<button class="btn btnsmall btn-success btn-add" type="button">
												<span class="fa fa-plus"></span>
											</button>
										</span>
									</div>
								</div>
							  </div>
							  @if(!empty($user->userDetail->customfields))
								  @foreach($user->userDetail->customfields as $fieldVal)
								  <div class="form-group row customfield">
									<label class="col-md-4 form-control-label">Custom field #<span class="fieldnum"></span></label>
									<div class="col-md-6">
										<div class="entry input-group col-xs-3">
											<input class="form-control" data-validation="required" name="userDetail[customfields][]" type="text" value="{{$fieldVal}}"/>
											<span class="input-group-btn">
												<button class="btn btnsmall btn-danger btn-remove" type="button">
													<span class="fa fa-minus"></span>
												</button>
											</span>
										</div>
									</div>
								  </div>
								  @endforeach							  
							  @endif							  
							  <div class="form-group row hubfield">
								<label class="col-md-4 form-control-label">Hub #<span class="fieldnum d-none"></span></label>
								<div class="col-md-6">
									<span class="input-group-btn">
											<button class="btn btnsmall btn-success btn-add addHubBtn" type="button">
												<span class="fa fa-plus"></span>
											</button>
										</span>
								</div>
							  </div>
							  @if(!empty($addresses))
								  @foreach($addresses as $ky => $address)
								  <div class="form-group row hubfield">
									<label class="col-md-4 form-control-label">Hub #<span class="fieldnum"></span></label>
									<div class="col-md-6">
										<div class="entry input-group col-xs-3">
											<input class="form-control editHub" readonly name="hub[{{$ky}}][name]" type="text" value="{{$address['name']}}"/>
											<input name="hub[{{$ky}}][address]" class="adrs" type="hidden" value="{{$address['address']}}"/>
											<input name="hub[{{$ky}}][latitude]" class="lat" type="hidden" value="{{$address['latitude']}}"/>
											<input name="hub[{{$ky}}][longitude]" class="lng" type="hidden" value="{{$address['longitude']}}"/>
											<span class="input-group-btn">
												<button class="btn btnsmall btn-danger btn-remove" type="button">
													<span class="fa fa-minus"></span>
												</button>
											</span>
										</div>
									</div>
								  </div>
								  @endforeach	
							  @endif
							</div>
							  <div class="col-md-6">								  
								  <div class="form-group row">									
									<div class="col-md-12">
										@if($user->userDetail->companylogo != '')
										<img src="{{ asset('images/customers/logo/'.$user->userDetail->companylogo) }}" alt="{{$user->name}}" style="margin-left: auto; margin-right: auto; width:250px; display: block;" class="img-fluid rounded-circle">
										@else
										<img src="{{ asset('admin/img/sprwt-icon.png') }}" alt="{{$user->name}}" style="margin-left: auto; margin-right: auto; width:250px; display: block;" class="img-fluid rounded-circle">
										@endif
									</div>
								  </div>
							  <div class="form-group row">
							<label class="col-md-4 form-control-label">Company Logo</label>
							<div class="col-md-6">
								{!! Form::file('companylogo', ['class' => 'form-control']) !!}
							</div>
						  </div>
							  <div class="form-group row">
							<label class="col-md-4 form-control-label">Time Zone</label>
							<div class="col-md-6">
							  {!! Form::select('userDetail[timezone]', $zones, $user->userDetail->timezone, ['class' => 'form-control']) !!}
							</div>
						  </div>
							  <div class="form-group row">
							<label class="col-md-4 form-control-label">Current Password</label>
							<div class="col-md-6">
							 {!! Form::password('user[current_password]', ['class' => 'form-control']) !!}
							</div>
						  </div>
								  <div class="form-group row">
							<label class="col-md-4 form-control-label">New Password</label>
							<div class="col-md-6">
							  {!! Form::password('user[password]', ['class' => 'form-control']) !!}
							</div>
						  </div>
								  <div class="form-group row">
							<label class="col-md-4 form-control-label">New Password</label>
							<div class="col-md-6">
							 {!! Form::password('user[confirm-password]', ['class' => 'form-control']) !!}
							</div>
						  </div>
								  		  
							</div>
							<div class="form-group col-md-12">   
								<p>&nbsp;</p>
								<input type="submit" name="submitConfig" value="Update" class="btn btn-success"/>
							</div>
						  </div>
				{{ Form::close() }}						
			  </div>
              <div class="tab-pane fade" id="zones" role="tabpanel" aria-labelledby="zones-tab">
			  	<div class="row">
                  <div class="col-md-12">
                    <p>Zones can be used to define the geographical areas where your drivers are permitted to work or to assign the work within a geographical area to specific drivers.</p>
                      <div class="custom-control custom-radio">
                        <input type="radio" value="1" id="customRadio1" name="zone" class="custom-control-input" {{$user->userDetail->zone == 1 ? 'checked' : ''}}>
                        <label class="custom-control-label" for="customRadio1">Allow drivers that don't have a designated service area to perform work anywhere, including someone else's service area.</label>
                      </div>
                      <div class="zonecheck"> Allow drivers that don't have a designated service area to perform work anywhere, including someone else's service area.
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" name="zone_info" value="1" class="custom-control-input" id="customCheck1" {{$user->userDetail->zone_info == 1 ? 'checked' : ''}}>
                          <label class="custom-control-label" for="customCheck1">Allow drivers that don't have a designated service area to perform work anywhere, including someone else's service area.</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="custom-control custom-radio">
                        <input type="radio" value="2" id="customRadio2" name="zone" class="custom-control-input" {{$user->userDetail->zone == 2 ? 'checked' : ''}}>
                        <label class="custom-control-label" for="customRadio2">Restrict Orders in Area to Driver</label>
                      </div>
                      <div class="zonecheck"> Allow drivers that don't have a designated service area to perform work anywhere, including someone else's service area. </div>
                    </div>
                    <div class="col-md-12">
                      <div class="custom-control custom-radio">
                        <input type="radio" value="3" id="customRadio3" name="zone" class="custom-control-input" {{$user->userDetail->zone == 3 ? 'checked' : ''}}>
                        <label class="custom-control-label" for="customRadio3">Suspend zones</label>
                      </div>
                    </div>
                    <div class="carlocat">
                      <div class="row">
                        <div class="col-sm-4 driverlist">
                          <ul>
                            <li>
                              <figure><img src="{{ asset('admin/img/car1.png') }}" alt=""/></figure>
                              Driver1</li>
                            <li>
                              <figure><img src="{{ asset('admin/img/car2.png') }}" alt=""/></figure>
                              Driver2</li>
                            <li>
                              <figure><img src="{{ asset('admin/img/car3.png') }}" alt=""/></figure>
                              Driver3</li>
                          </ul>
                        </div>
                        <div class="col-sm-8">
                          <div class="carcircle">
                            <div class="cardripos1">
								<img class="img1" src="{{ asset('admin/img/car1.png') }}" alt=""/> 
								<img class="img2" src="{{ asset('admin/img/car2.png') }}" alt=""/> 
								<img class="img3" src="{{ asset('admin/img/car3.png') }}" alt=""/>
							</div>
                            <div class="cardripos2">
								<img class="img1" src="{{ asset('admin/img/car1.png') }}" alt=""/> 
								<img class="img2" src="{{ asset('admin/img/car2.png') }}" alt=""/> 
								<img class="img3" src="{{ asset('admin/img/car3.png') }}" alt=""/>
							</div>
                            <div class="cardripos3">
								<img class="img1" src="{{ asset('admin/img/car1.png') }}" alt=""/> 
								<img class="img2" src="{{ asset('admin/img/car2.png') }}" alt=""/> 
								<img class="img3" src="{{ asset('admin/img/car3.png') }}" alt=""/>
							</div>
                            <div class="cardripos4">
								<img class="img1" src="{{ asset('admin/img/car1.png') }}" alt=""/> 
								<img class="img2" src="{{ asset('admin/img/car2.png') }}" alt=""/> 
								<img class="img3" src="{{ asset('admin/img/car3.png') }}" alt=""/>
							</div>
                            <img src="{{ asset('admin/img/carcircle.png') }}" alt=""/> </div>
                        </div>
                      </div>
                    </div>					                  
               </div>
			  </div>
			  <div class="tab-pane fade" id="notification" role="tabpanel" aria-labelledby="notification-tab">
				<div class="row">
					<div class="col-md-9">
					  @foreach($emailTypes as $etype => $elabel)
					  <div class="custom-control custom-radio custom-control-inline">
						<input id="customRadioInline_{{$etype}}" type="radio" value="{{$etype}}" name="customRadioInline1" class="custom-control-input templatetype" {{$defaultmsg == $etype ? 'checked' : ''}}>
						<label for="customRadioInline_{{$etype}}" class="custom-control-label">{{$elabel}}</label>
					  </div>
					  @endforeach
					</div>
				</div>
				@foreach($emailTypes as $etype => $elabel)
				{{Form::model($user, ['method' => 'PUT', 'enctype' => 'multipart/form-data', 'id' => $etype, 'class' => 'form-horizontal pt-2 d-none', 'route' => ['admin.email-template-update']])}}
			  	<div class="row">
							<div class="col-md-6">
							  <div class="form-group row d-none">
								<label class="col-md-4 form-control-label">Send email as</label>
								<div class="col-md-6">
									{!! Form::hidden('userDetail[templatetype]', $elabel) !!}
									{!! Form::text('userDetail[emailas]', (isset($emailTemplsts[trim($elabel)]) ? $emailTemplsts[trim($elabel)]['emailas'] : ''), ['class' => 'form-control']) !!}
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Email Subject</label>
								<div class="col-md-6">
									{!! Form::text('userDetail[emailsubject]', (isset($emailTemplsts[trim($elabel)]) ? $emailTemplsts[trim($elabel)]['emailsubject'] : ''), ['class' => 'form-control']) !!}
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Email Template</label>
								<div class="col-md-6">									
									{!! Form::textarea('userDetail[emailtemplate]', (isset($emailTemplsts[trim($elabel)]) ? $emailTemplsts[trim($elabel)]['emailtemplate'] : ''), ['class' => 'form-control', 'rows' => 10, 'cols' => 50]) !!}
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Shortkeys</label>
								<div class="col-md-6">
								  <select class="custom-select sortkeys" for="userDetail[emailtemplate]">
								  <option value="1">Select Shortkeys to insert into template message</option>
								  <option value="first_name">First Name</option>
								  <option value="last_name">Last Name</option>
								  <option value="address">Address</option>
								  <option value="time_to_deliver">Time To Deliver</option>
								  <option value="company_email">Company Email</option>
								  <option value="company_number">Company Numbar</option>
								  <option value="full_address">Full Address</option>
								</select>
								</div>
							  </div>
							</div>
							  <div class="col-md-6">								  
							  <div class="form-group row">
							<label class="col-md-4 form-control-label">SMS Template</label>
							<div class="col-md-6">
								{!! Form::textarea('userDetail[smstemplate]', (isset($emailTemplsts[trim($elabel)]) ? $emailTemplsts[trim($elabel)]['smstemplate'] : ''), ['class' => 'form-control', 'rows' => 10, 'cols' => 50]) !!}
							</div>
						  </div>
							  <div class="form-group row">
							<label class="col-md-4 form-control-label">Shortkeys</label>
							<div class="col-md-6">
							   <select class="custom-select sortkeys" for="userDetail[smstemplate]">
								  <option value="1">Select Shortkeys to insert into template message</option>
								  <option value="first_name">First Name</option>
								  <option value="last_name">Last Name</option>
								  <option value="address">Address</option>
								  <option value="time_to_deliver">Time To Deliver</option>
								  <option value="company_email">Company Email</option>
								  <option value="company_number">Company Numbar</option>
								  <option value="full_address">Full Address</option>
								</select>
							</div>
						  </div>
							  
								  		  
							</div>
							<div class="form-group col-md-12">   
								<p>&nbsp;</p>
								<input type="submit" name="submitConfig" value="Update" class="btn btn-success"/>
							</div>
						  </div>
				{{ Form::close() }}	
				@endforeach
			  </div>
			  </div>
			  
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<div class="modal modal-dialog-form" id="addModalPopUp" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
  <div class="modal-dialog modal-dialog-form" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Hub</h5><br>
        <!--<p>Add a driver to your routes. You can then select which drivers are active for each delivery date.</p>-->
      </div>
      <div class="modal-body">
        <div class="tab-pane" id="config" role="tabpanel" aria-labelledby="config-tab">
	
						  <div class="row">
							<div class="col-md-6">
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Name</label>
								<div class="col-md-8">
									<input class="form-control name" id="name" name="name" type="text" data-validation="required">
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Address</label>
								<div class="col-md-8">
									<input class="form-control address" id="address" name="address" type="text" data-validation="required">
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Latitude</label>
								<div class="col-md-8">
									<input class="form-control latitude" name="latitude" type="text" data-validation="required">
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Longitude</label>
								<div class="col-md-8">
									<input class="form-control longitude" name="longitude" type="text" data-validation="required">
								</div>
							  </div>
								
							</div>
							  <div class="col-md-6">
								  <div id="mapHub"></div>
							</div>
							
						  </div>

						</div>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-success addHubData">Save</button>        
      </div>
    </div>
  </div>
</div>
@endsection

@section('footer_scripts') 
<style>
	#map {
		height: 500px;
	}
	#mapHub {
		height: 200px;
	}
	.pac-container {
		background-color: #FFF;
		z-index: 20;
		position: fixed;
		display: inline-block;
		float: left;
	}
	.editHub{
		cursor: pointer;
	}
	.modal{
		z-index: 20;   
	}
	.modal-backdrop{
		z-index: 10;        
	}
	.modal-dialog{ max-width:60% !important; margin: 1.75rem auto; min-height:500px !important;}
</style>
<script type="text/javascript">
	document.getElementById('phone').addEventListener('input', function (e) {
		var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
		e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
	});
	$(document).ready(function(){	
		var fc = {{count($addresses)}};
		$(document).on('click', '.addHubData', function(e)
		{
			var address = $('#addModalPopUp').find('.address').val();
			var name = $('#addModalPopUp').find('.name').val();
			var latitude = $('#addModalPopUp').find('.latitude').val();
			var longitude = $('#addModalPopUp').find('.longitude').val();
			var editPos = $(this).attr('editPos');
			var fieldHtml = '<div class="form-group row hubfield">';
			fieldHtml += '<label class="col-md-4 form-control-label">Hub #<span class="fieldnum"></span></label>';
			fieldHtml += '<div class="col-md-6">';
			fieldHtml += '<div class="entry input-group col-xs-3">';
			fieldHtml += '<input class="form-control adrs editHub" readonly name="hub['+fc+'][name]" value="'+name+'" type="text"/>';
			fieldHtml += '<input name="hub['+fc+'][address]" class="adrs" type="hidden" value="'+address+'" />';
			fieldHtml += '<input name="hub['+fc+'][latitude]" class="lat" type="hidden" value="'+latitude+'" />';
			fieldHtml += '<input name="hub['+fc+'][longitude]" class="lng" type="hidden" value="'+longitude+'" />';
			fieldHtml += '<span class="input-group-btn">';
			fieldHtml += '<button class="btn btnsmall btn-danger btn-remove" type="button">';
			fieldHtml += '<span class="fa fa-minus"></span>';
			fieldHtml += '</button>';
			fieldHtml += '</span>';
			fieldHtml += '</div>';
			fieldHtml += '</div>';
			fieldHtml += '</div>';
			fc = fc + 1;
			if(editPos == 0){
				$('.hubfield:last').after(fieldHtml);
			} else {
				$('.hubfield:eq('+editPos+')').replaceWith(fieldHtml);
			}
			$('#addModalPopUp').modal('hide');
			renumberHubField();
		});
		$(document).on('click', '.editHub', function(e)
		{
			var address = $(this).siblings('.adrs').val();
			var name = $(this).val();
			var latitude = $(this).siblings('.lat').val();
			var longitude = $(this).siblings('.lng').val();
			$('#addModalPopUp').find('.modal-title').text('Edit Hub');
			$('#addModalPopUp').find('.address').val(address);
			$('#addModalPopUp').find('.name').val(name);
			$('#addModalPopUp').find('.latitude').val(latitude);
			$('#addModalPopUp').find('.longitude').val(longitude);
			$('#addModalPopUp').find('.addHubData').attr('editPos', $('.hubfield').index((this).closest('.hubfield')));
			$('#addModalPopUp').modal();
		});
		$(document).on('click', '.addHubBtn', function(e)
		{
			$('#addModalPopUp').find('.name, .address, .latitude, .longitude').val('');
			$('#addModalPopUp').find('.addHubData').attr('editPos', 0);
			$('#addModalPopUp').find('.modal-title').text('Add Hub');
			$('#addModalPopUp').modal();
		});
		$('#addModalPopUp').on('shown.bs.modal', function (e) {
			addHubMapInitilize();
		});
		$(document).on('click', '.customfield .btn-add', function(e)
		{
			if($('.customfield').length < 6){
				var fieldHtml = '<div class="form-group row customfield">';
				fieldHtml += '<label class="col-md-4 form-control-label">Custom field #<span class="fieldnum"></span></label>';
				fieldHtml += '<div class="col-md-6">';
				fieldHtml += '<div class="entry input-group col-xs-3">';
				fieldHtml += '<input class="form-control" data-validation="required" name="userDetail[customfields][]" type="text"/>';
				fieldHtml += '<span class="input-group-btn">';
				fieldHtml += '<button class="btn btnsmall btn-danger btn-remove" type="button">';
				fieldHtml += '<span class="fa fa-minus"></span>';
				fieldHtml += '</button>';
				fieldHtml += '</span>';
				fieldHtml += '</div>';
				fieldHtml += '</div>';
				fieldHtml += '</div>';
				$('.customfield:last').after(fieldHtml);
			}
			renumberCustomField();
		}).on('click', '.btn-remove', function(e)
		{
			$(this).closest('.customfield, .hubfield').remove();
			renumberCustomField();
			renumberHubField();
		});
		renumberCustomField();
		function renumberCustomField()
		{
			var startNum = 0;
			$('.customfield').each(function(){
				$(this).find('.fieldnum').text(startNum);
				startNum = startNum + 1;
			});
			//$('.customfield:last').find('.btn-remove').addClass('btn-add').removeClass('btn-remove').addClass('btn-success').removeClass('btn-danger').html('<span class="fa fa-plus"></span>');
		}
		renumberHubField();
		function renumberHubField()
		{
			var startNum = 0;
			$('.hubfield').each(function(){
				$(this).find('.fieldnum').text(startNum);
				startNum = startNum + 1;
			});
		}
		$('.sortkeys').change(function(){
			var fieldName = $(this).attr('for');
			if($(this).val() != 1){
				var sortKey = '{'+$(this).val()+'}';
				var currentTargetField = $(this).closest('.form-group.row').prev('.form-group.row').find('textarea');
			   	var fieldVal = currentTargetField.val();
				currentTargetField.val(fieldVal+sortKey);
			}
		});
		$('#zones .custom-control-input').change(function(){
			updateDriverPositionImage();
		});
		$('#notification .templatetype').change(function(){
			showTemplateFormAsPerType();
		});
		updateDriverPositionImage();
		showTemplateFormAsPerType();
		function showTemplateFormAsPerType()
		{			
			var checkedForm = $('#notification .templatetype:checked').val();
			$('#notification .form-horizontal').addClass('d-none');
			$('#notification .form-horizontal#'+checkedForm).removeClass('d-none');
		}
		function updateDriverPositionImage()
		{			
			var zone = parseInt($('#zones .custom-control-input[type="radio"]:checked').val());
			var zone_info = parseInt($('#zones .custom-control-input[type="checkbox"]:checked').val());
			if(zone != 1)
			{
			  	$('#zones .custom-control-input[type="checkbox"]').prop('checked', false);
			}
			zone = (isNaN(zone) ? 0 : zone);
			zone_info = (isNaN(zone_info) ? 0 : zone_info);
			$(".img1").attr("src", "{{ asset('admin/img/car1.png') }}");
		   	$(".img2").attr("src", "{{ asset('admin/img/car2.png') }}");
		   	$(".img3").attr("src", "{{ asset('admin/img/car3.png') }}");
			if(zone == 2)
			{
			   $(".cardripos3 .img1").attr("src", "{{ asset('admin/img/bcar1.png') }}");
			   $(".cardripos1 .img2").attr("src", "{{ asset('admin/img/bcar2.png') }}");
			   $(".cardripos1 .img3, .cardripos2 .img3, .cardripos3 .img3").attr("src", "{{ asset('admin/img/bcar3.png') }}");
			}
			else if(zone == 1)
			{
			   $(".cardripos3 .img1, .cardripos4 .img1").attr("src", "{{ asset('admin/img/bcar1.png') }}");
			   $(".cardripos1 .img2, .cardripos4 .img2").attr("src", "{{ asset('admin/img/bcar2.png') }}");
			   $(".cardripos1 .img3, .cardripos2 .img3, .cardripos3 .img3").attr("src", "{{ asset('admin/img/bcar3.png') }}");
			   if(zone_info == 1)
			   {
				   $(".cardripos1 .img1, .cardripos2 .img1").attr("src", "{{ asset('admin/img/car1.png') }}");
				   $(".cardripos2 .img2, .cardripos3 .img2").attr("src", "{{ asset('admin/img/car2.png') }}");
				   $(".cardripos1 .img3, .cardripos2 .img3, .cardripos3 .img3, .cardripos4 .img3").attr("src", "{{ asset('admin/img/car3.png') }}");
				   $(".cardripos3 .img1, .cardripos4 .img1").attr("src", "{{ asset('admin/img/bcar1.png') }}");
				   $(".cardripos1 .img2, .cardripos4 .img2").attr("src", "{{ asset('admin/img/bcar2.png') }}");
			   }
			}
			$.ajax({
				url: "{{url('admin/zone-update')}}",
			  	type:"POST",
			  	data:{
					"_token": "{{ csrf_token() }}",
					user_id:'{{$user->id}}',
					zone:zone,
					zone_info:zone_info,
			  	}
			 });
		}
	});
</script>
<script>	
	function addHubMapInitilize(){
		var map = new google.maps.Map(document.getElementById('mapHub'), {
          center: {lat: 37.090240, lng: -95.712891},
          zoom: 13
        });
		var input = document.getElementById('address');
		var searchBox = new google.maps.places.SearchBox(input);
		var markers = [];
		google.maps.event.addListener(searchBox, 'places_changed', function() {
			var places = searchBox.getPlaces();
			for (var i = 0, marker; marker = markers[i]; i++) {
			  marker.setMap(null);
			}
			markers = [];
			var bounds = new google.maps.LatLngBounds();
			var place = null;
			var viewport = null;
			for (var i = 0; place = places[i]; i++) {
			  var image = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25)
			  };
			  var marker = new google.maps.Marker({
				map: map,
				title: place.name,
				draggable: true,
				editable: true,
				position: place.geometry.location
			  });			
			  google.maps.event.addListener(marker, 'dragend', function() {
					var addrsLat = marker.getPosition().lat();
					var addrsLng = marker.getPosition().lng();
					$('.latitude').val(addrsLat);
					$('.longitude').val(addrsLng);
			  });
			  viewport = place.geometry.viewport;
			  markers.push(marker);
			  var addrsLat = marker.getPosition().lat();
			  var addrsLng = marker.getPosition().lng();
					$('.latitude').val(addrsLat);
					$('.longitude').val(addrsLng);
			  bounds.extend(place.geometry.location);
			}
			map.setCenter(bounds.getCenter());
		});
	}
	function initMap() {
		var myLatLng = {lat: 51.503454, lng: -0.119562};
		var mapSetting = new google.maps.Map(document.getElementById('map'), {
			zoom: 8,
			center: myLatLng
		});
		var marker, i;
		$('.hubfield').each(function(){
			if($(this).find('.lat').length > 0){
				var lat = parseFloat($(this).find('.lat').val());
				var lng = parseFloat($(this).find('.lng').val());
				var title = $(this).find('.adrs').val();
				var markrPos = new google.maps.LatLng(lat,lng);
				marker = new google.maps.Marker({
					position: markrPos,
					title: title,
					map: mapSetting
				});
				mapSetting.panTo(markrPos);
			}
		});
	}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&libraries=places&callback=initMap"></script>
@endsection