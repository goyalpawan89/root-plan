@extends( 'layouts.app' )
@section( 'content' )
<div class="container-fluid px-xl-5">
  <section class="py-2">
    <div class="row">

      <div class="col-lg-12 mb-4">        
        <div class="card">
          <div class="card-header">
            <h6 class="mb-0">Add Customer</h6>
          </div>

          <div class="card-body settingtab">
           
            {{Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['admin.customers.store']])}}
			  <div class="row">
							<div class="col-md-6">							  
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Name</label>
								<div class="col-md-8">
									{!! Form::text('name', old('name'), ['data-validation' => 'required', 'class' => 'form-control ' . $errors->first('name', 'is-invalid')]) !!}
									@if($errors->has('name'))
										<div class="invalid-feedback ml-3">{{ $errors->first('name') }}</div>
									@endif
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Email</label>
								<div class="col-md-8">
									{!! Form::text('email', old('email'), ['data-validation' => 'required email', 'class' => 'form-control ' . $errors->first('email', 'is-invalid')]) !!}
									@if($errors->has('email'))
										<div class="invalid-feedback ml-3">{{ $errors->first('email') }}</div>
									@endif
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Phone</label>
								<div class="col-md-8">
									{!! Form::text('phone', old('phone'), ['id' => 'phone', 'data-validation' => 'required', 'class' => 'form-control ' . $errors->first('phone', 'is-invalid')]) !!}
									@if($errors->has('phone'))
										<div class="invalid-feedback ml-3">{{ $errors->first('phone') }}</div>
									@endif
								</div>
							  </div>
							  <div class="form-group row">
								  <label class="col-md-4 form-control-label">Address</label>
								<div class="col-md-8">
									<button class="btn btn-success" type="button" id="manage_address">Manage Address</button>
								</div>
							  </div>
							</div>
							<div class="col-md-6">																  
							  	<div class="form-group row">
									<label class="col-md-4 form-control-label">Check-in time</label>
									<div class="col-md-8">
										{!! Form::text('check_in_time', old('check_in_time'), ['data-validation' => 'required', 'class' => 'form-control ' . $errors->first('check_in_time', 'is-invalid')]) !!}
										@if($errors->has('check_in_time'))
											<div class="invalid-feedback ml-3">{{ $errors->first('check_in_time') }}</div>
										@endif
									</div>
						  		</div>
							  	<div class="form-group row">
									<label class="col-md-4 form-control-label">Gate Code</label>
									<div class="col-md-8">
										{!! Form::text('gatecode', old('gatecode'), ['data-validation' => 'required', 'class' => 'form-control ' . $errors->first('gatecode', 'is-invalid')]) !!}
										@if($errors->has('gatecode'))
											<div class="invalid-feedback ml-3">{{ $errors->first('gatecode') }}</div>
										@endif
									</div>
							  	</div>	 
							  	<div class="form-group row">
									<label class="col-md-4 form-control-label">Default vehicle</label>
									<div class="col-md-8">
										{!! Form::text('defaultvehicle', old('defaultvehicle'), ['data-validation' => 'required', 'class' => 'form-control ' . $errors->first('defaultvehicle', 'is-invalid')]) !!}
										@if($errors->has('defaultvehicle'))
											<div class="invalid-feedback ml-3">{{ $errors->first('defaultvehicle') }}</div>
										@endif
									</div>
							  	</div>
						  		<div class="form-group row">
									<label class="col-md-4 form-control-label">Delivery notes</label>
									<div class="col-md-8">
										{!! Form::text('notes', old('notes'), ['data-validation' => 'required', 'class' => 'form-control ' . $errors->first('notes', 'is-invalid')]) !!}
										@if($errors->has('notes'))
											<div class="invalid-feedback ml-3">{{ $errors->first('notes') }}</div>
										@endif
									</div>
							  	</div>  
								<div class="avaiblaityDay d-none">
								
								</div>
							</div>
							<!--<div class="col-md-12"><div id="map"></div></div>-->
						  </div>
              <div class="form-group">
                <button type="submit" class="btn btn-success">Save</button>
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>

    </div>
  </section>
</div>
<div class="modal modal-dialog-form" id="addModalPopUp" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
  <div class="modal-dialog modal-dialog-form" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Address</h5>
      </div>
      <div class="modal-body">
        <div class="tab-pane" id="config" role="tabpanel" aria-labelledby="config-tab">
	
						  <div class="row">
							<div class="col-md-6">
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Name</label>
								<div class="col-md-8">
									<input class="form-control name" id="name" name="name" type="text" data-validation="required">
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Address</label>
								<div class="col-md-8">
									<input class="form-control address" id="address" name="address" type="text" data-validation="required">
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Latitude</label>
								<div class="col-md-8">
									<input class="form-control latitude" name="latitude" type="text" data-validation="required">
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Longitude</label>
								<div class="col-md-8">
									<input class="form-control longitude" name="longitude" type="text" data-validation="required">
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">&nbsp;</label>
								<div class="col-md-8">
									<button class="btn btn-success" id="addAddressByData">Add</button>
								</div>
							  </div>								
							</div>
							  <div class="col-md-6">
								  <div id="mapHub"></div>
							</div>
							  <div class="col-md-12">
							  		<table class="table table-bordered table-striped table-sm" id="addressTable">
								  		<thead>
											<tr>
												<td>Default</td>
												<td>Name</td>
												<td>Address</td>
												<td>Sunday</td>
												<td>Monday</td>
												<td>Tuesday</td>
												<td>Wednesday</td>
												<td>Thursday</td>
												<td>Friday</td>
												<td>Saturday</td>
												<td>&nbsp;</td>
											</tr>
										</thead>
										<tbody>
										
										</tbody>
								  	</table>
							  </div>
							
						  </div>

						</div>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-success addHubData">Save</button>        
      </div>
    </div>
  </div>
</div>
@endsection

@section( 'footer_scripts' )
<style>
	#mapHub {
		height: 200px;
	}
	.pac-container {
		background-color: #FFF;
		z-index: 20;
		position: fixed;
		display: inline-block;
		float: left;
	}
	.modal{
		z-index: 20;   
	}
	.modal-backdrop{
		z-index: 10;        
	}
	.modal-dialog{ max-width:60% !important; margin: 1.75rem auto; min-height:500px !important;}
	.deleteAddress{cursor: pointer;}
</style>
<script>
	document.getElementById('phone').addEventListener('input', function (e) {
		var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
		e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
	});
	var ipos = 1;
	$('#addAddressByData').click(function(){
		ipos = ipos + 1;
		var addresName = $('#name').val();
		var addresPos = $('#address').val();
		var latitudeVal = $('.latitude').val();
		var longitudeVal = $('.longitude').val();
		$('#addAddressByData').siblings('.text-danger').remove();
		if(addresName != '' && addresPos != '' && latitudeVal != '' && longitudeVal != ''){
			var tableHtml = '<tr>';
			tableHtml += '<td><div class="custom-control custom-radio custom-control-inline">';
			tableHtml += '<input id="is_default_'+ipos+'" type="radio" name="is_default" value="'+ipos+'" class="custom-control-input">';
			tableHtml += '<label for="is_default_'+ipos+'" class="custom-control-label">&nbsp;</label>';
			tableHtml += '</div></td>';
			tableHtml += '<td>'+addresName+'</td>';		
			tableHtml += '<td>'+addresPos+'</td>';	
			tableHtml += '<td><div class="custom-control custom-radio custom-control-inline">';
			tableHtml += '<input id="sunday_'+ipos+'" type="radio" name="sunday" value="'+ipos+'" class="custom-control-input">';
			tableHtml += '<label for="sunday_'+ipos+'" class="custom-control-label">&nbsp;</label>';
			tableHtml += '</div></td>';	
			tableHtml += '<td><div class="custom-control custom-radio custom-control-inline">';
			tableHtml += '<input id="monday'+ipos+'" type="radio" name="monday" value="'+ipos+'" class="custom-control-input">';
			tableHtml += '<label for="monday'+ipos+'" class="custom-control-label">&nbsp;</label>';
			tableHtml += '</div></td>';	
			tableHtml += '<td><div class="custom-control custom-radio custom-control-inline">';
			tableHtml += '<input id="tuesday'+ipos+'" type="radio" name="tuesday" value="'+ipos+'" class="custom-control-input">';
			tableHtml += '<label for="tuesday'+ipos+'" class="custom-control-label">&nbsp;</label>';
			tableHtml += '</div></td>';	
			tableHtml += '<td><div class="custom-control custom-radio custom-control-inline">';
			tableHtml += '<input id="wednesday'+ipos+'" type="radio" name="wednesday" value="'+ipos+'" class="custom-control-input">';
			tableHtml += '<label for="wednesday'+ipos+'" class="custom-control-label">&nbsp;</label>';
			tableHtml += '</div></td>';	
			tableHtml += '<td><div class="custom-control custom-radio custom-control-inline">';
			tableHtml += '<input id="thursday'+ipos+'" type="radio" name="thursday" value="'+ipos+'" class="custom-control-input">';
			tableHtml += '<label for="thursday'+ipos+'" class="custom-control-label">&nbsp;</label>';
			tableHtml += '</div></td>';	
			tableHtml += '<td><div class="custom-control custom-radio custom-control-inline">';
			tableHtml += '<input id="friday'+ipos+'" type="radio" name="friday" value="'+ipos+'" class="custom-control-input">';
			tableHtml += '<label for="friday'+ipos+'" class="custom-control-label">&nbsp;</label>';
			tableHtml += '</div></td>';	
			tableHtml += '<td><div class="custom-control custom-radio custom-control-inline">';
			tableHtml += '<input id="saturday'+ipos+'" type="radio" name="saturday" value="'+ipos+'" class="custom-control-input">';
			tableHtml += '<label for="saturday'+ipos+'" class="custom-control-label">&nbsp;</label>';
			tableHtml += '</div></td>';
			tableHtml += '<td><span class="deleteAddress"><i class="fa fa-trash" aria-hidden="true"></i></span>';	
			tableHtml += '<input type="hidden" name="aname['+ipos+']" value="'+addresName+'"/>';	
			tableHtml += '<input type="hidden" name="address['+ipos+']" value="'+addresPos+'"/>';	
			tableHtml += '<input type="hidden" name="latitude['+ipos+']" value="'+latitudeVal+'"/>';	
			tableHtml += '<input type="hidden" name="longitude['+ipos+']" value="'+longitudeVal+'"/>';	
			tableHtml += '</td></tr>';
			$('#addressTable tbody').append(tableHtml);
			$('#name, #address, .latitude, .longitude').val('');
		} else {
			$('#addAddressByData').before('<p class="text-danger">Please fill all details of address</p>');
		}
	});
	$('#manage_address').click(function(){
		$('#addModalPopUp').modal();
	});
	$('#addModalPopUp').on('shown.bs.modal', function (e) {
		$('#addModalPopUp').find('.address, .latitude, .longitude').val('');
		addHubMapInitilize();
	});
	$(document).delegate('.addHubData', 'click', function (e) {
		$('.avaiblaityDay').html($('#addressTable tbody').clone()).find('input').removeAttr('id');
		$('#addModalPopUp').modal('hide');
	});
	$(document).delegate('.deleteAddress', 'click', function (e) {
		$(this).closest('tr').remove();
	});
	function addHubMapInitilize(){
		var map = new google.maps.Map(document.getElementById('mapHub'), {
          center: {lat: 37.090240, lng: -95.712891},
          zoom: 13
        });
		var input = document.getElementById('address');
		var searchBox = new google.maps.places.SearchBox(input);
		var markers = [];
		google.maps.event.addListener(searchBox, 'places_changed', function() {
			var places = searchBox.getPlaces();
			for (var i = 0, marker; marker = markers[i]; i++) {
			  marker.setMap(null);
			}
			markers = [];
			var bounds = new google.maps.LatLngBounds();
			var place = null;
			var viewport = null;
			for (var i = 0; place = places[i]; i++) {
			  var image = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25)
			  };
			  var marker = new google.maps.Marker({
				map: map,
				title: place.name,
				draggable: true,
				editable: true,
				position: place.geometry.location
			  });			
			  google.maps.event.addListener(marker, 'dragend', function() {
					var addrsLat = marker.getPosition().lat();
					var addrsLng = marker.getPosition().lng();
					$('.latitude').val(addrsLat);
					$('.longitude').val(addrsLng);
			  });
			  viewport = place.geometry.viewport;
			  markers.push(marker);
			  var addrsLat = marker.getPosition().lat();
			  var addrsLng = marker.getPosition().lng();
					$('.latitude').val(addrsLat);
					$('.longitude').val(addrsLng);
			  bounds.extend(place.geometry.location);
			}
			map.setCenter(bounds.getCenter());
		});
	}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&libraries=places"></script>
@endsection