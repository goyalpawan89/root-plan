@extends('layouts.app')
@section('content')	
<div class="container-fluid px-xl-5">
  <section class="py-2">
    <div class="row">

      <div class="col-lg-12 mb-4">
        <!--<div class="tabhed">
          <h2>FAQ</h2>
        </div>-->
		
        <div class="card">
          <div class="card-header col-md-12">
            <div class="card-headerT"><h6 class="mb-0">Customers</h6></div>
			<div class="card-headerB" ><a href="{{route('admin.customers.create')}}" class="btn btn-info pull-right"><i class="fa fa-plus" aria-hidden="true"></i>
Add Customer</a></div>
          </div>

          <div class="card-body settingtab">
            <table class="table card-text table-md" id="users">
              <thead>
					<tr>
						<th>S No</th>
						<th>Active</th>
						<th>Name</th>
						<th>Address</th>
						<th>Delivery Notes</th>
						<th>Gate Code</th>
						<th>Action</th>
					</tr>
				</thead>
								             
            </table>
          </div>
        </div>
      </div>

    </div>
  </section>
</div>


@endsection

@section('footer_scripts') 
<script>
$(document).ready(function(){
    var dataTable = $('#users').DataTable({
		"scrollX": true,
		"dom": '<"float-left"B><"float-right"f>rt<"row pt-3"<"col-sm-3"i><"col-sm-3"l><"col-sm-6"p>>',
        "ajax": {
            url: "{{ url('admin/customers') }}",
			"dataType": "json",
			"type": "GET",
			"data":{ _token: "{{csrf_token()}}"}
        },
        "processing": true,
        "serverSide": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "columns": [
            { "data": "id" },
            { "data": "status" },
            { "data": "name" },
            { "data": "address" },
            { "data": "notes" },
            { "data": "gatecode" },
            { "data": "created_at" }
        ],
        columnDefs: [
            { orderable: false, targets: [3, 6] } //This part is ok now
        ],
		order: [[ 0, "desc" ]],
		language: {
			paginate: {
			  first: '<i class="fa fa-fw fa-fast-backward">',
			  last: '<i class="fa fa-fw fa-fast-forward">',
			  next: '<i class="fa fa-fw fa-long-arrow-right">',
			  previous: '<i class="fa fa-fw fa-long-arrow-left">'  
			}
		}
    });	           
	dataTable.columns().every( function () {
		var that = this;
		$( 'select', this.header() ).on( 'keyup change', function () {
			if ( that.search() !== this.value ) {
				that
					.search( this.value )
					.draw();
			}
		} );
	} );
	$(document).delegate('.custom-switch .custom-control-input', 'change', function(){	
		var status = ($(this).prop('checked') == true ? 1 : 0);
		$.ajax({
			url: "{{url('admin/status-update')}}",
			type:"POST",
			data:{
				"_token": "{{ csrf_token() }}",
				id:$(this).attr('name'),
				status:status,
				table:'location'
			}
		 });
	});
});
</script>
@endsection