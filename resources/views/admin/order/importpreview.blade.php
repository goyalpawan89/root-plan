@if(!empty($orders))
<div class="table-responsive" style="max-height:400px;">
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				@foreach($headings as $heading)
				<th style="min-width:150px;">
					{!! Form::select('titleRow['.$heading.']', $haddingOptions, (in_array($heading, $haddingOptions) ? $heading : 'Ignore'), ['data-validation' => 'required', 'class' => 'form-control']) !!}
				</th>
				@endforeach
			</tr>
			<tr>
				@foreach($headings as $heading)
				<th>
					{!! $heading !!}
				</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@foreach($orders as $order)
			<tr>
				@foreach($headings as $heading)
				<td>
					{!! $order[$heading] !!}
				</td>
				@endforeach
			</tr>
			@endforeach		
		</tbody>
	</table>
</div>
<div class="form-group">
	<button type="submit" class="btn btn-success">Import Data</button>
</div>
@else
	<table>
		<tbody>
			<tr>
				<td>Nothing To Import. Try Again With Another File</td>
			</tr>
		</tbody>
	</table>
@endif