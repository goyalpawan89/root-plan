@extends('layouts.app')
@section('content')	

<div class="container-fluid px-xl-5">
  <section class="py-2">
    <div class="row">

      <div class="col-lg-12 mb-4">
        <div class="card">         
          <div class="card-body">
              <div class="row">
				  <div class="col-md-2">
					@if($user->driver->image != '')
				  		<img src="{{ asset('images/driver/logo/'.$user->driver->image) }}" class="img-fluid"/>
					@else
						<img src="{{ asset('admin/img/driver.png') }}" class="img-fluid"/>
					@endif
				  </div>
				  <div class="col-md-8">
					<div>
						<div class="card-headerT"><h4>{{$user->name}}</h4></div>
						<div class="card-headerB grntxt" style="text-align:right; font-size:25px; font-weight:bold;">On route <i class="fa fa-rss" aria-hidden="true"></i></div>
					  </div>
					  <div class="clearfix"></div>
					  <div class="row">
							<div class="col-md-3">
						  <div><strong>Email</strong></div>
						  <div class="clearfix"></div>
						  <div>{{$user->email}}</div>
						  </div>
						  <div class="col-md-3">
						  <div><strong>Phone Number</strong></div>
						  <div class="clearfix"></div>
						  <div>{{$user->phone}}</div>
						  </div>
						  
						  <div class="col-md-2">
						  <div><strong>Work hours</strong></div>
						  <div class="clearfix"></div>
						  <div>{{is_null($user->driver) ? 'N/A' : $user->driver->worktime}}</div>
						  </div>
						  
						  <div class="col-md-2">
						   <div><strong>Meals left to deliver</strong></div>
						  <div class="clearfix"></div>
						  <div>N/A</div>
						  </div>
						  
						  <div class="col-md-2">
						   <div><strong>Meals delivered</strong></div>
						  <div class="clearfix"></div>
						  <div>N/A</div>						  
						  </div>
					  </div>
					  <div class="mt-3">&nbsp;</div>
					  <div class="row">
					  <div class="col-md-3">
						  <div><strong>Car Model</strong></div>
						  <div class="clearfix"></div>
						  <div>N/A</div>
						  </div>
						  <div class="col-md-3">
						  <div><strong>Car number</strong></div>
						  <div class="clearfix"></div>
						  <div>N/A</div>
						  
						  </div>
						  <div class="col-md-2">
						  <div><strong>Total order time</strong></div>
						  <div class="clearfix"></div>
						  <div>N/A</div>
						  </div>
						  <div class="col-md-2">
						  <div><strong>Total order miles</strong></div>
						  <div class="clearfix"></div>
						  <div>N/A</div>
						  </div>						 
					  </div>
					  <div class="mt-3"></div>					
				  </div>
				  <div class="col-md-2">
				  	<img src="{{ asset('admin/img/gLight.png') }}" class="img-fluid"/>
				  </div>
			  </div>
          </div>
        </div>		
      </div>
	  <div class="col-lg-12 mb-4">
        <div class="card">
          <div class="card-body">
              <div class="row">
					<ul class="nav nav-tabs col-lg-12" id="myTab" role="tablist">
					  <li class="nav-item">
						<a class="nav-link active" id="billng-tab" data-toggle="tab" href="#billng" role="tab" aria-controls="billng" aria-selected="true">Routes</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" id="config-tab" data-toggle="tab" href="#config" role="tab" aria-controls="config" aria-selected="false">Map</a>
					  </li>
					</ul>
					 <div class="form-group col-md-12 mt-2 mb-4"> 	
								<button type="button" name="submitConfig" value="" class="btn btn-success"><i class="fas fa-file-export"></i>&nbsp;Export routes</button>
					</div>
					<div class="tab-content col-lg-12" id="myTabContent">
					  <div class="tab-pane fade show active settingtab" id="billng" role="tabpanel" aria-labelledby="billng-tab">
							<table class="table card-text" id="users">
								<thead>
									<tr>
										<th>S No</th>
										<th>Order ID</th>
										<th>Priority</th>
										<th>Scheduled at</th>
										<th>Service start</th>
										<th>Service end</th>
										<th>Actual duration</th>
										<th>Location</th>
										<th>Driver</th>
										<th>Gate Code</th>
										<th>Stoup Number</th>
										<th>Address</th>
									</tr>
								</thead>								
						  	</table>
						</div>
					  <div class="tab-pane fade" id="config" role="tabpanel" aria-labelledby="config-tab">
							<img src="{{ asset('admin/img/Light.png') }}" height="400" alt="">
  
						</div>
					</div>
			  </div>
          </div>
        </div>		
      </div>
    </div>
  </section>
</div>
<!-- The Modal -->

@endsection

@section('footer_scripts') 
<script>
$(document).ready(function(){
    var dataTable = $('#users').DataTable({
		"responsive": true,
        "ajax": {
            url: "{{ url('admin/driver-orders') }}",
			"dataType": "json",
			"type": "GET",
			"data":{ driver_id: '{{$user->id}}', _token: "{{csrf_token()}}"}
        },
        "processing": true,
        "serverSide": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "columns": [
            { "data": "sno" },
            { "data": "id" },
            { "data": "priority" },
            { "data": "schduled" },
            { "data": "service_start" },
            { "data": "service_end" },
            { "data": "duration" },
            { "data": "location_id" },
            { "data": "name" },
            { "data": "gatecode" },
            { "data": "stoup_num" },
            { "data": "address" }
        ],
        columnDefs: [
            { orderable: false, targets: [0,6,7,8,9,10] } //This part is ok now
        ],
		order: [[ 1, "desc" ]],
		language: {
			paginate: {
			  first: '<i class="fa fa-fw fa-fast-backward">',
			  last: '<i class="fa fa-fw fa-fast-forward">',
			  next: '<i class="fa fa-fw fa-long-arrow-right">',
			  previous: '<i class="fa fa-fw fa-long-arrow-left">'  
			}
		}
    });	           
	dataTable.columns().every( function () {
		var that = this;
		$( 'select', this.header() ).on( 'keyup change', function () {
			if ( that.search() !== this.value ) {
				that
					.search( this.value )
					.draw();
			}
		} );
	} );
});
</script>
@endsection