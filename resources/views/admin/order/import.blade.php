@extends( 'layouts.app' )
@section( 'content' )
<div class="container-fluid px-xl-5">
  <section class="py-2">
    <div class="row">
      <div class="col-lg-12 mb-4">        
        <div class="card">
          <div class="card-header">
            <h6 class="mb-0">Import Order</h6>
          </div>
          <div class="card-body settingtab">
           
           {{Form::open(['method' => 'POST', 'id' => 'importFormData', 'enctype' => 'multipart/form-data', 'route' => ['admin.orders.import.submit']])}}
						  <div class="row">
							<div class="col-md-6">
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Order Date</label>
								<div class="col-md-8">
									{!! Form::text('orderdate', old('orderdate'), ['id' => 'orderdate', 'readonly' => 'readonly', 'data-validation' => 'required', 'class' => 'form-control ' . $errors->first('orderdate', 'is-invalid')]) !!}
									@if($errors->has('orderdate'))
										<div class="invalid-feedback ml-3">{{ $errors->first('orderdate') }}</div>
									@endif
								</div>
							  </div>
							</div>
							<div class="col-md-6">
							  <div class="form-group row">
								<label class="col-md-4 form-control-label">Excel File</label>
								<div class="col-md-8">
									{!! Form::file('import', ['id' => 'import', 'data-validation' => 'required', 'data-validation-allowing' => 'xlsx, xls', 'class' => 'form-control ' . $errors->first('import', 'is-invalid')]) !!}
									@if($errors->has('import'))
										<div class="invalid-feedback ml-3">{{ $errors->first('import') }}</div>
									@endif
								</div>
							  </div>
							</div>
						  </div>
			  			  <div class="row">
							  <div class="col-md-12" id="excelData">
							  
							  </div>
			  			  </div>
			  	
              <div class="form-group">
                <button type="button" class="btn btn-success" id="previewXlsData">Preview</button>
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
	  @if(!empty($rowStatuses))
	  <div class="col-lg-12 mb-4">        
        <div class="card">
          <div class="card-header">
            <h6 class="mb-0">Import Order Status</h6>
          </div>
          <div class="card-body settingtab">           
           	<table  class="table table-bordered table-striped">
			  	<thead>
					<tr>
						<th>S NO</th>
						<th>Address</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<?php $sno = 0; ?>
					@foreach($rowStatuses as $row)
						<tr>
							<td>{{++$sno}}</td>
							<td>{{$row['address']}}</td>
							<td>{{(isset($row['status']) && $row['status'] == 1) ? 'Imported' : 'Not Importad'}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
          </div>
        </div>
      </div>
	  @endif	
    </div>
  </section>
</div>
@endsection

@section( 'footer_scripts' )
<script>
	$('#orderdate').datetimepicker({
  		format:'M d, Y',
		timepicker:false,
		step: 15
	});
	$('#previewXlsData').click(function(event) {
		event.preventDefault();
		var formData = new FormData($('form#importFormData')[0]);
		$('#excelData').html('');
		$.ajax({
			url: "{{ route('admin.orders.import.submit') }}",
			type: 'POST',              
			data: formData,
			processData: false,
    		contentType: false,
			success: function(result)
			{
				$('#excelData').html(result);
				//console.log(result);
				//location.reload();
			},
			error: function(data)
			{
				console.log(data);
			}
		});
	});
</script>
@endsection