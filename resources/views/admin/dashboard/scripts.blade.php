<style>
	#mapData {
		height: 500px;
	}
	.drivers-lst{
		cursor: pointer;
	}
</style>	
<script>
	$(window).on("load",function(){
		$(".driverListBlock").mCustomScrollbar({
			theme:"dark-thin"
		});
	});
	$('#orderDateForPlanRoute').datetimepicker({
  		format:'M d, Y',
		timepicker: false
	});
	$('#planRoutesBtn').click(function(){
		$('#orderDateForPlanRoute').removeClass('is-invalid').prev('.invalid-feedback').remove();
		if($('#orderDateForPlanRoute').val() == ''){
		   	$('#orderDateForPlanRoute').addClass('is-invalid');
		   	$('#orderDateForPlanRoute').before('<div class="invalid-feedback ml-0">Please select date</div>');
			$('#orderDateForPlanRoute').prev('.invalid-feedback').show();
			return false;
		}
		$('#planRoutesModal').modal();
	});
	$('#planRoutesModal').on('shown.bs.modal', function (e) {
		$.ajax({
			url: "{{route('admin.get-orders-drivers-by-date')}}",
			type:"POST",
			data:{
				"_token": "{{ csrf_token() }}",
				date: $('#orderDateForPlanRoute').val()
			},
		   	success:function(data) {
				var responseData = JSON.parse(data);
				$('#available-drivers').removeClass('d-none');
				$('p.planinginfo').text('Planning with '+responseData.countorders+' orders & '+responseData.countdrivers+' drivers');
				var tableData = '';
				var nos = 0;
				$(responseData.drivers).each(function(key, item){
					nos = nos + 1;
					tableData += '<tr><td>'+nos+'</td>';
					tableData += '<td><div class="custom-control custom-checkbox">';
                    tableData += '<input id="driver_'+item.id+'" name="driver_id[]" value="'+item.id+'" type="checkbox" class="custom-control-input">';
                    tableData += '<label for="driver_'+item.id+'" class="custom-control-label">&nbsp;</label>';
                    tableData += '</div></td>';
					tableData += '<td>'+item.name+'</td>';
					tableData += '<td>'+item.worktime_start+'</td>';
					tableData += '<td>'+item.worktime_end+'</td>';
					tableData += '<td>'+item.vehicle+'</td></tr>';
				});
				$('#available-drivers tbody').html(tableData);
				//$("#msg").html(data.msg);
		   	}
		 });
	});
	$('#planRoutesModal').on('hidden.bs.modal', function () {
		$('p.planinginfo').text('');
		$('#available-drivers, #drivers-orders').addClass('d-none').find('tbody').html('');
	});
	$('#submitDriversForPlan').on('click', function () {
		var checkedDrivers = $('#available-drivers tbody [type="checkbox"]:checked').length;
		$('.resultMsg').text('').removeClass('text-danger').removeClass('text-success');
		if(checkedDrivers == 0){
		   	var errorMessage = '';
			$('.resultMsg').text('Please Select at least 1 driver').addClass('text-danger');
		} else {
			//$('#submitDriversForPlan').attr('disabled', 'disabled');
			//$('.resultMsg').text('Please wait').addClass('text-success');
			routePlanForSelectedDateAndDrivers();
		}
	});
	function routePlanForSelectedDateAndDrivers()
	{
		var planDate = $('#orderDateForPlanRoute').val();
		var selectedDrivers = [];
		$('#available-drivers tbody [type="checkbox"]:checked').each(function(){
			selectedDrivers.push($(this).val());
		});
		$.ajax({
			url: "{{route('admin.orders-planing')}}",
			type:"POST",
			data:{
				"_token": "{{ csrf_token() }}",
				planDate: planDate,
				pDrivers: selectedDrivers
			},
		   	success:function(data) {
				var responseData = JSON.parse(data);
				if(responseData.status == true){
					//$('#available-drivers').addClass('d-none').find('tbody').html('');
					//$('#drivers-orders').removeClass('d-none');
					//$('p.planinginfo').text('Planning with '+responseData.countorders+' orders & '+responseData.countdrivers+' drivers');
					var tableData = '';
					var nos = 0;
					$(responseData).each(function(key, item){
						nos = nos + 1;
						tableData += '<tr><td>'+nos+'</td>';
						tableData += '<td>'+item.id+'</td>';
						tableData += '<td>'+item.drivername+'</td>';
						tableData += '<td>'+item.schduled+'</td>';
						tableData += '<td>'+item.vehicle+'</td></tr>';
					});
					$('#drivers-orders tbody').html(tableData);
				} else {
					$('.resultMsg').text(responseData.message).addClass('text-danger');
				}
		   	}
		 });
	}
	$(document).ready(function(){
		var dataTable = $('#orders-users').DataTable({
			"scrollX": true,
			"dom": '<"float-left"B><"float-right"f>rt<"row pt-3"<"col-sm-3"i><"col-sm-3"l><"col-sm-6"p>>',
			"ajax": {
				url: "{{ url('admin/routes-orders') }}",
				"dataType": "json",
				"type": "GET",
				"data":{_token: "{{csrf_token()}}"}
			},
			"processing": true,
			"serverSide": true,
			"bPaginate": true,
			"sPaginationType": "full_numbers",
			"columns": [
				{ "data": "sno" },
				{ "data": "id" },
				{ "data": "priority" },
				{ "data": "schduled" },
				{ "data": "service_start" },
				{ "data": "service_end" },
				{ "data": "duration" },
				{ "data": "location_id" },
				{ "data": "name" },
				{ "data": "gatecode" },
				{ "data": "stoup_num" },
				{ "data": "address" }
			],
			columnDefs: [
				{ orderable: false, targets: [0,6,7,8,9,10] } //This part is ok now
			],
			order: [[ 1, "desc" ]],
			language: {
				paginate: {
				  first: '<i class="fa fa-fw fa-fast-backward">',
				  last: '<i class="fa fa-fw fa-fast-forward">',
				  next: '<i class="fa fa-fw fa-long-arrow-right">',
				  previous: '<i class="fa fa-fw fa-long-arrow-left">'  
				}
			}
		});			
		/*var dataTable = $('#routes-users').DataTable({
			"responsive": true,
			"ajax": {
				url: "{{ url('admin/routes-orders') }}",
				"dataType": "json",
				"type": "GET",
				"data":{_token: "{{csrf_token()}}"}
			},
			"processing": true,
			"serverSide": true,
			"bPaginate": true,
			"sPaginationType": "full_numbers",
			"columns": [
				{ "data": "sno" },
				{ "data": "id" },
				{ "data": "priority" },
				{ "data": "schduled" },
				{ "data": "service_start" },
				{ "data": "service_end" },
				{ "data": "duration" },
				{ "data": "location_id" },
				{ "data": "name" },
				{ "data": "gatecode" },
				{ "data": "stoup_num" },
				{ "data": "address" }
			],
			columnDefs: [
				{ orderable: false, targets: [0,6,7,8,9,10] } //This part is ok now
			],
			order: [[ 1, "desc" ]],
			language: {
				paginate: {
				  first: '<i class="fa fa-fw fa-fast-backward">',
				  last: '<i class="fa fa-fw fa-fast-forward">',
				  next: '<i class="fa fa-fw fa-long-arrow-right">',
				  previous: '<i class="fa fa-fw fa-long-arrow-left">'  
				}
			}
		});	*/
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			$($.fn.dataTable.tables( true ) ).css('width', '100%');
        	$($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
		}); 
	});
</script>
<script>	
	function initMap() {
		var myLatLng = {lat: 51.503454, lng: -0.119562};
		var mapSetting = new google.maps.Map(document.getElementById('mapData'), {
			zoom: 8,
			center: myLatLng
		});
	}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&libraries=places&callback=initMap"></script>