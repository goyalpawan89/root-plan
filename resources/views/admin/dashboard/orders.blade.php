<div class="tab-pane fade settingtab" id="orders" role="tabpanel" aria-labelledby="orders-tab">
	<div class="form-group col-md-12 mt-2 mb-4"> 	
		<a href="{{ url('admin/orders/create') }}" name="submitConfig" value="" class="btn btn-success"><i class="fas fa-plus"></i>&nbsp;Add Order</a>&nbsp;&nbsp;
		<a href="{{ url('admin/orders/import') }}" name="submitConfig" value="" class="btn btn-success"><i class="fas fa-plus"></i>&nbsp;Import Order</a>
	</div>
	<table class="table card-text table-md" id="orders-users">
		<thead>
			<tr>
				<th>S No</th>
				<th>Order ID</th>
				<th>Priority</th>
				<th>Scheduled at</th>
				<th>Service start</th>
				<th>Service end</th>
				<th>Actual duration</th>
				<th>Location</th>
				<th>Driver</th>
				<th>Gate Code</th>
				<th>Stoup Number</th>
				<th>Address</th>
			</tr>
		</thead>								
	</table>
</div>