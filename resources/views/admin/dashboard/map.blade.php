<div class="tab-pane fade show active" id="map" role="tabpanel" aria-labelledby="map-tab">
<div class="row">
  <div class="col-sm-8"><div id="mapData"></div></div>
  <div class="col-sm-4 drivers">
	<h2>Drivers</h2>
	<div class="row">
		<div class="col-md-12 driverListBlock" style="max-height:450px; overflow: auto;">
			@foreach($drivers as $driver)
			<div class="drivers-lst">
			  <figure><img src="{{ asset('admin/img/01-shutterstock_476340928-Irina-Bg-1024x683.png') }}" alt="" /></figure>
			  <h3>{{$driver->name}}</h3>
			  <p>{{$driver->email}}</p>
			  <p>{{$driver->phone}}</p>
			  <div class="drivaction">
				  <!--<i class="fa fa-cart-arrow-down" aria-hidden="true"></i> 20 
				  <i class="fa fa-clock-o" aria-hidden="true"></i> 120min 
				  <i class="fa fa-tachometer" aria-hidden="true"></i> 90miles-->
				  <i class="fa fa-cart-arrow-down" aria-hidden="true"></i> N/A 
				  <i class="fa fa-clock-o" aria-hidden="true"></i> N/A  
				  <i class="fa fa-tachometer" aria-hidden="true"></i> N/A 
			  </div>
			</div>
			@endforeach
		</div>
	</div>                    
  </div>
</div>
</div>