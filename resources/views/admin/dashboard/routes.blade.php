<div class="tab-pane fade" id="routes" role="tabpanel" aria-labelledby="routes-tab" style="100%!important;">
	<div class="form-group col-md-12 mt-2 mb-4"> 
		<label>
			Select Date : 
		</label>		
		<label>
			<input type="text" autocomplete="off" readonly id="orderDateForPlanRoute" name="orderDateForPlanRoute" class="form-control"/>
		</label>		
		<button type="button" name="submitConfig" id="planRoutesBtn" class="btn btn-success">
			<i class="fas fa-plus"></i>&nbsp;Plan route
		</button>&nbsp;&nbsp;
		<button type="button" name="submitConfig" value="" class="btn btn-success"><i class="fas fa-file-export"></i>&nbsp;Export routes</button>
	</div>
	<table class="table card-text" id="routes-users">
		<thead>
			<tr>
				<th>S No</th>
				<th>#</th>
				<th>Driver</th>
				<th>WT From</th>
				<th>WT To</th>
				<th>Vehicle</th>
			</tr>
		</thead>								
	</table>
</div>