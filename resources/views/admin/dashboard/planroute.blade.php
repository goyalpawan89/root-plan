<div class="modal planmod fade bd-example-modal-xl" id="planRoutesModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content stepara">
      <div class="modal-header">
        <h5 class="modal-title" id="planName">Plan Route</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <!-- MultiStep Form -->
      <div class="row justify-content-center mt-0">
        <div class="col-md-12">
			<div class="col-md-12 mt-2 mb-2">
			  	<p>Manually add an order to your route, Sprwt Root system will then autoamtically adjust your map preview and delivery driver routes to accomoadate this new order.</p>				
			  	<p class="planinginfo"></p>				
				<table class="table card-text d-none table-striped table-bordered" id="available-drivers">
					<thead>
						<tr>
							<th>S No</th>
							<th>#</th>
							<th>Driver</th>
							<th>WT From</th>
							<th>WT To</th>
							<th>Vehicle</th>
						</tr>
					</thead>
					<tbody>
					
					</tbody>
					<tfoot>
						<tr>
							<td colspan="3">
								<span class="resultMsg"></span>
							</td>
							<td colspan="3" align="right">
								<button id="submitDriversForPlan" class="btn btn-success">Plan Routes</button>
								<button class="btn btn-danger">Cancel</button>
							</td>
						</tr>
					</tfoot>
				</table>
				<table class="table card-text d-none table-striped table-bordered" id="drivers-orders">
					<thead>
						<tr>
							<th>S No</th>
							<th>Order Id</th>
							<th>Driver</th>
							<th>Date</th>
							<th>Vehicle</th>
						</tr>
					</thead>
					<tbody>
					
					</tbody>					
				</table>
			</div>
        </div>
      </div>
    </div>
  </div>
</div>