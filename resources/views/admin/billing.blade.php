@extends('layouts.app')
@section('content')
<div class="container-fluid">
  <section>
    <div class="row">
      <div class="col-lg-12 mb-4">
        <div class="card">
          <div class="card-body settingtab">
            <div class="crdcapt">
              <h5>Manage subscription</h5>
            </div>
            @if(auth()->user()->userSubscription->plan_id > 0)
            <div class="row subshead">
              <div class="col-md-12 d-none">
                <div class="row">
                  <div class="col-md-6">Account Plan:</div>
                  <div class="col-md-6">{{auth()->user()->userSubscription->plan->planname}}</div>
                </div>
                <div class="row">
                  <div class="col-md-6">Number of drivers:</div>
                  <div class="col-md-6">{{auth()->user()->userSubscription->drivers}} Drivers</div>
                </div>
              </div>
              <div class="col-md-12 text-center">
                <button type="button" class="grnbtn updateCard">Update Card</button>
                <button type="button" class="grnbtn cancelSubscribe">Cancel subscription</button>
              </div>
            </div>
            @endif
            @include('admin.billing.planblock')
            @if(auth()->user()->userSubscription->plan_id > 0)
				@include('admin.billing.driver')
				@include('admin.billing.cardlist')            
				@include('admin.billing.credit')            
				@include('admin.billing.invoice')    
            @endif 
			</div>
        </div>
      </div>
    </div>
  </section>
</div>
@if(auth()->user()->userSubscription->plan_id == 0)
	@include('admin.billing.planmodal')
@endif 
@if(auth()->user()->userSubscription->plan_id > 0)
	@include('admin.billing.updatecard')
	@include('admin.billing.update')
	@include('admin.billing.cancel')
@endif 
@endsection

@section('footer_scripts')
@include('admin.billing.style')
<script src="https://js.stripe.com/v3/"></script>
@include('admin.billing.script')
@endsection