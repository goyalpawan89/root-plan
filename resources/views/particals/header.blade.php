<!--<div class="admusr">Normal User</div>-->
<header class="header">
  <nav class="navbar navbar-expand-lg px-4 py-2 "><a href="#" class="sidebar-toggler text-gray-500 mr-4 mr-lg-5 lead"><i class="fas fa-align-left"></i></a>
    <ul class="ml-auto d-flex align-items-center list-unstyled mb-0">
      <li class="nav-item dropdown ml-auto">
	  <a id="userInfo" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">
	  <img src="{{ asset('admin/img/sprwt-icon.png') }}" alt="Jason Doe" style="max-width: 50px" class="img-fluid rounded-circle"> {{auth()->user()->name}} 
	  <i class="fa fa-angle-down" aria-hidden="true"></i> 
	  </a>
        <div aria-labelledby="userInfo" class="dropdown-menu">    
		<?php if(auth()->user()->role==2){ ?>
		<!--<a href="https://sprwt.freshdesk.com" class="dropdown-item"><i class="fa fa-question-circle" aria-hidden="true"></i> <label style="margin-left:14px;">Support</label> </a>
		<a href="#" class="dropdown-item"><i class="fa fa-usd" aria-hidden="true"></i>  <label style="margin-left:17px;">Billing</label> </a>--> 
		<?php }?>
		<a href="{{url('logout')}}" class="dropdown-item"><i class="fa fa-sign-out" aria-hidden="true"></i><label style="margin-left:17px;"> Logout</label></a> </div>
		
      </li>
    </ul>
  </nav>
</header>



