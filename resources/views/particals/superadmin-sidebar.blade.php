@inject('request', 'Illuminate\Http\Request')
<div id="sidebar" class="sidebar py-3" style="min-height:1000px">
  <div class="sidelogo"><img src="{{ asset('admin/img/logo-sprwt-2.png') }}" alt="" /></div>
  <div class="suadm">Super Admin</div>
  <ul class="sidebar-menu list-unstyled">
    <li class="sidebar-list-item"><a href="{{url('superadmin/customers')}}" class="sidebar-link text-muted {{ $request->segment(2) == 'customers' ? 'active' : '' }}"><i class="fa fa-user mr-3 text-gray sidebar-link-i" aria-hidden="true"></i><span>Customers</span></a></li>
    <li class="sidebar-list-item"><a href="{{route('superadmin.configuration')}}" class="sidebar-link text-muted {{ $request->segment(2) == 'configuration' ? 'active' : '' }}"><i class="fa fa-cogs mr-3 text-gray sidebar-link-i" aria-hidden="true"></i><span>Config</span></a></li>
    <li class="sidebar-list-item"><a href="{{route('superadmin.faqs.index')}}" class="sidebar-link text-muted {{ $request->segment(2) == 'faqs' ? 'active' : '' }}"><i class="fa fa-question mr-3 text-gray sidebar-link-i" aria-hidden="true"></i><span>FAQ</span></a></li>
    <li class="sidebar-list-item"><a href="{{route('superadmin.blogs.index')}}" class="sidebar-link text-muted {{ $request->segment(2) == 'blogs' ? 'active' : '' }}"><i class="fa fa-rss mr-3 text-gray sidebar-link-i" aria-hidden="true"></i><span>Blog</span></a></li>
    <li class="sidebar-list-item"><a href="{{route('superadmin.coupons.index')}}" class="sidebar-link text-muted {{ $request->segment(2) == 'coupons' ? 'active' : '' }}"><i class="fa fa-gift mr-3 text-gray sidebar-link-i" aria-hidden="true"></i><span>Coupon</span></a></li>

  </ul>
  <ul class="sidebar-menu list-unstyled logout">
    <li class="sidebar-list-item"><a href="{{url('logout')}}" class="sidebar-link text-muted"><i class="fa fa-sign-out mr-3 text-gray" aria-hidden="true"></i>
        <span>Logout</span></a></li>

  </ul>
</div>