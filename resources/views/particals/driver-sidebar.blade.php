<div id="sidebar" class="sidebar py-3" style="min-height:1000px">
  <div class="sidelogo"><img src="{{ asset('admin/img/logo-sprwt-2.png') }}" alt="" /></div>
  <!--<div class="suadm">Driver</div>-->
  <ul class="sidebar-menu list-unstyled">
    <li class="sidebar-list-item"><a href="{{url('driver/dashboard')}}" class="sidebar-link text-muted active"><i class="fa fa-home mr-3 text-gray sidebar-link-i" aria-hidden="true"></i><span>Dashboard</span></a></li>
    
  </ul>
  <ul class="sidebar-menu list-unstyled logout">
    <li class="sidebar-list-item"><a href="{{url('logout')}}" class="sidebar-link text-muted"><i class="fa fa-sign-out mr-3 text-gray" aria-hidden="true"></i>
        <span>Logout</span></a></li>
  </ul>
</div>