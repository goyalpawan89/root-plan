<!-- JavaScript files-->
<script src="{{ asset('admin/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('admin/vendor/popper.js/umd/popper.min.js') }}"> </script>
<script src="{{ asset('admin/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('admin/vendor/jquery.cookie/jquery.cookie.js') }}"> </script>
<script src="{{ asset('admin/vendor/chart.js/Chart.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<script src="{{ asset('admin/js/charts-home.js') }}"></script>
<script src="{{ asset('admin/js/front.js') }}"></script>
<script src="{{ asset('admin/vendor/bootbox/bootbox.min.js') }}"></script>
<script src="{{ asset('admin/vendor/datatable/DataTables-1.10.21/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/vendor/datatable/DataTables-1.10.21/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/vendor/datatable/DataTables-1.10.21/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/vendor/datatable/DataTables-1.10.21/js/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin/vendor/form-validator/jquery.form-validator.min.js') }}"></script>
<script src="{{ asset('admin/vendor/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>
<script src="{{ asset('admin/vendor/scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script>
  $.validate({
    lang: 'en'
  });
</script>
<script>
	function getSetMinHight(){
		var winMinHgt = $(window).height();
		$('#sidebar').css('min-height', winMinHgt);
	}
	$(document).ready(function(){
		getSetMinHight();
	});
	$(window).resize(function(){
		getSetMinHight();
	});
	$(document).on("click", ".deleteitem", function(e) {
		e.preventDefault();
		var currentForm = $(this).closest('form');
		bootbox.confirm({
			message: "Are you sure? to delete this item",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if(result){
					currentForm.submit();
				}
			}
		});
	});
</script>

