<html lang="en-US">
<head>
  <meta charset="utf-8">
</head>
<body>
  <table width="500" border="1">
    <tbody>
      <tr>
        <th scope="row">FULL NAME :</th>
        <td>{{$fullname}}</td>
      </tr>
      <tr>
        <th scope="row">EMAIL :</th>
        <td>{{$email}}</td>
      </tr>
      <tr>
        <th scope="row">WEBSITE :</th>
        <td>{{$website}}</td>
      </tr>
      <tr>
        <th scope="row">PHONE :</th>
        <td>{{$phone}}</td>
      </tr>
      <tr>
        <th scope="row">SUBJECT :</th>
        <td>{{$subject}}</td>
      </tr>
      <tr>
        <th scope="row">HOW DID YOU HEAR OF US?</th>
        <td>{{$howdidhear}}</td>
      </tr>
      <tr>
        <th scope="row">MESSAGE :</th>
        <td>{{$msg}}</td>
      </tr>
    </tbody>
  </table>
</body>

</html>