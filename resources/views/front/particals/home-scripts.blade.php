<script src="{{ asset('front/home/js/jquery-3.4.1.min.js') }}"></script> 
<script src="{{ asset('front/home/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('front/home/js/owl.carousel.js') }}"></script>
<script>
$(document).ready(function() {
  var owl = $('.owl-carousel');
  owl.owlCarousel({
	items: 1,
	loop: true,
	margin: 10,
	dots:false,
	autoplay: true,
	autoplayTimeout: 8000,
	autoplayHoverPause: true
  });
  $('.play').on('click', function() {
	owl.trigger('play.owl.autoplay', [1000])
  })
  $('.stop').on('click', function() {
	owl.trigger('stop.owl.autoplay')
  })
})
</script>
<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "220px";
}
function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>


<script>
	  /* ========================================== 
scrollTop() >= 300
Should be equal the the height of the header
========================================== */

$(window).scroll(function(){
    if ($(window).scrollTop() >= 50) {
        $('.top_section').addClass('fixed-header');
        $('.top_section').addClass('visible-title');
    }
    else {
        $('.top_section').removeClass('fixed-header');
        $('.top_section').removeClass('visible-title');
    }
});
	  
	  </script>
	  
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>



