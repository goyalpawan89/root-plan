<footer>
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-3 text-center">
	  <div class="ml-form-embed"
		  data-account="1172398:q1e8w0b6a2"
		  data-form="962178:k1m8i2">
		</div>
        <div class="footer_logo "> <img src="{{ asset('front/home/images/logo.png') }}" alt="logo"><br><br>

 <a class="get-btn"  onclick="ml_webform_1413062('show')" href="javascript:;">Get more From Us</a> </div>
      </div>
      <div class="col-sm-12 col-md-7 col-lg-6">
        <ul class="footer_links">
          <li><a href="{{ url('/about') }}">Company</a></li>
          <li><a href="{{ url('/plan') }}">Pricing</a></li>
           <li><a href="{{ url('/terms') }}">Terms & Conditions</a></li>
           <li><a href="{{ url('/faq') }}">FAQ</a></li>
          <li><a href="{{ url('/contactus') }}">Contact Us</a></li>
          
          <li><a href="{{ url('/blog') }}">Blog</a></li>
        </ul>        
      </div>
      <div class="col-sm-12 col-md-5 col-lg-3">
        <ul class="social_links">
          <li><a href="https://facebook.com/sprwt.io"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://www.linkedin.com/company/sprwt"><i class="fa fa-linkedin"></i></a></li>
          <li><a href="https://instagram.com/sprwt.io"><i class="fa fa-instagram"></i></a></li>
        </ul>
        <div class="copyright">2020 All Rights Reserved. <br>Sprwt Root Planner. A Sprwt Company</div>
      </div>
    </div>
  </div>
</footer>