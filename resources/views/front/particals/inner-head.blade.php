@inject('request', 'Illuminate\Http\Request')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{ config('app.name', 'Root Planner') }}</title>
@if($request->segment(1) == '')
	<link rel="stylesheet" type="text/css" href="{{ asset('front/home/css/bootstrap.min.css') }}">
@else
	<link rel="stylesheet" type="text/css" href="{{ asset('front/inner/css/bootstrap.min.css') }}">
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('front/inner/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('front/home/css/owl.carousel.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('front/home/css/owl.theme.default.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('front/home/css/custom.css?v=1') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('front/inner/css/custom.css?v=1') }}">
<!-- Favicon-->
<link rel="shortcut icon" href="{{ asset('admin/img/favicon.jpg') }}">
<!--<script>
function initFreshChat() {
window.fcWidget.init({
token: "981e866a-f682-4197-b5ab-a4b4a7f66591",
host: "https://wchat.freshchat.com"
});
}
function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
</script>-->

<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="2df6cbd4-18e5-49bb-a986-25a5d0e0d240";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>


<!-- MailerLite Universal -->
<script>
(function(m,a,i,l,e,r){ m['MailerLiteObject']=e;function f(){
var c={ a:arguments,q:[]};var r=this.push(c);return "number"!=typeof r?r:f.bind(c.q);}
f.q=f.q||[];m[e]=m[e]||f.bind(f.q);m[e].q=m[e].q||f.q;r=a.createElement(i);
var _=a.getElementsByTagName(i)[0];r.async=1;r.src=l+'?v'+(~~(new Date().getTime()/1000000));
_.parentNode.insertBefore(r,_);})(window, document, 'script', 'https://static.mailerlite.com/js/universal.js', 'ml');var ml_account = ml('accounts', '1172398', 'q1e8w0b6a2', 'load');
</script>
<!-- End MailerLite Universal -->

<script>
    var ml_webform_1413062 = ml_account('webforms', '{{config("configs.Mailerlite_form_Id")}}', '{{config("configs.Mailerlite_app_Id")}}', 'load');
    ml_webform_1413062('animation', 'fadeIn');
</script>

<!-- Hotjar Tracking Code for https://rootplanner.io -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:{{config("configs.Hotjar")}},hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>