{{config("configs.Script_begin_header")}}
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{ config('app.name', 'Root Planner') }}</title>
<link rel="stylesheet" type="text/css" href="{{ asset('front/home/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('front/home/css/font-awesome.min.css') }}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('front/home/css/owl.carousel.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('front/home/css/owl.theme.default.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('front/home/css/custom.css') }}">

