@inject('request', 'Illuminate\Http\Request')
<?php $roleSlug = config('global.userroleslug'); ?>
<div class="top_section">
  <header>
  <div class="header" id="myHeader">
    <div class="container-fluid">
      <div class="row">
      <div class="col-sm-12">
      <div class="logo"><a href="{{ url('/') }}"><img src="{{ asset('front/home/images/logo.png') }}" alt="logo"></a></div>
      <div class="menus">
        <nav class="navbar navbar-default navbar-header" role="navigation">
            <ul class="nav navbar-nav">
              <li class="{{ $request->segment(1) == '' ? 'active' : '' }}"><a href="{{ url('/') }}">Home</a></li>
              <li class="{{ $request->segment(1) == 'about' ? 'active' : '' }}"><a href="{{ url('/about') }}">About</a></li>
              <li class="{{ $request->segment(1) == 'plan' ? 'active' : '' }}"><a href="{{ url('/plan') }}">Pricing</a></li>
              <li class="{{ $request->segment(1) == 'faq' ? 'active' : '' }}"><a href="{{ url('/faq') }}">FAQ</a></li>
              <li class="{{ $request->segment(1) == 'blog' ? 'active' : '' }}"><a href="{{ url('/blog') }}">Blog</a></li>
              <li class="{{ $request->segment(1) == 'contactus' ? 'active' : '' }}"><a href="{{ url('/contactus') }}">Contact Us</a></li>             
            </ul>
        </nav>
		<div class="mobile_menu">
      <div id="mySidenav" class="sidenav mobile_menu">
  		<ul>
          <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
          <li class="{{ $request->segment(1) == '' ? 'active' : '' }}"><a href="{{ url('/') }}">Home</a></li>
          <li class="{{ $request->segment(1) == 'about' ? 'active' : '' }}"><a href="{{ url('/about') }}">About</a></li>
          <li class="{{ $request->segment(1) == 'plan' ? 'active' : '' }}"><a href="{{ url('/plan') }}">Pricing</a></li>
          <li class="{{ $request->segment(1) == 'faq' ? 'active' : '' }}"><a href="{{ url('/faq') }}">FAQ</a></li>
          <li class="{{ $request->segment(1) == 'blog' ? 'active' : '' }}"><a href="{{ url('/blog') }}">Blog</a></li>
          <li class="{{ $request->segment(1) == 'contactus' ? 'active' : '' }}"><a href="{{ url('/contactus') }}">Contact Us</a></li>
		  @guest
			  <li class="{{ $request->segment(1) == 'login' ? 'active' : '' }}"><a href="{{ url('/login') }}">Login</a></li>
			  <li class="{{ $request->segment(1) == 'register' ? 'active' : '' }}"><a href="{{ url('/register') }}">Register</a></li> 
		  @endguest
		  @auth			
			  <?php $roleUrl = $roleSlug[auth()->user()->role].'/dashboard'; ?>
			  <li class="{{ $request->segment(1) == 'my-account' ? 'active' : '' }}"><a href="{{ url($roleUrl) }}">My Account</a></li>
			  <li class="{{ $request->segment(1) == 'logout' ? 'active' : '' }}"><a href="{{ url('logout') }}">Logout</a></li> 
		  @endauth
          </ul>
          <!--<div class="right_btn mobile"> <a href="#">Login</a> <a href="#">Register</a> </div>-->
        </ul>
	  </div>
      <span onclick="openNav()">&#9776;</span>
      </div>
      </div>
      <div class="right_btn"> 
		  @guest
			  <a href="{{ url('/login') }}">Login</a> 
			  <a href="{{ url('/register') }}">Register</a> 
		  @endguest
		  @auth
		  	  <a href="{{ url($roleUrl) }}">My Account</a> 
		  	  <a href="{{ url('logout') }}">Logout</a> 
		  @endauth
	  </div>
      </div>
      </div>
    </div>
	</div>
  </header>
</div>