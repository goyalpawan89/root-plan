<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	{!! config('configs.Script_Begin_Header') !!}
	@include('front.particals.inner-head')
	{!! config('configs.Script_End_Header') !!}
</head>
<body id="{{ (isset($bodyid) ? $bodyid : '') }}">
	{!! config('configs.Script_Begin_Body') !!}
	@include('front.particals.inner-header')
    @yield('content')
	@include('front.particals.inner-footer')
	@include('front.particals.inner-scripts')
	@yield('scripts')
	{!! config('configs.Script_End_Body') !!}
</body>
</html>