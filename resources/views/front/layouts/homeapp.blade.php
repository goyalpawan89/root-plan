<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	@include('front.particals.home-head')
</head>
<body id="home_bg"> 
    @yield('content')
	@include('front.particals.home-scripts')
</body>
</html>