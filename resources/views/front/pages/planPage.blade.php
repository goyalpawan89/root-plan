@extends( 'front.layouts.innerapp' )
@section( 'content' )
<div class="planhead">
  <div class="container-fluid">
    <h2><span>Pricing</span> Plans that scale with you</h2>
    <p>Are you ready to boost your business into the next Meal Prep Management solution? Still have doubts? Just book a demo with one of our sales agent and you'll be amazed with what Sprwt can do to help your business.</p>
  </div>
</div>
<div class="plan">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-lg-4">
        <div class="planbox seeds">
          <div class="planname">{{$plans[0]->planname}}<img src="{{ asset('front/inner/img/p1.jpg') }}" alt="" /></div>
          <div class="planpric">${{floatval($plans[0]->amount)}}<span>/per Driver</span></div>
          <div class="plandes">{!! $plans[0]->plandescription !!}</div>
          <ul>
            <li>
              <figure><i class="fa fa-check-circle" aria-hidden="true"></i>
              </figure>50 orders
            </li>
            <li>
              <figure><i class="fa fa-check-circle" aria-hidden="true"></i>
              </figure>Import Deliveries
            </li>
            <li>
              <figure><i class="fa fa-check-circle" aria-hidden="true"></i>
              </figure>Dispatch Routes to App
            </li>
          </ul>
          <div><button type="button" class="btn subscribePlan" plan-id="{{$plans[0]->id}}" plan-name="{{$plans[0]->planname}}" plan-amount="{{floatval($plans[0]->amount)}}">Subscribe</button></div>
        </div>
      </div>
      <div class="col-sm-12 col-lg-4">
        <div class="planbox root">
          <div class="planname">{{$plans[1]->planname}}<img src="{{ asset('front/inner/img/p2.jpg') }}" alt="" /></div>
          <div class="planpric">${{floatval($plans[1]->amount)}}<span>/per Driver</span></div>
          <div class="plandes">{!! $plans[1]->plandescription !!}</div>
          <ul>
            <li>
              <figure><i class="fa fa-check-circle" aria-hidden="true"></i>
              </figure>150 orders
            </li>
            <li>
              <figure><i class="fa fa-check-circle" aria-hidden="true"></i>
              </figure>Starter plus
            </li>
            <li>
              <figure><i class="fa fa-check-circle" aria-hidden="true"></i>
              </figure>Live Delivery Tracking
            </li>

            <li>
              <figure><i class="fa fa-check-circle" aria-hidden="true"></i>
              </figure>Signature Capture
            </li>
          </ul>
          <div><button type="button" class="btn subscribePlan" plan-id="{{$plans[1]->id}}" plan-name="{{$plans[1]->planname}}" plan-amount="{{floatval($plans[1]->amount)}}">Subscribe</button></div>
        </div>
      </div>
      <div class="col-sm-12 col-lg-4">
        <div class="planbox sprwt">
          <div class="planname">{{$plans[2]->planname}}<img src="{{ asset('front/inner/img/p3.jpg') }}" alt="" /></div>
          <div class="planpric">${{floatval($plans[2]->amount)}}<span>/per Driver</span></div>
          <div class="plandes">{!! $plans[2]->plandescription !!}</div>
          <ul>
            <li>
              <figure><i class="fa fa-check-circle" aria-hidden="true"></i>
              </figure>500 orders
            </li>
            <li>
              <figure><i class="fa fa-check-circle" aria-hidden="true"></i>
              </figure>Base plus
            </li>
            <li>
              <figure><i class="fa fa-check-circle" aria-hidden="true"></i>
              </figure>Analytics
            </li>
            <li>
              <figure><i class="fa fa-check-circle" aria-hidden="true"></i>
              </figure>Signature Capture
            </li>
            <li>
              <figure><i class="fa fa-check-circle" aria-hidden="true"></i>
              </figure>Weekly Orders Planning
            </li>
          </ul>
          <div><button type="button" class="btn subscribePlan" plan-id="{{$plans[2]->id}}" plan-name="{{$plans[2]->planname}}" plan-amount="{{floatval($plans[2]->amount)}}">Subscribe</button></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid ">
  <div class="addons">
    <h2>Addons</h2>
    <p>Want to build the perfect plan from the seed up? Add our add-on services as you go and build the perfect system for your businesses's needs.</p>

    <div class="addonopt">
      <ul>
        <li>
          <div class="addbox">
            <div class="head">SMS</div>
            <div class="price">$20 <span>/month</span></div>
            <div class="discrpt">250 messages</div>
          </div>

        </li>
        <li>
          <div class="addbox">
            <div class="head">Traffic Info</div>
            <div class="price">$30 <span>/mos. Per driver</span></div>

          </div>

        </li>
        <li>
          <div class="addbox">
            <div class="head">Signature Capture</div>
            <div class="price">$5 <span>/mos. Per driver</span></div>

          </div>

        </li>
        <li>
          <div class="addbox">
            <div class="head">Analytics</div>
            <div class="price">$10 <span>/mos. Per driver</span></div>

          </div>

        </li>
        <li>
          <div class="addbox">
            <div class="head">Sprwt.io Integration</div>
            <div class="price">$10 <span>/mos. Per driver</span></div>

          </div>

        </li>

      </ul>
    </div>
  </div>
</div>
<div class="modal planmod fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content stepara">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Seed Plan</b>
		  <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget eros efficitur, efficitur urna tincidunt, ultrices dolor. Sed tristique ex et lacus ultricies vestibulum.</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>		
		    <div class="stepwizard">
    <div class="stepwizard-row">
        <div class="stepwizard-step">
            <button type="button" class="btn-circle active">1</button>
            <p></p>
        </div>
        <div class="stepwizard-step">
            <button type="button" class="btn-circle">2</button>
            <p></p>
        </div>
        <div class="stepwizard-step">
            <button type="button" class="btn-circle">3</button>
            <p></p>
        </div>
		 <div class="stepwizard-step">
            <button type="button" class="btn-circle">4</button>
            <p></p>
        </div>
    </div>
</div>
<div class="stepbox" id="step_1">
  <h2>Step 1</h2>
  <form class="form-inline">
    <div class="form-group mb-2">
      <label for="">How many drivers you have? <span class="nofdriver"></span></label><br />
      <input type="range" min="1" max="{{config('global.max_drivers')}}" name="nosdriver" id="nosdriver" step="1" value="1" class="custom-range">
    </div>
  </form>
</div>
<div class="stepbox"  id="step_2">
  <h2>Step 2</h2>
  <form class="form-inline">   
      <div class="planstep2">Choose the addons you want: </div>

			<div class="col-sm-12 col-md-12 col-lg-6">
			<ul class="modulesplan">
			@foreach($modules as $modId => $modName)				
				  <li class="step2">
				
  
<div class="checkbox checkbox-info checkbox-circle">
                        <input id="customCheck_{{$modId}}" name="addons[SMS]" type="checkbox" >
                        <label  for="customCheck_{{$modId}}">
                           {{$modName}}
                        </label>
                    </div>

				  </li>				  
			@endforeach
			 </ul>
		
		</div>    
  </form>
</div>
<div class="stepbox" id="step_3">
  <h2>Step 3</h2>
  <form class="form-inline">
    <div class="form-group mb-2"><br />
	  <div class="table-responsive"><table class="table  table-borderless" width="100%">
		  <tr>
		    <td><label for="label">Receipt</label></td>
		    <td class="planName">&nbsp;</td>
		    </tr>
		  <tr>
		  		<td>Plan</td>
		  		<td class="planName">Plan</td>
		  </tr>
		  <tr>
		  		<td>Drivers</td>
		  		<td class="totalDrivers">Plan</td>
		  </tr>
		  <tr>
		  		<td>Modules</td>
		  		<td class="allModules">Plan</td>
		  </tr>
		  <tr>
		  		<td>Coupons</td>
		  		<td class="couponCode"><input type="form-control" name="couponcode"/></td>
		  </tr>
	  </table></div>
    </div>
  </form>
</div>
<div class="stepbox" id="step_4">
  <h2>Step 4</h2>
    <div class="form-group mb-2">
      <table class="table table-borderless">
        <tr>
		    <td colspan="2"><label for="label">Billing information</label></td>
		    </tr>
		  <tr>
		  		<td>Street Address</td>
		  		<td><input type="form-control" name="address"/></td>
		  </tr>
		  <tr>
		  		<td>City</td>
		  		<td><input type="form-control" name="city"/></td>
		  </tr>
		  <tr>
		  		<td>Zip Code</td>
		  		<td><input type="form-control" name="zipcode"/></td>
		  </tr>
		  <tr>
		  		<td>State</td>
		  		<td><input type="form-control" name="state"/></td>
		  </tr>
		  <tr>
		  		<td>Country</td>
		  		<td><input type="form-control" name="country"/></td>
		  </tr>
	  </table>
    </div>
	<div class="form-group mb-2">
      <label for="">Payment Section</label> 
		<div class="clearfix"></div>
		<div class=" ">
		<div class="row">
			
<div class="col-12 col-sm-12 col-md-6  ">			
	
	<div class=" carddtl ">
	<div class="input-group ">
    <div class="input-group-prepend">
      <div class="input-group-text"><i class="fa fa-credit-card" aria-hidden="true"></i>
</div>
    </div>
    <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Card number">
  </div>
	
	</div>
	
			</div>
			<div class="col-12 col-sm-6 col-md-3"><div class=" carddtl "><input type="text" class="form-control  " id="inlineFormInputName2" placeholder="MM "></div></div>
			<div class="col-12 col-sm-6 col-md-3" ><div class=" carddtl "><input type="text" class="form-control  " id="inlineFormInputName2" placeholder="AA CVC"></div></div>
			
			</div>
		
		</div>
		
		
		
	  <div class="col-md-12 col-md-offset-3">
            <div class="panel panel-default credit-card-box">
                <!--<div class=" display-table" >
                    <div class="row display-tr" >
                       
                        <div class="display-td text-left" >                            
                            <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                        </div>
                    </div>                    
                </div>-->
                <!--<div class="panel-body">
  
                    @if (Session::has('success'))
                        <div class="alert alert-success text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{ Session::get('success') }}</p>
                        </div>
                    @endif
  
                    <form 
                            role="form" 
                            action="{{ route('stripe.post') }}" 
                            method="post" 
                            class="require-validation"
                            data-cc-on-file="false"
                            data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                            id="payment-form">
                        @csrf
  
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Name on Card</label> <input
                                    class='form-control' size='4' type='text'>
                            </div>
                        </div>
  
                        <div class='form-row row'>
                            <div class='col-sm-12 form-group card required p-2'>
                                <label class='control-label'>Card Number</label> <input
                                    autocomplete='off' class='form-control card-number' size='20'
                                    type='text'>
                            </div>
                        </div>
  
                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-4 form-group cvc required'>
                                <label class='control-label'>CVC</label> <input autocomplete='off'
                                    class='form-control card-cvc' placeholder='ex. 311' size='4'
                                    type='text'>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Month</label> <input
                                    class='form-control card-expiry-month' placeholder='MM' size='2'
                                    type='text'>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Year</label> <input
                                    class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                    type='text'>
                            </div>
                        </div>
  
                        <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>Please correct the errors and try
                                    again.</div>
                            </div>
                        </div>
  						<input type="hidden" id="totalpayable" name="totalpayable" value="" />
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now</button>
                            </div>
                        </div>
                          
                    </form>
                </div>-->
            </div>        
        </div>
    </div>
</div>
<input type="hidden" id="planAmtPerDriver" name="planAmtPerDriver" value="" />
<input type="hidden" id="currentStep" name="currentStep" value="" />
<div class="plantot ml-4 mr-4"> Total: <span class="plantot_dyn">$110.30</span></div>
<div class="m-4 text-right">
  <button class="gry_btn mr-2 closemodal" type="submit"><i class="fa" aria-hidden="true"></i>Cancel</button>
  <button class="gry_btn mr-2 backmodal" type="submit"><i class="fa" aria-hidden="true"></i>Back</button>
  <button class="green_btn nextmodal" type="submit"><i class="fa" aria-hidden="true"></i>Next</button>
  <button class="green_btn completemodal" type="submit">Complete</button>
</div>		
    </div>
  </div>
</div>
<?php $roleSlug = config('global.userroleslug'); ?>
@auth	
	<?php $roleUrl = $roleSlug[auth()->user()->role].'/dashboard'; ?>
@endauth
@endsection

@section( 'scripts' )
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
	function moveStep(setp){
		$('.stepbox').addClass('d-none');
		$('#step_'+setp).removeClass('d-none');
		if(setp == 1){
		   $('.closemodal').removeClass('d-none');
		   $('.backmodal').addClass('d-none');
		} else{
		   $('.closemodal').addClass('d-none');
		   $('.backmodal').removeClass('d-none');
	    }
		if(setp == 4){
		   $('.completemodal').removeClass('d-none');
		   $('.nextmodal').addClass('d-none');
		} else{
		   $('.completemodal').addClass('d-none');
		   $('.nextmodal').removeClass('d-none');
	    }
		$('.stepwizard-step .btn-circle').removeClass('active');
		var count = 0;
		$('.stepwizard-step').each(function(){
			//$(this).attr('data-cont', count+"_"+setp);
			if(count < setp){
				//$(this).attr('data-cont-inner', count+"_"+setp);
			   	$(this).find('.btn-circle').addClass('active');
			}
			count = count + 1;
		});
		if(setp == 3){
		   	var checkedVals = $('.custom-control-input:checkbox:checked').map(function() {
				return $(this).siblings('label').text();
			}).get();
			var allMods = checkedVals.join(", ");
			$('.allModules').text(allMods);
		}
	}
	$(document).ready(function(){		
		$(document).delegate('#nosdriver', 'change', function() {
			var totalDrivers = $(this).val();
			$('.totalDrivers').val(totalDrivers);
			var planAmtPerDriver = $('#planAmtPerDriver').val();
			$('span.nofdriver').text(totalDrivers);
			var totalAmount = parseInt(totalDrivers)*parseFloat(planAmtPerDriver);
			$(".bd-example-modal-xl").find('.plantot_dyn').text("$"+totalAmount);
			$('#totalpayable').val(totalAmount);
		});		
		$(document).delegate('.closemodal', 'click', function() {
			$(".bd-example-modal-xl").modal('hide');
		});		
		$(document).delegate('.nextmodal', 'click', function() {
			var step = parseInt($(".bd-example-modal-xl").find('#currentStep').val()) + 1;
			moveStep(step);
			$(".bd-example-modal-xl").find('#currentStep').val(step);
		});		
		$(document).delegate('.backmodal', 'click', function() {
			var step = parseInt($(".bd-example-modal-xl").find('#currentStep').val()) -1;
			moveStep(step);
			$(".bd-example-modal-xl").find('#currentStep').val(step);
		});
		$(document).delegate('.subscribePlan_Exit', 'click', function() {
			var planId = $(this).attr('plan-id');
			var planName = $(this).attr('plan-name');
			var planAmt = $(this).attr('plan-amount');
			$(".bd-example-modal-xl").find('.modal-title b, .planName').text(planName);
			$(".bd-example-modal-xl").find('.plantot_dyn').text("$"+planAmt);
			$(".bd-example-modal-xl").find('#planAmtPerDriver').val(planAmt);
			$(".bd-example-modal-xl").find('#currentStep').val(1);
			$('.totalDrivers').val($('#nosdriver').val());
			moveStep(1);
			$(".bd-example-modal-xl").modal();
		});
		$(document).delegate('.subscribePlan', 'click', function() {
			@guest
				window.location = '{{ url("/login") }}';
			@endguest
		  	@auth	
				window.location = '{{ url($roleUrl) }}';
			@endauth
		});
	});
</script>
<script type="text/javascript">
$(function() {
   
    var $form         = $(".require-validation");
   
    $('form.require-validation').bind('submit', function(e) {
        var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');
  
        $('.has-error').removeClass('has-error');
        $inputs.each(function(i, el) {
          var $input = $(el);
          if ($input.val() === '') {
            $input.parent().addClass('has-error');
            $errorMessage.removeClass('hide');
            e.preventDefault();
          }
        });
   
        if (!$form.data('cc-on-file')) {
          e.preventDefault();
          Stripe.setPublishableKey($form.data('stripe-publishable-key'));
          Stripe.createToken({
            number: $('.card-number').val(),
            cvc: $('.card-cvc').val(),
            exp_month: $('.card-expiry-month').val(),
            exp_year: $('.card-expiry-year').val()
          }, stripeResponseHandler);
        }
  
  });
  
  function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            /* token contains id, last4, and card type */
            var token = response['id'];
               
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }
   
});
</script>
@endsection