@extends('front.layouts.innerapp')
@section('content')	
 <div class="blogmain">
	  
	  <div class="container-fluid ">
	
		  <div class="row">
			 <div class="col-sm-6"> <div class="contushead">
		 <h2> <span class="subtxt">Blog</span>
			 {{$post->title}}
			  </h2>
		  
		  </div>
			  
			  
			
			  </div>
			  
			  
			  
		  </div>
		  <div class="row blogposttm">
		  <div class="col-sm-8"><i class="fa fa-arrow-left" aria-hidden="true"></i><a href="{{ url('/blog') }}"> Back to all posts</a></div>
			  <div class="col-sm-4 text-right">Created at: {{$post->created_at->format('M d, Y H:i')}}</div>
		  
		  
		  </div>
	<div class="row">
		  <div class="col-sm-12 text-center">
		  @if($post->image != '')
			<img src="{{ asset('images/blogs/main/'.$post->image) }}" alt="{{$post->title}}" />
		  @endif			  
			<div class="blgcapt">
			
				{!! $post->content !!}
				
			</div>  
		
		
	  </div>
		
		  
		  
		  </div>
	 
	 </div>
	  
	  </div>
@endsection