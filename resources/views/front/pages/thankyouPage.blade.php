@extends('front.layouts.innerapp')
@section('content')
<div class="container-fluid">
  <div class="aboutus" style="padding:80px 4%;">
    <div class="row">
      <div class="col-lg-5 offset-lg-1" style="align-self: center !important;">
        <div class="abouthead">
          <!--<h2><span>Contact Us</span></h2>-->
          <h3>Thank you!</h3>
        </div>
        <p>Tired of spending hours trying to map messy delivery routes yourself? Start saving 80% of your time by switching to Sprwt Root Planner. Plan your delivery days instantly. Optimize based on how many drivers you have. Sprwt Root Planner is smart technology built for your businesses efficient needs.</p>
      </div>
      <div class="col-lg-4"><img src="{{ asset('front/inner/img/about2Asset 1@2x.png') }}" alt=""/></div>
    </div>
  </div>
</div>
@endsection