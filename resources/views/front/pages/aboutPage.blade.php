@extends('front.layouts.innerapp')
@section('content')	

<div class="container-fluid ">
	  <div class="aboutus"><div class="row">
	<div class="col-lg-5 offset-lg-1  ">
		  <div class="abouthead"><h2><span>About</span> WE'RE INNOVATIVE </h2>
		  <p>Routing Software</p></div>
		<p>Tired of spending hours trying to map messy delivery routes yourself? Start saving 80% of your time by switching to Sprwt Root Planner. Plan your delivery days instantly. Optimize based on how many drivers you have. Sprwt Root Planner is smart technology built for your businesses efficient needs.</p>
		  
		  </div>
		  <div class="col-lg-6"><img src="{{ asset('front/inner/img/about2Asset 1@2x.png') }}" alt=""/></div>
	
	
	</div></div>
	  
	  
	  </div>
	  <div class="container-fluid  ">
	<div class="aboutus">  <div class="abotsec d-none d-sm-none d-md-none d-lg-block d-xl-block "><div class="row">
	<div class="col-lg-6 ">
		  <img src="{{ asset('front/inner/img/about.jpg') }}" alt=""/> </div>
		  <div class="col-lg-6"><div class="abtmr"><div class="abouthead"><h2>Why we built Sprwt Root Planner</h2>
		  </div>
		<p>We built this technology because of the demand we had for this system in the meal prep and restaurant space. We created the #1 meal prep and restaurant management system and our customers we’re asking us for a delivery route planning system that works. Of course there are hundreds of alternatives, but some looked like they belonged in 2003 and others didn’t even work.  We listened and we built accordingly. The features you actually use and need are what we’ve created. All the extra functionality that was originally designed for 200+ fleet corporations is simply overkill. Why pay extra when you don’t need it? Just manually add customers or import via Sprwt or a Excel/CSV file, choose which customers are active on your delivery, specify how many drivers you have and your route will automatically generate. You can configure your delivery drivers zones, start and end location, and more. </p></div></div>
	
	
	</div></div>
		  <div class="abotsec  d-block d-sm-block d-md-block d-lg-none d-xl-none "><div class="row">
	
		  <div class="col-lg-6"><div class="abtmr"><div class="abouthead"><h2>Why we built Sprwt Root Planner</h2>
		  </div>
		<p>We built this technology because of the demand we had for this system in the meal prep and restaurant space. We created the #1 meal prep and restaurant management system and our customers we’re asking us for a delivery route planning system that works. Of course there are hundreds of alternatives, but some looked like they belonged in 2003 and others didn’t even work.  We listened and we built accordingly. The features you actually use and need are what we’ve created. All the extra functionality that was originally designed for 200+ fleet corporations is simply overkill. Why pay extra when you don’t need it? Just manually add customers or import via Sprwt or a Excel/CSV file, choose which customers are active on your delivery, specify how many drivers you have and your route will automatically generate. You can configure your delivery drivers zones, start and end location, and more. </p></div></div>
	<div class="col-lg-6 ">
		  <img src="{{ asset('front/inner/img/about.jpg') }}" alt=""/> </div>
	
	</div></div></div>
	  
	  
	  </div>
@endsection