@extends('front.layouts.innerapp')
@section('content')	

<div class="slide_banner_cont">
    <div class="container-fluid">
    <div class="row">
    <div class="col-sm-12">
      <h2>THE MOST AMAZING</h2>
      <h4>Route Planning Software</h4>
	  
      <p>Tired of spending hours trying to map messy delivery routes yourself? Start saving 80% of your time by switching to Sprwt Root Planner. Plan your delivery days instantly. Optimize based on how many drivers you have. Sprwt Root Planner is smart technology built for your businesses efficient needs.</p>
    </div>
	 <div class="col-sm-12">
        <div class="schedule_btn"><a href="http://calendly.com/msprwt/root">Schedule a Demo Today</a></div>
      </div>
    </div>
    </div>
    
    
  </div>
<div class="three_step">
  <div class="container">
    <div class="row">
      <div class="steps">
        <div class="step_head">
          <h2>Three Easy Steps</h2>
          <p>Our system was built to be simple. No more tedious 10-step solution, just 1, 2, 3.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="steps_boxes">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="step_box">
            <div class="step-icon"><img src="{{ asset('front/home/images/customer_locate.png') }}" alt="customer"></div>
            <div class="step-cont">
              <h3>Add customers</h3>
              <p>Either manually add your customers or utilize our excel or Sprwt import features to automatically create your routes.</p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="step_box">
            <div class="step-icon"><img src="{{ asset('front/home/images/drive.png') }}" alt="customer"></div>
            <div class="step-cont">
              <h3>Assign a driver</h3>
              <p>Select which drivers are active for your deliveries and assign them to a route. Within a matter of minutes you'll have a detailed report breaking down every route by driver.</p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="step_box">
            <div class="step-icon"><img src="{{ asset('front/home/images/route.png') }}" alt="customer"></div>
            <div class="step-cont">
              <h3>Plan your route</h3>
              <p>Select your driver's start location and end location, click 2 buttons, and your routes are automatically created. As your drivers complete their routes, your customers are automatically notified of delivery.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="test_top">
  <div class="col-sm-4">
    <div class="test_top_img"><img src="{{ asset('front/home/images/homeAsset 1.png') }}" alt="home"></div>
  </div>
  <div class="col-sm-8">
    <div class="test_top_cont">
      <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
      <p>Donec non massa nisi. Nunc dictum purus eget est fringilla mattis. Nullam blandit viverra iaculis. Phasellus a dui lacus. Mauris nibh mi, consequat a posuere nec, finibus vel ante. Nunc vel turpis magna. Suspendisse lorem neque, lobortis id ornare eu, pharetra eu nisl. Aliquam vestibulum laoreet nibh, ut posuere mauris auctor quis.</p>
      <a href="#">Read More</a> </div>
  </div>
</div>
<div class="three_step step_border_none">
  <div class="container">
    <div class="row">
      <div class="testimonials">
        <div class="steps">
          <div class="step_head">
            <h2>Testimonials</h2>
            <p>See what our customers have to say.</p>
          </div>
          <div class="owl-carousel owl-theme">
            <div class="item">
              <div class="testimonial">
                <div class="testimonial_imag"><img src="{{ asset('front/home/images/avtar.png') }}" alt="avtar"></div>
                <h3>Dean Blank</h3>
                <h5>Clean Creations</h5>
                <p>We used to use a different route planning software, but it was so annoying to consistently go back and forth between our Meal Prep delivery system and theirs. This system is completely integrated with our meal prep website and allows us to exponentially reduce the time it takes to create routes for our drivers.</p>
              </div>
            </div>
            <div class="item">
              <div class="testimonial">
                <div class="testimonial_imag"><img src="{{ asset('front/home/images/avtar.png') }}" alt="avtar"></div>
                <h3>Vinny Rosado</h3>
                <h5>Vi Fit Life</h5>
                <p>I’ve used several route planning systems before and nothing compares to the way this product looks. The designs are sleek, modern, and easy to use. There’s no more clutter for 100 features I never used. Everything I need and actually use are here. Nothing more, nothing less. Simple, I love it!</p>
              </div>
            </div>
            <div class="item">
              <div class="testimonial">
                <div class="testimonial_imag"><img src="{{ asset('front/home/images/avtar.png') }}" alt="avtar"></div>
                <h3>Joana Dorian</h3>
                <h5>Joana’s Boutique Flowers</h5>
                <p>We were recommended this platform from a flower shop we know the owners of in Tampa who happens to know the guy who worked on this project. We decided to give it a shot. We only have 2 drivers max 3 and this product was so simple to use it was mindless. There’s nothing much to say besides this really does everything I need. Creates orders, select drivers, view routes, and send straight to my google maps app. Love it!</p>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      <div class="features">
        <div class="steps">
          <div class="step_head">
            <h2>Features</h2>
            <p>Simple solutions so you can ditch the old way and start doing it the Root way.</p>
          </div>
          <div class="steps_boxes">
            <div class="container">
              <div class="row">
                <div class="col-sm-4">
                  <div class="step_box">
                    <div class="step-icon"><img src="{{ asset('front/home/images/import.png') }}" alt="customer"></div>
                    <div class="step-cont">
                      <h3>Import your stops</h3>
                      <p>Add your stops in 3 ways. Import a spreadsheet, save Customer Profiles, or add them manually.</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="step_box">
                    <div class="step-icon"><img src="{{ asset('front/home/images/adjust_route.png') }}" alt="customer"></div>
                    <div class="step-cont">
                      <h3>Adjust your routes</h3>
                      <p>It's easy to customize your routes with Sprwt Root Software. Move and re-assign stops; delete or add last minute orders.</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="step_box">
                    <div class="step-icon"><img src="{{ asset('front/home/images/track.png') }}" alt="customer"></div>
                    <div class="step-cont">
                      <h3>Live tracking</h3>
                      <p>Track your drivers progresso and keep track of your customer's ETA in real-time. Don't keep people waiting - give your customers more accurate ETAs.</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="step_box">
                    <div class="step-icon"><img src="{{ asset('front/home/images/update.png') }}" alt="customer"></div>
                    <div class="step-cont">
                      <h3>Update your customers</h3>
                      <p>Let your customers know when to expect a delivery, when it's been completed with automatic SMS and email notifications.</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="step_box">
                    <div class="step-icon"><img src="{{ asset('front/home/images/api_inte.png') }}" alt="customer"></div>
                    <div class="step-cont">
                      <h3>Api Integration</h3>
                      <p>Push your orders directly into our platform with our API integration. Our routing experts are happy to help you get set up.</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="step_box">
                    <div class="step-icon"><img src="{{ asset('front/home/images/support.png') }}" alt="customer"></div>
                    <div class="step-cont">
                      <h3>Speedy, helpful support</h3>
                      <p>Live chat, phone, and email support offered Monday-Friday. 24/7 support available for Enterprise customers.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="schedule">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-md-8">
        <div class="schedule_cont">
          <h2>Ready to get started?</h2>
          <p>Contact us and give your business the boost it needs to grow.</p>
        </div>
      </div>
      <div class="col-sm-4 col-md-4">
        <div class="schedule_btn"><a href="http://calendly.com/msprwt/root">Schedule a Demo Today</a></div>
      </div>
    </div>
  </div>
</div>
@endsection