@extends('front.layouts.innerapp')
@section('content')	
<div class="blogmain">
	  
	  <div class="container ">
	
		  <div class="row">
			 <div class="col-sm-7"> <div class="contushead">
		 <h2> <span class="subtxt">Legal</span>
			  Terms are especially
			  <span>important at Sprwt </span></h2>
		  <p>At Sprwt we care about your protection and your business and therefore our lawyers help us keep all those details in order.</p>
		  </div>
			  
			  
			
			  </div>
			  <div class="col-sm-5"><img src="{{ asset('front/inner/img/legal.svg') }}" alt=""/>
			
			  </div>
			  
			  
	    </div>
		  
	<div class="container terms" ><div class="row"><div class="col-12"><ul id="myTab" role="tablist" class="nav nav-tabs nav-pills flex-column flex-sm-row text-center bg-light border-0 rounded-nav"><li class="nav-item flex-sm-fill"><a id="service-tab" data-toggle="tab" href="#service" role="tab" aria-controls="service" aria-selected="true" class="nav-link border-0 text-uppercase font-weight-bold active">Terms of Service</a></li> <li class="nav-item flex-sm-fill"><a id="policy-tab" data-toggle="tab" href="#policy" role="tab" aria-controls="policy" aria-selected="true" class="nav-link border-0 text-uppercase font-weight-bold">Acceptable Use Policy</a></li> <li class="nav-item flex-sm-fill"><a id="web-tab" data-toggle="tab" href="#web" role="tab" aria-controls="web" aria-selected="false" class="nav-link border-0 text-uppercase font-weight-bold">Website Terms of Use</a></li> <li class="nav-item flex-sm-fill"><a id="cookie-tab" data-toggle="tab" href="#cookie" role="tab" aria-controls="cookie" aria-selected="false" class="nav-link border-0 text-uppercase font-weight-bold">Cookie Policy</a></li> <li class="nav-item flex-sm-fill"><a id="privacy-tab" data-toggle="tab" href="#privacy" role="tab" aria-controls="privacy" aria-selected="false" class="nav-link border-0 text-uppercase font-weight-bold">Privacy Policy</a></li></ul> <div id="myTabContent" class="tab-content"><div id="service" role="tabpanel" aria-labelledby="service-tab" class="tab-pane fade px-4 py-5 show active"><section><div class="container"><div class="row"><div class="col-12"><h3>SPRWT END USER SOFTWARE LICENSE AGREEMENT</h3> <small>Last Modified: Oct 28, 2019</small></div></div> <div class="row"><div class="col-12 mt-5"><p></p><div class="alert alert-warning grey">
                  NOTICE TO USER: PLEASE READ THIS CONTRACT CAREFULLY. USING ALL OR ANY PORTION OF THE SOFTWARE INDICATES YOUR ACCEPTANCE
                OF ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT, INCLUDING, WITHOUT LIMITATION, THE RESTRICTIONS ON: USE (CONTAINED IN
                SECTION 2) AND TRANSFERABILITY (CONTAINED IN IN SECTION 4). YOU AGREE THAT THIS AGREEMENT IS ENFORCEABLE LIKE ANY
                WRITTEN NEGOTIATED AGREEMENT SIGNED BY YOU. IF YOU DO NOT AGREE, YOU MAY NOT USE THIS SOFTWARE AND MUST CONTACT
                SLICKVIEW LLC TO TERMINATE YOUR LICENSE AND TO RETURN ALL COPIES OF THE SOFTWARE LICENSED TO YOU.  
                </div> <p></p> <p></p><h4>1. DEFINITIONS.</h4> <br>
                When used in this Agreement, the following terms shall have the respective meanings indicated, such meanings to be
                applicable to both the singular and plural forms of the terms defined:
                "Computer" means an electronic device that accepts information in digital or similar form and manipulates it for a
                specific result based on a sequence of instructions.
                “Licensee”, “You” and “Your” mean You, Your Company and Your Company’s employees, unless otherwise indicated.<p></p> <p>"Licensor" means Slickview LLC (“Slickview”).</p> <p>"Permitted Number" means One (1) Kitchen Location, unless otherwise indicated under a valid license (e.g. volume
                license) granted by Slickview.</p> <p>“Kitchen Location” means a single kitchen location operated by a single entity which has capability for: (a) food
                preparation; (b) order fulfillment; (c) generating master reporting data; and (d) being a Pickup Location.</p> <p>“Retail Location” means a single location operated by the same entity as a Kitchen Location, and which also has
                capability for: (a) order fulfillment; (b) sharing reporting data; (c) sales of grab and go foods, beverages and other
                products; and (d) being a Pickup Location.</p> <p>“Pickup Location” means a single location that uses a Kitchen Location’s menu for: (a) order fulfillment; and (b)
                sharing reporting data. A pickup location must be owned by a third party entity and limited to the usage of a customer
                picking up orders. Any other usage of this, i.e. the facility is operated by the same single entity as a Kitchen
                Location or is used as a retail outlet for sales of Grab and Go foods is considered a retail location.</p> <p>"Software" means the web-based application, currently known as “Sprwt”, accessible to You pursuant to this Agreement and
                licensed to You by Slickview.</p> <p>"Use" or "Using" means to access, install, download, copy or otherwise benefit from using the functionality of the
                Software in accordance with the Documentation.</p> <p></p><h4>2. SOFTWARE LICENSE.</h4>
                This End User Software License Agreement ("Agreement") is made and effective as of the day you agree to these terms (the
                “Effective Date”), by and between Slickview LLC ("Licensor") and You ("Licensee").
                The Software is a proprietary product of Slickview and is licensed (not sold) to customers for their Use only under the
                terms of this Agreement. <br>
                In consideration of Your payment of the setup and license fees set forth below and Your compliance with the terms of
                this Agreement, Slickview grants to you a non-exclusive and non-transferable license to Use the Software for the
                purposes described herein.<p></p> <p></p><h4>2.1 General Use.</h4>
                You may install and Use the Software on Your compatible computer, up to the Permitted Number of domains for Use at the
                Permitted Number of kitchens or similar establishments. A separate License is required for each installation of the
                Software in excess of the Permitted Number. <br>
                The Software is a platform developed to enable a meal-prep business owner to manage and operate his meal prep business
                as it relates to internet orders and customer utilization and software management. The software is equipped with various
                features including, but not limited to: custom meal builder, meal packs, chef menu, a macro-nutrition calculator, gift
                cards, user profile with royalty system and a referral system, billing, email delivery notifications, detailed
                cooking/chef reports, delivery reports and label reports.<p></p> <p></p><h4>2.2 Use of Software.</h4>
                a. Customer Owned Data. All data and logos uploaded by Customer remains the property of Customer, as between Sprwt and
                Customer (Customer Data). Customer grants Sprwt the right to use, publicly display and distribute the Customer Data for
                purposes of performing under this agreement.<br>
                b. Contractor Access and Usage. Customer may allow its contractors to access the Service in compliance with the terms of
                this agreement, which access must be for the sole benefit of Customer. Customer is responsible for the compliance with
                this agreement by its contractors.<br>
                c. Customer Responsibilities. Customer (i) must keep its passwords secure and confidential; (ii) is solely responsible
                for Customer Data and all activity in its account in the Service; (iii) must use commercially reasonable efforts to
                prevent unauthorized access to its account, and notify Sprwt promptly of any such unauthorized access; and (iv) may use
                the Service only in accordance with the Service’s Knowledge Base and applicable law.<br>
                d. Technical Support. Sprwt must provide customer support for the Service under the terms of Sprwt Customer Support
                Policy (Support), which is located at sprwt.io/support, and is incorporated into this agreement for all purposes.<p></p> <p></p><h4>2.3 Customization.</h4>
                This Agreement does not include the customization of the Software to You or Your business. Any additional customization
                and/or modifications requested by You shall be billed separately to You by Slickview at its customary rates and charges.
                <p></p> <p></p><h4>2.4 Pricing.</h4>
                Your License to Use the Software is contingent upon the successful payment of the amounts due under this Agreement or
                any other Agreement made between You and Slickview as reflected in Exhibit A, attached hereto and made a part hereto.
                Your failure to pay as required shall be considered a breach of this Agreement and may, at Slickview’s sole option, lead
                to either a temporary or permanent revocation of this License and may subject You to further civil and criminal
                liability. The Basic Setup Fee is only refundable if Licensee terminates the License within the first seven (7) calendar
                days after initial installation. Thereafter, the Basic Setup Fee is completely non-refundable.
                <p></p> <p></p><h4>2.4 Compensation.</h4>
                A. Licensee must pay all fees as specified on the order, but if not specified then within 30 days of receipt of an
                invoice. Customer is responsible for the payment of all sales, use, withholding, VAT and other similar taxes. This
                agreement contemplates one or more orders for the Service, which orders are governed by the terms of this agreement.
                B. If Slickview does not receive any payment on it’s monthly due date, then Slickview retains the right to shut down the
                site until payment is made. The Licensee agrees to pay a late fee equal to the lesser of (i) five percent (5%) of such
                overdue amount (including any prior late fees, if any) for every 10 days such payment remains outstanding or (ii) the
                maximum amount permitted to be charged under applicable law. Such late fee shall apply to any payment set forth on the
                order.<p></p> <p></p><h4>2.5 Disclaimer.</h4>
                SPRWT DISCLAIMS ALL WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY, TITLE AND
                FITNESS FOR A PARTICULAR PURPOSE. WHILE SPRWT TAKES REASONABLE PHYSICAL, TECHNICAL AND ADMINISTRATIVE MEASURES TO SECURE
                THE SERVICE, SPRWT DOES NOT GUARANTEE THAT THE SERVICE CANNOT BE COMPROMISED. CUSTOMER UNDERSTANDS THAT THE SERVICE MAY
                NOT BE ERROR FREE, AND USE MAY BE INTERRUPTED.
                <p></p> <p></p><h4>3. INTELLECTUAL PROPERTY RIGHTS.</h4>
                A. The Software is the intellectual property of and is owned by Slickview. The structure, organization and code of the
                Software are the valuable trade secrets and confidential information of Slickview and its suppliers. The Software is
                protected by copyright and trade secret laws, including without limitation by United States Copyright Law, international
                treaty provisions and applicable laws in the country in which it is being used. You may not copy the Software, except as
                set forth in Section 2 ("Software License"). Slickview retains ownership of and all rights in the Software and in any
                copy, derivative or modification of the Software and/or its documentation, no matter by whom made. You agree that
                unauthorized copying, transfer and/or distribution of the Software will cause great damage to Slickview, which damage is
                far greater than the value of the copies of the Software. It is expressly understood and agreed by You that nothing in
                this Agreement is intended to nor shall any provision(s) be construed to assign or transfer to You (or to require
                Slickview to assign or transfer to You) any of Slickview’s copyrights and other ownership rights, in both cases related
                to the Software and/or to Slickview’s proprietary information and/or trade secrets (“Slickview’s Confidential
                Information”).<br>
                You also agree not to reverse engineer, decompile, disassemble or otherwise attempt to discover the source code of the
                Software except to the extent you may be expressly permitted to decompile under applicable law, it is essential to do so
                in order to achieve operability of the Software with another software program, and You have first requested Slickview to
                provide the information necessary to achieve such operability and Slickview has not made such information available.
                Slickview has the right to impose reasonable conditions and to request a reasonable fee before providing such
                information. Any information supplied by Slickview or obtained by You, as permitted hereunder, may only be used by You
                for the purpose described herein and may not be provided, disclosed, transferred, assigned or otherwise made available
                to any third party or used to create any software which is substantially similar to the expression of the Software.
                Requests for information should be directed to the Slickview Customer Support Department. Trademarks shall be used in
                accordance with accepted trademark practice, including identification of trademarks owners' names. Trademarks can only
                be used to identify printed output produced by the Software and such use of any trademark does not give You any rights
                of ownership in that trademark. Except as expressly stated above, this Agreement does not grant You any intellectual
                property rights in the Software.
                <p></p> <p> B. Reservation of Rights.<br>
                The software, workflow processes, user interface, designs, know-how, and other technologies provided by Sprwt as part of
                the Service are the proprietary property of Sprwt and its licensors, and all right, title and interest in and to such
                items, including all associated intellectual property rights, remain only with Sprwt. Customer may not remove or modify
                any proprietary marking or restrictive legends in the Service. Sprwt reserves all rights unless expressly granted in
                this agreement.</p> <p>C. Restrictions.<br>
                Customer may not (i) sell, resell, rent or lease the Service or use it in a service provider capacity; (ii) use the
                Service to store or transmit infringing, unsolicited marketing emails, libelous, or otherwise objectionable, unlawful or
                tortious material, or to store or transmit material in violation of third-party rights; (iii) interfere with or disrupt
                the integrity or performance of the Service; (iv) attempt to gain unauthorized access to the Service or their related
                systems or networks; (v) reverse engineer the Service; or (vi) access the Service to build a competitive service or
                product, or copy any feature, function or graphic for competitive purposes.</p> <p>D. Aggregate Data.<br>
                Licensee owns any intellectual property rights in and to its ingredient lists, recipes and meals. Licensor cannot use,
                disclose or resell that information to any third party without Licensee’s written consent. Licensee does, however, grant
                to Licensor the right to access, sell and/or otherwise use all or any portion of Licensee’s customer volume and data,
                purchasing habits and behaviors, order preferences and/or customer contact information for any lawful purposes.
                </p> <p>E. Judicial Process.<br>
                In the event You receive a subpoena or other validly issued administrative or judicial process (i.e. deposition,
                interrogatories, requests for information or documents in legal proceedings, civil investigative demand or other similar
                process) requesting all or part of Slickview’s Confidential Information, You will undertake to provide reasonable notice
                to Slickview of such receipt, so that Slickview may seek to obtain a protective order or other reliable assurance that
                Slickview’s Confidential Information will be accorded confidential treatment. Thereafter, You shall be entitled to
                comply with such subpoena or other process to the extent permitted by law. If, in the absence of a protective order or
                other remedy or the receipt of a waiver by Slickview, You are nonetheless legally compelled to disclose Slickview’s
                Confidential Information to any government agency, tribunal or other party, or else stand liable for contempt or suffer
                other censure or penalty, You may, without liability hereunder, disclose Slickview’s Confidential Information to such
                government agency, tribunal or other party, without liability to Slickview.</p> <p></p><h4>4. TRANSFER.</h4>
                You may not, rent, lease, sublicense, transfer, assign or authorize all or any portion of the Software to be copied
                onto, transferred to, and/or otherwise used by any other user’s computer, except as may be expressly permitted herein.
                <p></p> <p></p><h4>5. MULTIPLE ENVIRONMENT SOFTWARE / MULTIPLE COPIES.</h4>
                If the Software supports multiple environments and/or platforms, the total number of domains, kitchens, and/or other
                establishments Using the Software may not exceed the Permitted Number of One (“1”) license per entity. You may not,
                rent, lease, sublease, sublicense, lend, copy or allow any other individual, company, organization and/or entity to Use
                the Software, irrespective of any relationship that such other individual, company, organization, and/or entity has with
                You.
                <p></p> <p></p><h4>6. AMBIGUITIES.</h4>
                Both parties and their attorneys have participated in the drafting of this Agreement and neither party shall be
                considered the “drafter” for the purpose of any statute, case, or rule of construction that might cause any provision to
                be construed against the drafter of the Agreement.   
                <p></p> <p></p><h4>7. EXPORT RULES.</h4>
                In addition, if the Software is identified as export controlled items under the Export Laws, You represent and warrant
                that You are not a citizen of, or otherwise located within, an embargoed nation (including without limitation Iran,
                Iraq, Syria, Sudan, Libya, Cuba, North Korea, and Serbia) and that You are not otherwise prohibited under the Export
                Laws from receiving and/or Using the Software. All rights to Use the Software are granted on condition that such rights
                are forfeited if You fail to comply with the terms of this Agreement and/or if any of Your representations in this
                Agreement are false.<p></p> <p></p><h4>8. GOVERNING LAW.</h4>
                This Agreement shall be governed by and interpreted in accordance with the laws of the State of Connecticut. The parties
                agree not to commence any action, suit or proceeding against the other or its affiliates or their employees, officers,
                directors or shareholders in any jurisdiction other than the State of Connecticut. Buyer hereby irrevocably (i) submits
                to the exclusive jurisdiction of any Connecticut state or Federal court sitting in Fairfield County, Connecticut, in any
                action or proceeding arising out of or relating to this Agreement or the transactions contemplated hereby, (ii) agrees
                that all claims in respect of such action or proceeding may be heard and determined in such Connecticut state court or
                in such Federal court, and (iii) waives, to the fullest extent permitted by law, the defense of an inconvenient forum to
                the maintenance of such action or proceeding. Any controversy or claim arising out of or relating to this Agreement, or
                the breach thereof, shall be settled by arbitration administered by the American Arbitration Association. The prevailing
                party in any such dispute shall be entitled to recover its reasonable attorney’s fees and costs.
                <p></p> <p></p><h4>9. DISCLAIMER &amp; LIMITATION OF LIABILITY.</h4>
                A. THE SOFTWARE IS LICENSED (NOT SOLD). It is licensed to licensees without either express or implied warranties of any
                kind, on an “as is” basis. Slickview makes no express or implied warranties to Licensee with regard to the Software, as
                to its performance, merchantability, fitness for any purpose or non-infringement of patents, copyrights or other
                proprietary rights of others.<br> <p>B. Neither Slickview nor anyone else who has been involved in the creation, production, testing or delivery of this
                software shall be liable for any direct, incidental or consequential damages, such as, but not limited to, loss of
                profits or benefits, resulting from the Use of the Software and/or arising out of any breach of any warranty, any server
                downtime and/or any Software “glitches.” If any of the provisions of this Agreement, or portions thereof, are invalid
                and/or unenforceable under any statute, regulation or other rule of law, they are to be deemed omitted from this
                Agreement, to the extent they are invalid and/or unenforceable. If any part of this Agreement is found void and
                unenforceable, it will not affect the validity of the balance of the Agreement, which balance shall remain valid and
                enforceable according to its terms.</p> <p>C. In no event shall Slickview be liable to Licensee if the Software and/or any server on which Licensee’s data is
                stored gets corrupted, infected by a virus, becomes the target of so-called malware, ransomware, keystroke logging,
                “hacking” and/or any other unauthorized and/or malicious access, denial of access, use, or other improper actions by a
                third party. If such an adverse event does occur, Licensee agrees to indemnify Licensor against all claims, demands,
                damages, losses, causes of action and the like which may arise from and/or relate to any such adverse event.
                </p> <p>D. Licensee acknowledges that the Software requires Licensee’s use of “Stripe” (individually and collectively “Merchant
                Account”) for the financial transactions on Licensee’s website, and that account crediting, refunds, fraud protections
                and the like for Licensee’s Merchant Account are controlled by the policies and procedures of those companies and
                Licensee’s bank(s). Licensee agrees to look solely to Licensee’s Merchant Account, Licensee’s bank(s) and/or Licensee’s
                customer(s) with regard to any issues that may arise concerning those accounts and/or Licensee’s customer transactions,
                including but not limited to payment processing, credits, refunds, and fraudulent purchases. Further, Licensee agrees to
                indemnify Licensor against all claims, demands, damages, losses, causes of action and the like which may arise from
                and/or relate to any transaction(s) and/or attempted transaction(s) by, with and/or through Licensee’s Merchant Account
                and/or Licensee’s bank(s). Licensee understands that Sprwt utilizes Stripe as its payment processor and that all
                transactions and fees are processed through the Stripe system. Sprwt uses a customized integration with Stripe. Any fee
                associated with the usage of Stripe is found on their website https://stripe.com/pricing. Utilizing Sprwt with Stripe
                there is an application fee of 150 basis points per transaction.<br></p> <p>E. Sprwt is not liable for any indirect, special, incidental or consequential damages arising out of or related to this
                agreement (including, without limitation, costs of delay; loss of data, records or information; and lost profits), even
                if it knows of the possibility of such damage or loss.<br>
                F. Sprwt’s total liability arising out of or related to this agreement (whether in contract, tort or otherwise) does not
                exceed the amount paid by Customer within the 3-month period prior to the event that gave rise to the liability.
                </p> <p>G. There is a $500 non-refundable installation fee for any servers that are active but taken down due to cancellation.
                Any cancelled accounts that wants to sign up again and there is no active data or information pertaining to the old site
                will have to pay a non-refundable $2000 setup fee.</p> <p></p> <p></p><h4>10. GENERAL PROVISIONS.</h4>
                This Agreement shall not prejudice the statutory rights of any party dealing as a consumer. This Agreement may only be
                modified by a writing signed by an authorized officer of Slickview. Updates may be licensed to You by Slickview with
                additional or different terms. This is the entire agreement between Slickview and You relating to the Software and it
                supersedes any prior representations, discussions, undertakings, communications or advertising relating to the Software.
                <p></p> <p></p><h4>11. NOTICE TO U.S. GOVERNMENT END USERS.</h4>
                If You are acquiring the Software on behalf of any unit or agency of the United States Government, the following
                provision applies: The Software was developed exclusively at private expense and with no government funding; The
                Software is a trade secret of Slickview for all purposes of the Freedom of Information Act; The Software and
                Documentation are "Commercial Items," as that term is defined at 48 C.F.R. §2.101, consisting of "Commercial Computer
                Software" and "Commercial Computer Software Documentation," as such terms are used in 48 C.F.R. §12.212 or 48 C.F.R.
                §227.7202, as applicable, subject to limited utilization; and the Software and all copies of it, in all respects, are
                and shall remain proprietary to Slickview . Consistent with 48 C.F.R. §12.212 or 48 C.F.R. §§227.7202-1 through
                227.7202-4, as applicable, the Commercial Computer Software and Commercial Computer Software Documentation are being
                licensed to U.S. Government end users: (a) only as Commercial Items, and (b) with only those rights as are granted to
                all other end users pursuant to the terms and conditions herein. Unpublished-rights are reserved under the copyright
                laws of the United States. Use, duplication or disclosure by the U.S. Government or any person or entity acting on its
                behalf is subject to restrictions for software developed exclusively at private expense, as set forth in the Department
                of Defense, Rights in Technical Data Computer Software clause at DFARS 252.227-7013 or any successor clause; and for all
                government agencies, the Commercial Computer Software-Restricted Rights clause at FAR 52.227-19 or any successor clause.
                Use of the Software shall be limited to the facility for which it was acquired. All other U.S. Government personnel
                Using the Software are hereby on notice that the Use of the Software is subject to restrictions which are the same as,
                or similar to, those specified above. For U.S. Government End Users, Slickview agrees to comply with all applicable
                equal opportunity laws including, if appropriate, the provisions of Executive Order 11246, as amended, Section 402 of
                the Vietnam Era Veterans Readjustment Assistance Act of 1974 (38 USC 4212), and Section 503 of the Rehabilitation Act of
                1973, as amended, and the regulations at 41 CFR Parts 60-1 through 60-60, 60-250, and 60-741. The affirmative action
                clause and regulations contained in the preceding sentence shall be incorporated by reference in this Agreement.
                <p></p> <p></p><h4>12. TERM AND TERMINATION.</h4>
                A. This Agreement and the License granted herein shall commence upon the Effective Date and continue until all of the
                obligations of the parties have been performed or until earlier terminated as provided herein.
                <p></p> <p>B. Licensor’s appointment as consultant pursuant to this Agreement and this Agreement shall terminate upon the
                occurrence of any of the following events:<br>
                (i) In the event either party defaults in any material obligation owed to the other party pursuant to this Agreement,
                then this Agreement may be terminated if the default is not cured following at least forty-five (45) days written notice
                to the defaulting party. If such default is a payment default by the Licensee, any termination by Licensor’s shall not
                relieve Licensee of such payment obligation through the date of such termination; or<br>
                (ii) Either party is bankrupt or insolvent, or bankruptcy or insolvency proceedings are instituted against a party and
                the proceeding is not dismissed within forty-five (45) days after commencement.
                </p> <p>C. The License granted herein shall terminate immediately upon the termination of this Agreement. Upon the termination
                of the License, Licensee shall: (i) immediately cease Use of the Software; (ii) immediately destroy and/or delete all
                copies of the Software in Licensee’s possession and/or control; and (iii) certify such to Licensor such
                destruction/deletion within seven (7) calendar days after the termination of the License.
                </p> <p>D. Subscription Refund.<br>
                i. Licensee may terminate this Agreement at any time, but only upon written notice to Slickview. If any termination
                shall be effective after the seventh (7th) calendar day in any month, then Licensee shall be obligated to Licensor for
                the entire month’s Monthly License Fee, regardless of whether Licensee or Licensor initiated the termination.
                ii. If Licensee terminates this Agreement, Licensee has rights to request a refund limited to the monthly Subscription
                Fee for any Kitchen or Retail location. The Subscription Fee is only refundable if Licensee requests a refund within the
                first seven (7) calendar days after termination. Thereafter, the Basic Setup Fee is completely non-refundable. The
                refund is limited to the amount of unused days within the current billing period.
                </p> <p>E. Maintenance of Customer Data.<br>
                - Within 90-days after termination, Data will be available limited to Ingredients, Meal Recipes, Extra Items, and
                Customers’ User Profile data.<br>
                - After such 90-day period, Sprwt has no obligation to maintain the Customer Data and may destroy it.
                </p> <p>F. Return Sprwt Property Upon Termination.<br>
                Upon termination of this agreement for any reason, Customer must pay Sprwt for any unpaid amounts, and destroy or return
                all property of Sprwt. Upon Sprwt’s request, Customer will confirm in writing its compliance with this destruction or
                return requirement.
                </p> <p>G. Suspension for Violations of Law.<br>
                Sprwt may temporarily suspend the Service or remove the applicable Customer Data, or both, if it in good faith believes
                that, as part of using the Service, Customer has violated a law. Sprwt will attempt to contact Customer in advance.
                </p> <p></p><h4>13. INDEMNITY.</h4>
                If any third-party brings a claim against Sprwt, or requires Sprwt to respond to a legal process, related to Customer’s
                acts, omissions, data or information within the Software, Customer must defend, indemnify and hold Sprwt harmless from
                and against all damages, losses, and expenses of any kind (including reasonable legal fees and costs) related to such
                claim or request.
                <p></p> <p></p><h4>14. OTHER TERMS</h4>
                A. Entirety of this Agreement.<br>
                The terms and conditions set forth herein constitute the entire agreement between the parties and supersede any
                communications or previous agreements with respect to the subject matter of this Agreement. There are no written or oral
                understandings directly or indirectly related to this Agreement that are not set forth herein. No change can be made to
                this Agreement other than in writing and signed by both parties.
                <p></p> <p>B. No Assignment.<br>
                Neither party may assign or transfer this agreement or an order to a third party, except that this agreement with all
                orders may be assigned, without the consent of the other party, as part of a merger, or sale of substantially all the
                assets, of a party.
                </p> <p>C. Independent Contractors.<br>
                The parties are independent contractors with respect to each other.
                </p> <p>D. Enforceability and Force Majeure.<br>
                If any term of this agreement is invalid or unenforceable, the other terms remain in effect. Except for the payment of
                monies, neither party is liable for events beyond its reasonable control, including, without limitation force majeure
                events.
                </p> <p>E. Feedback.<br>
                By submitting ideas, suggestions or feedback to Sprwt regarding the Service, Customer agrees that such items submitted
                do not contain confidential or proprietary information; and Customer hereby grants Sprwt an irrevocable, unlimited,
                royalty-free and fully-paid perpetual license to use such items for any business purpose.
                </p> <p></p><h4>15. HEADINGS IN THIS AGREEMENT.</h4>
                The headings in this Agreement are for convenience only, confirm no rights or obligations in either party, and do not
                alter any terms of this Agreement.
                <p></p> <p></p><h4>16. SEVERABILITY &amp; ENFORCEABILITY.</h4>
                If any term of this Agreement is held by a court of competent jurisdiction to be invalid or unenforceable, then this
                Agreement, including all of the remaining terms, will remain in full force and effect as if such invalid or
                unenforceable term had never been included. The failure of any party at any time to require performance by the other
                party of a provision under this Agreement shall in no way affect the right of that party to thereafter enforce the same,
                or to enforce any of the other provisions of this Agreement; nor shall the waiver by any party of the breach of any
                provision hereof be taken or held to be a waiver of any subsequent breach of any such provision or as a waiver of the
                provision itself.<p></p> <p></p><h4>17. COMPLIANCE WITH LICENSES.</h4>
                If You are a business or organization, You agree that upon request from Slickview or Slickview’s authorized
                representative, You will within five (5) days fully document and certify that Use of any and all Slickview Software at
                the time of the request is in conformity with Your valid licenses from Slickview.
                <p></p> <p></p><h4>18. BINDING EFFECT.</h4>
                All rights, obligations, duties, restrictions and qualifications herein provided for shall insure to, and be binding
                upon, the parties hereto, each of their heirs, executors, administrators, legal representatives, successors and
                permitted assigns.
                <p></p> <p></p><h4>9. WAIVER OF JURY TRIAL.</h4>
                EACH PARTY HERETO HEREBY WAIVES, TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, ANY RIGHT IT MAY HAVE TO A TRIAL BY
                JURY IN ANY LEGAL PROCEEDING DIRECTLY OR INDIRECTLY ARISING OUT OF OR RELATING TO THIS AGREEMENT (WHETHER BASED ON
                CONTRACT, TORT OR ANY OTHER THEORY). EACH PARTY HERETO: (I) CERTIFIES THAT NO REPRESENTATIVE, AGENT OR ATTORNEY OF ANY
                OTHER PARTY HAS REPRESENTED, EXPRESSLY OR OTHERWISE, THAT SUCH OTHER PARTY WOULD NOT, IN THE EVENT OF LITIGATION, SEEK
                TO ENFORCE THE FOREGOING WAIVER AND (II) ACKNOWLEDGES THAT IT AND THE OTHER PARTIES HERETO HAVE BEEN INDUCED TO ENTER
                INTO THIS AGREEMENT BY, AMONG OTHER THINGS, THE MUTUAL WAIVERS AND CERTIFICATIONS IN THIS SECTION.
                If You have any questions regarding this Agreement or if You wish to request any information from Slickview please use
                the address and contact information included with this product to contact the Slickview office serving Your
                jurisdiction.<br>
                Slickview LLC, Slickmeals and Sprwt are either registered trademarks or trademarks of Slickview LLC in the United States
                and/or other countries.
                <p></p> <p>Additional Costs:<br>
                Prices set out herein are exclusive of all taxes. Licensee shall pay any taxes, tariffs, duties and other charges or
                assessments imposed or levied by any government or governmental agency at any time in connection with this Agreement
                and/or the Software, including, without limitation, any federal, provincial, state and local sales, use, goods and
                services, value-added and personal property taxes on any payments due Licensor in connection with the Software or
                maintenance services provided hereunder, except for taxes based solely on the net income of Licensor.
                Additional Features Available to Add to System
                <i>(Slickview reserves the right to modify pricing for additional features, from time to time after 15 days’ advance notice
                to Licensee)</i></p> <p><b>Plan Upgrades</b>
                
                Additional Kitchen Location: $500/mo. for each additional location (up to a total of 3 locations). Use of Software at
                more than 4 locations requires a custom quote.<br>
                Additional Retail Location: $500/mo. for each additional location. Includes 1 device/register per location, additional
                devices/registers will be quoted at $50/mos.<br>
                Pickup Location: $10/mo. for each additional location<br>
                Reseller Program: $50/mos. Manage multiple store locations, retail locations, resellers/ambassador, branches or
                franchises, and more on a single dashboard. Inclusive in Pro Plan<br>
                Shipping Fulfillment Tool: Contact us for details<br>
                Marketing/Sales Suites: Contact us for details
                </p> <p><b>IT Security Upgrades</b>
                
                The Sprwt system at launch is equipped with 2GB RAM, 1vCPU, 2TB storage. If you'd like additional RAM, CPU, or Storage
                you will need to purchase them separately.<br><br>
                
                Backups: $100/mos. Backups exist to backup server data and to make sure a licensee has a previous version of data to
                reinstall if any errors occur to data or if data is otherwise corrupted or “hacked.”<br>
                Snapshots: $1/GB of data/server. Snapshots are manual instances taken of a server that can be backed up. They can occur
                more frequently i.e. every 30 minutes, every 2 hour, every day, etc… ($100 Setup Fee)<br>
                Load Balancers: $100/mos. Load Balancers are a readily available, fully-managed service that work right out of the box
                and can be deployed in minutes. Load Balancers distribute incoming traffic across a licensee’s infrastructure to
                increase the application's availability. Perform health checks on your Droplets to ensure they remain responsive and the
                application achieves 100% uptime. Load Balancers are easily managed remotely by Licensor. Support for multiple protocols
                includes HTTP, HTTPS, TCP, and managed TLS certificates. ($100 Setup Fee)<br>
                Additional Ram: Variable Price. Requires custom quote.
                </p> <p><b>Digital Marketing Upgrades</b>
                
                SMS Marketing: Send messages to customers to remind them of a new menu, new updates, or confirmation on an order they
                just made.<br><br>
                
                SMS/MMS - One-time setup fee of $100, plus $0.07c per SMS message and $0.08c per MMS message. Each message has a cap of
                140 characters. You will be charged an additional 10% for the total amount of messages you send in order to cover costs
                for potential incoming messages.<br><br>
                
                Inbound &amp;Outbound Phone Line: Custom Phone lines are $8.50 per month per line or an annual rate of $100 per line per
                year.<br><br>
                
                PPC Advertising (Paid Ads FB/IG + Google Adwords): Create and optimize inbound advertising campaign, manage content,
                improve relevant user/keyword, relevant landing, and relevant messaging (does not include ad budget). See pricing levels
                below<br><br>
                
                Facebook/Instagram LVL I –500<br>
                Facebook/Instagram LVL II – LVL I +$500<br>
                Facebook/Instagram LVL III – LVL II +$500<br><br>
                
                Google Adwords LVL I – $500<br>
                Google Adwords LVL II – LVL I +$500<br>
                Google Adwords LVL III – LVL I I+$500<br><br>
                
                Remarketing LVL I - $500<br>
                Remarketing LVL II – LVL I + $500<br> <br>
                Email Marketing: Manage frequency campaigns with email content. Improve conversion rate and click through rate via A|B
                split testing campaign, integrate with paid ads and blogging at higher LVL.
                <br><br>
                Email Marketing LVL I – $500<br>
                Email Marketing LVL II – LVL I+$500<br>
                Email Marketing LVL III – LVL II +$500<br> <br> <br>
                Blogging: $500 per 20 articles for Search Engine Optimization (scheduled posts between 2-3 months). Advanced blogging
                integration with PPC and Email marketing pricing below.
                <br><br>
                Blogging LVL I - $500<br>
                Blogging LVL II – LVL 1 + $500<br><br></p> <p>Additional Terms (if any):</p></div></div></div></section></div> <div id="policy" role="tabpanel" aria-labelledby="policy-tab" class="tab-pane fade px-4 py-5"><section><div class="container"><div class="row"><div class="col-12"><h1>Sprwt Acceptable Use Policy</h1> <small>Last Modified: July 30, 2018</small></div></div> <div class="row"><div class="col-12 mt-5"><p>This Sprwt Acceptable Use Policy ("AUP") applies to the use of any product, service or website provided by us (Sprwt),
                whether we provide it directly or use another party to provide it to you (each, a "Sprwt Service"). This AUP is designed
                to ensure compliance with the laws and regulations that apply to the Sprwt Service. This AUP also protects the interests
                of all of our clients and their customers, as well as our goodwill and reputation. These terms are so important that we
                cannot provide the Sprwt Service unless you agree to them. By using the Sprwt Service, you are agreeing to these terms.
                </p> <p>If you are using any Sprwt Service, this AUP applies to you. Every client of ours agrees to abide by this AUP and is
                responsible for any violations. You are not allowed to assist or engage others in a way that would violate this AUP. We
                will enforce and ensure compliance with this AUP by using methods we consider to be appropriate, such as complaint and
                email failure monitoring.</p> <p>We periodically update these terms and we will let you know when we do through the Notification app in the Sprwt portal
                used to access your Sprwt subscription (if you have one), or by posting a revised copy on our website. You agree to
                review the AUP on a regular basis and always remain in compliance.
                </p> <h4>1. Reporting Suspected Violations</h4> <p>We encourage recipients of email messages sent using the Sprwt Service to report suspected violations of this AUP to us
                by forwarding a copy of the received email with FULL headers to support@sprwt.io. We have a policy to investigate all of
                these reports and to respond in the way we consider appropriate.
                </p> <p>If you know of or suspect a violation of this AUP, you will promptly notify us in writing of the known or suspected
                violation of this AUP.
                </p> <h4>2. No SPAM Permitted</h4> <p>You may not use the Sprwt Service in any way (directly or indirectly) to send, transmit, handle, distribute or deliver:
                (a) unsolicited email ("spam" or "spamming") in violation of the CAN-SPAM Act (referenced below) or any other law; (b)
                email to an address obtained via Internet harvesting methods or any surreptitious methods (e.g., scraping or
                harvesting); (c) email to an address that is incomplete, inaccurate and/or not updated for all applicable opt-out
                notifications, using best efforts and best practices in the industry, or (d) commercial electronic messages in violation
                of Canada’s Anti-Spam Legislation (referenced below).
                </p> <h4>3. Prohibited Email Content and Formatting; Email Best Practices</h4> <p>Email sent, or caused to be sent to or through the Sprwt Service may not: (a) use or contain invalid or forged headers;
                (b) use or contain invalid or non-existent domain names; (c) employ any technique to otherwise misrepresent, hide or
                obscure any information in identifying the point of origin or the transmission path; (d) use other means of deceptive
                addressing; (e) use a third party's internet domain name without their consent, or be relayed from or through a third
                party's equipment without the third party’s permission; (f) contain false or misleading information in the subject line
                or otherwise contain false or misleading content; or (g) use our trademark(s), tagline(s), or logo(s) without our prior
                written consent and only then pursuant to our trademark usage guidelines.
                </p> <p>If you use email, we recommend that you adopt the Messaging, Malware and Mobile Anti-Abuse Working Group (M3AAWG) Sender
                Best Communications Practices (BCP), which were created and agreed upon with collaborative input from both volume email
                senders and Internet Service Providers. The Sender Best Communications Practices document is available at
                https://www.m3aawg.org/sites/default/files/document/M3AAWG_Senders_BCP_Ver3-2015-02.pdf. You will use commercially
                reasonable efforts to follow these practices.
                </p> <p>In addition, you are prohibited from using the Sprwt Service to email: (a) purchased, rented, or borrowed lists, and (b)
                lists that are likely to result in an excessive number of unsubscribe requests or SPAM complaints or notices, as
                determined by acceptable industry practices.
                </p> <h4>4. Email Opt-out Requirements</h4> <p>You warrant that each email you send or is sent for you using the Sprwt Service will contain: (a) header information
                that is not false or misleading; and (b) an advisement that the recipient may unsubscribe, opt-out or otherwise demand
                that use of its information for unsolicited, impermissible and/or inappropriate communication(s) as described in this
                AUP be stopped (and how the recipient can notify you that it wants to unsubscribe, opt-out, or stop this use of its
                information). These requirements may not apply if the email sent is a transactional email and these requirements are not
                otherwise required by law. You warrant that you will promptly comply with all opt-out, unsubscribe, "do not call" and
                "do not send" requests.</p> <h4>5. Telephone Marketing</h4> <p>You must comply with all laws relating to telephone marketing, including without limitation those specifically
                referenced in the ‘Proper Usage of Sprwt Service’ section below. You must comply with all laws related to the recording
                of phone calls and ensure all proper consent to record is obtained prior to making any such recording. If you use the
                Sprwt Service to place telephone calls, you must also comply with all applicable industry standards, including those
                applicable guidelines published by the CTIA and the Mobile Marketing Association. You are prohibited from using or
                permitting access to use the Sprwt Service to make emergency calls or to provide or seek emergency services.
                </p> <h4>6. No Disruption</h4> <p>You agree not to use the Sprwt Service in a way that impacts the normal operation, privacy, integrity or security of
                another's property. Another’s property includes another’s account(s), domain name(s), URL(s), website(s), network(s),
                system(s), facilities, equipment, data, other information, or business operations. You also agree not to use the Sprwt
                Service to gain unauthorized access to, use, monitor, make an unauthorized reference to, another’s property, unless you
                have the appropriate express prior consent to do so. Examples of prohibited actions include (without limitation):
                hacking, spoofing, denial of service, mailbombing and/or sending any email that contains or transmits any virus or
                propagating worm(s), or any malware, whether spyware, adware or other such file or program. You also agree not to use
                the Sprwt Service in a way that causes or may cause any Sprwt IP addresses, Sprwt domains, or Sprwt customer domains to
                be blacklisted. These restrictions apply regardless of your intent and whether or not you act intentionally or
                unintentionally.</p> <h4>7. Proper Usage of the Sprwt Service</h4> <p>You will respect the limits that apply to your use the Sprwt Service as specified in the products and features pages on
                our site. We may update or change these Service Limits by updating our site, so we encourage you to review this page
                periodically.</p> <p>In addition, and without limiting the other requirements in this AUP, you may not (directly or indirectly) use the Sprwt
                Service with content, or in a manner that:</p> <br>
                is threatening, abusive, harassing, stalking, or defamatory;<br>
                is deceptive, false, misleading or fraudulent;<br>
                is invasive of another's privacy or otherwise violates another’s legal rights (such as rights of privacy and publicity);<br>
                contains vulgar, obscene, indecent or unlawful material;<br>
                infringes a third party's intellectual property right(s);<br>
                publishes, posts, uploads, or otherwise distributes any software, music, videos, or other material protected by
                intellectual property laws (or by rights of privacy or publicity) unless you have all necessary rights and consents to
                do so;<br>
                uploads files that contain viruses, corrupted files, or any other similar software or programs that may damage the
                operation of another person's computer;<br>
                downloads any file that you know, or reasonably should know, cannot be legally distributed in that way;<br>
                falsifies or deletes any author attributions, legal or proprietary designations, labels of the origin or source of
                software, or other material contained in a file that is uploaded;<br>
                restricts or inhibits any other user of the Sprwt Service from using and enjoying our website and/or the Sprwt Service;<br>
                harvests or otherwise collects information about others, including e-mail addresses, without their consent; <br>violates the
                usage standards or rules of an entity affected by your use, including without limitation any ISP, ESP, or news or user
                group (and including by way of example and not limitation circumventing or exceeding equipment use rights and
                restrictions and/or location and path identification detail); <br>is legally actionable between private parties; <br>is not a
                good faith use of the service, such as uploading Contacts in excess of your Contact tier, emailing those Contacts and
                then purging them shortly thereafter; <br>and/or is in violation of any applicable local, state, national or international
                law or regulation, including all export laws and regulations and without limitation the Controlling the Assault of
                Non-Solicited Pornography and Marketing Act (CAN-SPAM Act) (15 U.S.C. § 7701 et seq.), the U.S Telephone Consumer
                Protection Act of 1991 (47 U.S.C. § 227), the Do-Not-Call Implementation Act of 2003 (15 U.S.C. § 6152 et seq.;<br>
                originally codified at § 6101 note), the Directive 2000/31/EC of the European Parliament and Council of 8 June 2000, on
                certain legal aspects of information society services, in particular, electronic commerce in the Internal Market
                ('Directive on Electronic Commerce'), along with the Directive 2002/58/EC of the European Parliament and Council of 12
                July 2002, concerning the processing of personal data and the protection of privacy in the electronic communications
                sector ('Directive on Privacy and Electronic Communications'), regulations promulgated by the U.S. Securities Exchange
                Commission, any rules of national or other securities exchange, including without limitation, the New York Stock
                Exchange, the American Stock Exchange or the NASDAQ, the Personal Information Protection and Electronic Documents Act
                (PIPEDA) (S.C. 2000, c. 5), Canada’s Anti-Spam Legislation (CASL) (S.C. 2010, c. 23), Japan’s Act on Regulation of
                Transmission of Specified Electronic Mail (Act No. 26 of April 17, 2002) and any regulations having the force of law or
                laws in force in your or your email recipient's country of residence.<br> <br> <p>If you use our Application Programming Interfaces (APIs), developer tools, or associated software, you will comply with
                our API Terms</p> <p>You will use the Sprwt Service for your internal business purposes and will not: <br>
                (i) willfully tamper with the security of the Sprwt Service or tamper with our customer accounts;<br>
                (ii) access data on the Sprwt Service not intended for you;<br>
                (iii) log into a server or account on the Sprwt Service that you are not authorized to access;<br>
                (iv) attempt to probe, scan or test the vulnerability of any Sprwt Service or to breach the security or authentication measures without proper
                authorization;<br>
                (v) willfully render any part of the Sprwt Service unusable;<br>
                (vi) lease, distribute, license, sell or otherwise commercially exploit the Sprwt Service or make the Sprwt Service available to a third party other than as
                contemplated in your subscription to the Sprwt Service;<br>
                (vii) use the Sprwt Service for timesharing or service bureau purposes or otherwise for the benefit of a third party; or (viii) provide to third parties any evaluation version of the
                Sprwt Service without our prior written consent.</p> <h4>8. Sprwt Trademark Use</h4> <p>Unless you have our express prior written permission, you may not use any name, logo, tagline or other mark of ours or
                the Sprwt Service, or any identifier or tag generated by the Sprwt Service, including without limitation: (a) as a
                hypertext link to any website or other location (except as provided for or enabled expressly by us); or (b) to imply
                identification with us as an employee, contractor, agent or other similar representative capacity. You also agree not to
                remove or alter any of these items as we may have provided or enabled.
                </p> <h4>9. General Terms</h4> <p>We may immediately suspend your access to the Sprwt Service if you breach this AUP or don’t respond to us in a
                reasonable period after we’ve contacted you about a potential breach of this AUP. We may also suspend your access as we
                explain in our Customer Terms of Service and, if you breach this AUP, we may terminate your subscription agreement for
                cause. You acknowledge we may disclose information regarding your use of any Sprwt Service to satisfy any law,
                regulation, government request, court order, subpoena or other legal process. If we make this type of required
                disclosure we will notify you, unless we are required to keep the disclosure confidential.
                </p> <p>We are not obligated to, but may choose to, remove any prohibited materials and deny access to any person who violates
                this AUP. We further reserve all other rights.
                </p> <p>We may update and change any part or all of this AUP. If we update or change this AUP, the updated AUP will be posted at
                https://sprwt.io/terms. If you have a Sprwt subscription, we will we will let you know through the
                Notification app in the Sprwt portal used to access your Sprwt subscription. If you do not have a Sprwt subscription, we
                will let you know by posting the revised copy on our website. The updated AUP will become effective and binding on the
                next business day after it is posted. When we change this AUP, the "Last Modified" date above will be updated to reflect
                the date of the most recent version. We encourage you to review this AUP periodically.</p></div></div></div></section></div> <div id="web" role="tabpanel" aria-labelledby="web-tab" class="tab-pane fade px-4 py-5"><section><div class="container"><div class="row"><div class="col-12"><h1>Sprwt Website Terms of Use</h1> <small>Last Modified: July 30, 2018</small></div></div> <div class="row"><div class="col-12 mt-5"><p>Sprwt Inc. ("Sprwt") operates each website ("Site") that links to these Terms of Use to provide online access to
                information about Sprwt and the products, services, and opportunities we provide. Use of the Sprwt Service is governed
                by our Customer Terms of Service, available at https://sprwt.io/terms/
                <br><br>
                By accessing and using the Site, you agree to these Terms of Use.
                <br><br>
                Sprwt reserves the right to modify these Terms of Use at any time without giving you prior notice. Your use of the Site
                following any such modification constitutes your agreement to follow and be bound by these Terms of Use as modified. The
                last date these Terms of Use were revised is set forth below.
                <br><br>
                1. Permitted Use of The Site
                <br><br>
                You may use the Site, and the information, writings, images and/or other works that you see, hear or otherwise
                experience on the Site (singly or collectively, the "Content") solely for your non-commercial, personal purposes and/or
                to learn about Sprwt products and services, and solely in compliance with these Terms of Use.
                <br><br>
                2. Prohibited Use of The Site
                <br><br>
                By accessing the Site, you agree that you will not:
                <br><br>
                Use the Site in violation of these Terms of Use;<br>&gt;
                Use the Site in violation of the terms of Sprwt's Acceptable Use Policy at https://sprwt.io/terms/.<br>
                Copy, modify, create a derivative work from, reverse engineer or reverse assemble the Site, or otherwise attempt to
                discover any source code, or allow any third party to do so;<br>
                Sell, assign, sublicense, distribute, commercially exploit, grant a security interest in or otherwise transfer any right
                in, or make available to a third party, the Content or Service in any way;<br> <br><br>
                Use or launch any automated system, including without limitation, "robots," "spiders," or "offline readers," that
                accesses the Site in a manner that sends more request messages to the Sprwt servers in a given period of time than a
                human can reasonably produce in the same period by using a conventional on-line web browser;<br> <br><br>
                Use the Site in any manner that damages, disables, overburdens, or impairs any Sprwt website or interferes with any
                other party's use and enjoyment of the Site;<br>
                Mirror or frame the Site or any part of it on any other web site or web page.<br>
                Attempt to gain unauthorized access to the Site;<br>
                Access the Site by any means other than through the interface that is provided by Sprwt for use in accessing the Site;<br> <br>
                Use the Site for any purpose or in any manner that is unlawful or prohibited by this Agreement.
                <br>
                Any unauthorized use of any Content or the Site may violate patent, copyright, trademark, and other laws.
                <br>
                3. Copyrights and Trademarks
                <br>
                The Site is based upon proprietary Sprwt technology and includes the Content. The Site is protected by applicable
                intellectual property and other laws, including trademark and copyright laws. The Site, including all intellectual
                property rights in the Site, belongs to and is the property of Sprwt or its licensors (if any). Sprwt owns and retains
                all copyrights in the Content. Except as specifically permitted on the Site as to certain Content, the Content may not
                be copied, reproduced, modified, published, uploaded, posted, transmitted, performed, or distributed in any way, and you
                agree not to modify, rent, lease, loan, sell, distribute, transmit, broadcast, or create derivative works based on the
                Content or the Site, in whole or in part, by any means. Sprwt, the Sprocket Design, the Sprwt logos, and other marks
                used by Sprwt from time to time are trademarks and the property of Sprwt. The appearance, layout, color scheme, and
                design of the Sprwt.io site are protected trade dress. Customer does not receive any right or license to use the
                foregoing. Sprwt may use and incorporate into the Site or the Sprwt Service any suggestions or other feedback you
                provide, without payment or condition.<br> <br>
                Pursuant to Title 17, United States Code, Section 512(c)(2), notifications of claimed copyright infringement on the Site
                or the Service should be sent to Sprwt's designated Copyright Agent. See the Claims of Copyright Infringement
                instructions below.<br> <br>
                4. Information and Materials You Post or Provide to Sprwt
                <br><br>
                You represent that you have all right, title, and interest to materials you post on the Site or provide to Sprwt
                ("Materials"), including but not limited to any consent, authorization, release, clearance or license from any third
                party (such as, but not limited to, any release related to rights of privacy or publicity) necessary for you to provide,
                post, upload, input or submit the Materials, and that posting such Materials does not violate or constitute the
                infringement of any patent, copyright, trademark, trade secret, right of privacy, right of publicity, moral rights, or
                other intellectual property right recognized by any applicable jurisdiction of any person or entity, or otherwise
                constitute the breach of any agreement with any other person or entity. You further represent and warrant that you are
                who you say you are, that you have not submitted fictitious, false or inaccurate information about yourself, and that
                all information contained in the posted Materials is true and your own work or work you are authorized to submit, and
                that the posted Materials do not contain any threatening, harassing, libelous, false, defamatory, offensive, obscene, or
                pornographic, material, or other material that would violate any other applicable law or regulation. You agree that you
                will not knowingly and with intent to defraud provide material and misleading information. You represent and warrant
                that the Materials you supply do not violate these Terms of Use.
                <br><br>
                Sprwt does not claim ownership of, verify or control over the Content generated, provided or otherwise submitted by the
                Clients to Sprwt in the course of using the Website, Software and Services. The Clients and other users are solely
                responsible for all Content they generate and submit to Sprwt and Sprwt accepts no liability whatsoever for such
                content. Sprwt uses such Content to provide the Services only.
                By providing their Content to Sprwt, the Clients grant Sprwt a world-wide, royalty free, non-exclusive license to copy,
                distribute, transmit, display, reproduce, edit, translate, perform and reformat Content in order to provide the
                Services.<br>
                Sprwt reserves the right at its sole discretion to remove, screen or edit without notice any Content at any time and for
                any reason.<br>
                By using the Services you agree not to generate any Content that is offensive, threatening, promotes violence, promotes
                any illegal activity, is obscene, defaming, pornographic or otherwise harmful, represents unauthorized copies or
                distributions of copyrighted work or other intellectual property or is contrary to law or otherwise objectionable.
                Sprwt has the right to disclose your identity to legal institutions.
                Sprwt will not be responsible, or liable to any third party, for the content or accuracy of any content posted by you on
                the Website.<br>
                Sprwt has the right to remove any posting you make on the Website if, in its opinion, your post does not comply with the
                content standards set out in these Terms.<br>
                Sprwt applies retention periods for the Content submitted by the Clients as stipulated in the Privacy Policy.
                
                <br><br>
                5. Links to Third-Party Web Sites
                <br><br>
                Links on the Site to third party web sites or information are provided solely as a convenience to you. If you use these
                links, you will leave the Site. Such links do not constitute or imply an endorsement, sponsorship, or recommendation by
                Sprwt of the third party, the third-party web site, or the information there. Sprwt is not responsible for the
                availability of any such web sites. Sprwt is not responsible or liable for any such web sites or the content thereon. If
                you use the links to the web sites of Sprwt affiliates or service providers, you will leave the Site and will be subject
                to the terms of use and privacy policy applicable to those web sites.
                <br><br>
                
                6. Downloading Files<br> <br>
                Sprwt cannot and does not guarantee or warrant that files available for downloading through the Site will be free of
                infection by software viruses or other harmful computer code, files or programs.<br> <br>
                7. Disclaimers; Limitations of Liability<br> <br>
                SPRWT AND ITS SERVICE PROVIDERS, LICENSORS AND SUPPLIERS MAKE NO REPRESENTATIONS ABOUT THE SUITABILITY, RELIABILITY,
                AVAILABILITY, TIMELINESS, SECURITY OR ACCURACY OF THE SITE OR THE CONTENT FOR ANY PURPOSE. TO THE MAXIMUM EXTENT
                PERMITTED BY APPLICABLE LAW, ALL SUCH INFORMATION, SOFTWARE, PRODUCTS, SERVICE AND RELATED GRAPHICS ARE PROVIDED "AS IS"
                WITHOUT WARRANTY OR CONDITION OF ANY KIND. SPRWT AND ITS SERVICE PROVIDERS, LICENSORS AND SUPPLIERS HEREBY DISCLAIM ALL
                WARRANTIES AND CONDITIONS OF ANY KIND WITH REGARD TO THE SITE AND THE CONTENT, INCLUDING ALL IMPLIED WARRANTIES OR
                CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. NO STATEMENT OR
                INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED FROM SPRWT IN ANY MEANS OR FASHION SHALL CREATE ANY WARRANTY NOT
                EXPRESSLY AND EXPLICITLY SET FORTH IN THIS AGREEMENT. THE CONTENT MAY INCLUDE INACCURACIES OR TYPOGRAPHICAL ERRORS.
                <br><br>
                TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL SPRWT AND ITS SERVICE PROVIDERS, LICENSORS OR
                SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL, EXEMPLARY OR OTHER TYPE OF
                DAMAGES, INCLUDING WITHOUT LIMITATION DAMAGES FOR COVER OR LOSS OF USE, DATA, REVENUE OR PROFITS, ARISING OUT OF OR IN
                ANY WAY CONNECTED WITH THE USE OR PERFORMANCE OF THE SITE, WITH THE DELAY OR INABILITY TO USE THE SITE, OR FOR ANY
                CONTENT, OR OTHERWISE ARISING OUT OF THE USE OF THE SITE, WHETHER BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY,
                THE FAILURE OF ANY LIMITED REMEDY TO ACHIEVE ITS ESSENTIAL PURPOSE, OR OTHERWISE, EVEN IF SPRWT OR ANY OF SPRWT'S
                SUPPLIERS HAS BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION
                OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU.
                <br><br>
                IF, NOTWITHSTANDING THE OTHER TERMS OF THIS AGREEMENT, SPRWT IS DETERMINED TO HAVE ANY LIABILITY TO YOU OR ANY THIRD
                PARTY FOR ANY LOSS, HARM OR DAMAGE, YOU AGREE THAT THE AGGREGATE LIABILITY OF SPRWT AND ITS OFFICERS, DIRECTORS,
                MANAGERS, EMPLOYEES, AFFILIATES, AGENTS, CONTRACTORS, SERVICE PROVIDERS, LICENSORS OR SUPPLIERS SHALL IN ALL CASES BE
                LIMITED TO ONE HUNDRED DOLLARS.
                <br><br>
                A. THE SOFTWARE IS LICENSED (NOT SOLD). It is licensed to licensees without either express or implied warranties of any
                kind, on an “as is” basis. Sprwt makes no express or implied warranties to Licensee with regard to the Software, as to
                its performance, merchantability, fitness for any purpose or non-infringement of patents, copyrights or other
                proprietary rights of others.
                <br><br>
                B. Neither Sprwt nor anyone else who has been involved in the creation, production, testing or delivery of this software
                shall be liable for any direct, incidental or consequential damages, such as, but not limited to, loss of profits or
                benefits, resulting from the Use of the Software and/or arising out of any breach of any warranty, any server downtime
                and/or any Software “glitches.” If any of the provisions of this Agreement, or portions thereof, are invalid and/or
                unenforceable under any statute, regulation or other rule of law, they are to be deemed omitted from this Agreement, to
                the extent they are invalid and/or unenforceable. If any part of this Agreement is found void and unenforceable, it will
                not affect the validity of the balance of the Agreement, which balance shall remain valid and enforceable according to
                its terms.
                <br><br>
                C. In no event shall Sprwt be liable to Licensee if the Software and/or any server on which Licensee’s data is stored
                gets corrupted, infected by a virus, becomes the target of so-called malware, ransomware, keystroke logging, “hacking”
                and/or any other unauthorized and/or malicious access, denial of access, use, or other improper actions by a third
                party. If such an adverse event does occur, Licensee agrees to indemnify Licensor against all claims, demands, damages,
                losses, causes of action and the like which may arise from and/or relate to any such adverse event.
                <br><br>
                D. Licensee acknowledges that the Software requires Licensee’s use of “Stripe” (individually and collectively “Merchant
                Account”) for the financial transactions on Licensee’s website, and that account crediting, refunds, fraud protections
                and the like for Licensee’s Merchant Account are controlled by the policies and procedures of those companies and
                Licensee’s bank(s). Licensee agrees to look solely to Licensee’s Merchant Account, Licensee’s bank(s) and/or Licensee’s
                customer(s) with regard to any issues that may arise concerning those accounts and/or Licensee’s customer transactions,
                including but not limited to payment processing, credits, refunds, and fraudulent purchases. Further, Licensee agrees to
                indemnify Licensor against all claims, demands, damages, losses, causes of action and the like which may arise from
                and/or relate to any transaction(s) and/or attempted transaction(s) by, with and/or through Licensee’s Merchant Account
                and/or Licensee’s bank(s).
                <br><br>
                E. Sprwt is not liable for any indirect, special, incidental or consequential damages arising out of or related to this
                agreement (including, without limitation, costs of delay; loss of data, records or information; and lost profits), even
                if it knows of the possibility of such damage or loss.
                <br><br>
                Sprwt’s total liability arising out of or related to this agreement (whether in contract, tort or otherwise) does not
                exceed the amount paid by Customer within the 3-month period prior to the event that gave rise to the liability.
                
                There is a $500 non-refundable installation fee for any servers that are active but taken down due to cancellation. Any
                cancelled accounts that wants to sign up again and there is no active data or information pertaining to the old site
                will have to pay a non-refundable $2000 setup fee.
                
                
                9. Indemnification
                
                You understand and agree that you are personally responsible for your behavior on the Site. You agree to indemnify,
                defend and hold harmless Sprwt, its parent companies, subsidiaries, affiliated companies, joint venturers, business
                partners, licensors, employees, agents, and any third-party information providers from and against all claims, losses,
                expenses, damages and costs (including, but not limited to, direct, incidental, consequential, exemplary and indirect
                damages), and reasonable attorneys' fees, resulting from or arising out of your use, misuse, or inability to use the
                Site or the Content, or any violation by you of these Terms of Use.
                <br><br>
                9. Privacy
                <br><br>
                Your use of the Site is subject to Sprwt's Privacy Policy, available at https://sprwt.io/terms
                <br><br>
                10. Additional Terms of Service
                <br><br>
                If you are a customer of Sprwt or an employee, representative or agent of a Sprwt customer, your use of the Sprwt
                Service is subject to Sprwt's, available at https://sprwt.io/terms/
                <br><br><br><br>
                11. General Provisions
                <br><br>
                a. Entire Agreement/No Waiver. These Terms of Use constitute the entire agreement of the parties with respect to the
                subject matter hereof. No waiver by Sprwt of any breach or default hereunder shall be deemed to be a waiver of any
                preceding or subsequent breach or default.
                <br><br>
                b. Correction of Errors and Inaccuracies. The Content may contain typographical errors or other errors or inaccuracies
                and may not be complete or current. Sprwt therefore reserves the right to correct any errors, inaccuracies or omissions
                and to change or update the Content at any time without prior notice. Sprwt does not, however, guarantee that any
                errors, inaccuracies or omissions will be corrected.
                <br><br>
                c. Enforcement/ Choice of Law/ Choice of Forum. If any part of these Terms of Use is determined by a court of competent
                jurisdiction to be invalid or unenforceable, it will not impact any other provision of these Terms of Use, all of which
                will remain in full force and effect. Any and all disputes relating to these Terms of Use, Sprwt's Privacy Policy, your
                use of the Site, any other Sprwt web site or the Content are governed by, and will be interpreted in accordance with,
                the laws of the Commonwealth of Massachusetts, without regard to any conflict of laws provisions. You agree to the sole
                and exclusive jurisdiction and venue of the federal or state courts in Boston, Massachusetts in the event of any dispute
                of any kind arising from or relating to these Terms of Use, Sprwt's Privacy Policy, your use of the Site, any other
                Sprwt web site or the Content
                <br><br>
                
                *****************************
                <br><br>
                Claims of Copyright Infringement
                <br><br>
                DMCA Notices
                <br><br><br><br>
                Sprwt respects the intellectual property rights of others, and we ask our users to do the same. Sprwt may, in its sole
                discretion, suspend the access or terminate the accounts of users who violate others' intellectual property rights.
                <br><br>
                If you believe that your work has been copied in a way that constitutes infringement on Sprwt's website, please provide
                the following information to Sprwt's Copyright Agent.
                <br><br>
                Contact Sprwt:
                <br><br>
                The Sprwt Copyright Agent for notice of claims of copyright infringement on or relating to this website
                ("Notifications") can be reached either by sending an e-mail to support@Sprwt.io
                <br><br>
                
                
                Submission of Notification:
                <br><br>
                To be effective, the Notification must include the following:
                <br><br>
                a) A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is
                allegedly infringed ("Complaining Party");
                <br><br>
                b) Identification of the copyrighted work claimed to have been infringed, or if multiple copyrighted works at a single
                online site are covered by a single notification, a representative list of such works at that site;
                <br><br>
                c) Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that
                is to be removed or access to which is to be disabled, and information reasonably sufficient to permit Sprwt to locate
                the material;
                <br><br>
                d) Information reasonably sufficient to permit Sprwt to contact the Complaining Party, such as an address, telephone
                number, and if available, an electronic mail address at which the complaining party may be contacted;
                <br><br>
                e) A statement that the Complaining Party has a good faith belief that use of the material in the manner complained of
                is not authorized by the copyright owner, its agent, or the law; and
                <br><br>
                f) A statement that the information in the notification is accurate, and under penalty of perjury, that the Complaining
                Party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.
                <br><br>
                Receipt of Notification:
                <br><br>
                Upon receipt of the written Notification containing the information as outlined in 1 through 6:
                <br><br><br>
                a) Sprwt will remove or disable access to the material that is alleged to be infringing;
                <br><br>
                b) Sprwt will forward the written notification to such alleged infringer (the "Alleged Infringer");
                <br><br>
                c) Sprwt will take reasonable steps to promptly notify the Alleged Infringer that it has removed or disabled access to
                the material.
                <br><br>
                Counter Notification:
                <br><br>
                An Alleged Infringer may submit a Counter Notification to contest the claim of alleged infringement. To be effective, a
                Counter Notification must be a written communication provided to Sprwt's Copyright Agent that includes substantially the
                following:
                <br><br>
                a) A physical or electronic signature of the Alleged Infringer;
                <br><br>
                b) Identification of the material that has been removed or to which access has been disabled and the location at which
                the material appeared before it was removed or access to it was disabled;
                <br><br>
                c) A statement under penalty of perjury that the Alleged Infringer has a good faith belief that the material was removed
                or disabled as a result of mistake or misidentification of the material to be removed or disabled;
                <br><br>
                d) The Alleged Infringer's name, address, and telephone number, and a statement that the Alleged Infringer consents to
                the jurisdiction of Federal District Court for the judicial district in which the Alleged Infringer's address is
                located, or if the Alleged Infringer's address is outside of the United States, for any judicial district in which Sprwt
                may be found, and that the Alleged Infringer will accept service of process from the person who provided notification or
                an agent of such person.
                <br><br>
                Receipt of Counter Notification:
                <br><br>
                Upon receipt of a Counter Notification containing the information as outlined in 1 through 4 above:
                <br><br>
                a) Sprwt will promptly provide the Complaining Party with a copy of the Counter Notification;
                <br><br>
                b) Sprwt will inform the Complaining Party that it will replace the removed material or cease disabling access to it
                within ten (10) business days;
                <br><br>
                c) Sprwt will replace the removed material or cease disabling access to the material within ten (10) to fourteen (14)
                business days following receipt of the Counter Notification, provided Sprwt's Copyright Agent has not received notice
                from the Complaining Party that an action has been filed seeking a court order to restrain Alleged Infringer from
                engaging in infringing activity relating to the material on Sprwt's network or system.<br></p></div></div></div></section></div> <div id="cookie" role="tabpanel" aria-labelledby="cookie-tab" class="tab-pane fade px-4 py-5"><section><div class="container"><div class="row"><div class="col-12"><h1>Sprwt Cookie Policy</h1> <small>Last Modified: July 30, 2018</small></div></div> <div class="row"><div class="col-12 mt-5"><p>
                This Cookie Policy explains how Sprwt, Inc. and our affiliates use cookies and similar technologies to
                recognise you when you visit our Websites and when you use our Subscription Service (the Subscription
                Service), including our associated mobile applications (Mobile Apps) owned and controlled by Sprwt. It
                explains what these technologies are and why we use them, as well as your rights to control our use of
                them.</p> <p>What are cookies?</p> <p>A cookie is a small file containing a string of characters that is sent to your computer when you visit
                a website. When you visit the site again, the cookie allows that site to recognize your browser. Cookies
                may store user preferences and other information.</p> <p>Cookies provide a convenience feature to save you time, or tell the Web server that you have returned to
                a specific page. For example, if you personalize pages on our Websites, or register for the Subscription
                Service, a cookie helps us to recall your specific information on subsequent visits. When you return to
                the same Website, the information you previously provided can be retrieved, so you can easily use the
                customized features.</p> <p>Cookies set by the website owner (in this case, Sprwt) are called "first party cookies". Cookies set by
                parties other than the website owner are called "third party cookies". Third party cookies enable third
                party features or functionality to be provided on or through the website (e.g. like advertising,
                interactive content and analytics). The parties that set these third party cookies can recognise your
                computer both when it visits the website in question and also when it visits certain other websites.</p> <p>Why do we use cookies?</p> <p>We use first party and third party cookies for several reasons. Some cookies are required for technical
                reasons in order for our Websites to operate, and we refer to these as "essential" or "strictly
                necessary" cookies. Other cookies also enable us to track and target the interests of our users to
                enhance the experience on our Websites and Subscription Service. For example, Sprwt keeps track of the
                Websites and pages you visit within Sprwt, in order to determine what portion of the Sprwt Website or
                Subscription Service is the most popular or most used. This data is used to deliver customized content
                and promotions within the Sprwt Website and Subscription Service to customers whose behavior indicates
                that they are interested in a particular subject area. Third parties serve cookies through our Websites
                for advertising, analytics and other purposes. This is described in more detail below.</p> <p>What types of cookies do we use and how do we use them?</p> <p>The specific types of first and third party cookies served through our Websites and the purposes they
                perform. For a list of the cookies used by Sprwt, see this page. For a list of the cookies our customers
                can use via the Subscription Service, see this page. These cookies include:</p> <p>Essential website cookies: These cookies are strictly necessary to provide you with services available
                through our Websites, Subscription Service and Mobile Apps and to use some of its features, such as
                access to secure areas.<br>
                Performance and functionality cookies: These cookies are used to enhance the performance and
                functionality of our Websites, Subscription Service and Mobile Apps but are non-essential to their use.
                However, without these cookies, certain functionality may become unavailable.<br>
                Analytics and customization cookies: These cookies collect information that is used either in aggregate
                form to help us understand how our Websites, Subscription Service and Mobile Apps are being used or how
                effective our marketing campaigns are, or to help us customize our Websites for you.<br>
                Advertising cookies: These cookies are used to make advertising messages more relevant to you. They
                perform functions like preventing the same ad from continuously reappearing, ensuring that ads are
                properly displayed for advertisers, and in some cases selecting advertisements that are based on your
                interests.<br>
                Social networking cookies: These cookies are used to enable you to share pages and content that you find
                interesting on our Websites, Subscription Service or Mobile Apps through third party social networking
                and other websites. These cookies may also be used for advertising purposes too.</p> <p>How can I control cookies?</p> <p>You have the right to decide whether to accept or reject cookies. You can exercise your cookie
                preferences by clicking on the appropriate opt-out links provided below.</p> <p>You can set or amend your web browser controls to accept or refuse cookies. If you choose to reject
                cookies, you may still use our website though your access to some functionality and areas of our website
                may be restricted. As the means by which you can refuse cookies through your web browser controls vary
                from browser-to-browser, you should visit your browser's help menu for more information.</p> <p>In addition, most advertising networks offer you a way to opt out of targeted advertising. If you would
                like to find out more information, please visit http://www.aboutads.info/choices/ or
                http://www.youronlinechoices.com. You may opt out by clicking here: http://preferences.truste.com/ (or
                if located in the European Union, by clicking here: http://www.youronlinechoices.eu/). Please note this
                does not opt you out of being served advertising. You will continue to receive generic advertisements.</p> <p>Essential website cookies: Because these cookies are strictly necessary to deliver the Websites to you,
                you cannot refuse them. You can block or delete them by changing your browser settings however, as
                described above.</p> <p>What about other tracking technologies, like web beacons?</p> <p>Cookies are not the only way to recognize or track visitors to a website. We employ a software
                technology called clear gifs (a.k.a. Web Beacons/Web Bugs), that help us better manage the Website and
                Subscription Service by informing us what content is effective. Clear gifs are tiny graphics with a
                unique identifier, similar in function to cookies, and are used to track the online movements of Web
                users. In contrast to cookies, which are stored on a user's computer hard drive, clear gifs are embedded
                invisibly on Web pages or in emails and are about the size of the period at the end of this sentence. We
                use clear gifs or pixels in our HTML-based emails to let us know which emails have been opened by
                recipients. This allows us to gauge the effectiveness of certain communications and the effectiveness of
                our marketing campaigns. We tie the information gathered by clear gifs in emails to our customers'
                Personal Information.</p> <p>Do you use Flash cookies or Local Shared Objects?</p> <p>The Adobe Flash Player (and similar applications) use technology to remember settings, preferences and
                usage similar to browser cookies but these are managed through a different interface than the one
                provided by your Web browser. This technology creates locally stored objects that are often referred to
                as "Flash cookies." Sprwt does not use Flash cookies. However, our customers of our software platform
                may create pages on the Sprwt platform that employ Adobe Flash cookies. Sprwt does not have access or
                control over our customers' Flash cookies, but you may access your Flash management tools from Adobe's
                web site directly here.</p> <p>Similarly, our customers may create pages using the Sprwt Subscription Service that use technology from
                tracking utility companies, such as cookies and web beacons. The use of these technologies by our
                customers is not covered by our Cookie Policy or Privacy Notice. We do not have control over third party
                cookies or trackers our customers use.</p> <p>Do you serve targeted advertising?</p> <p>Third parties may serve cookies on your computer or mobile device to serve advertising through our
                Websites. These companies may use information about your visits to this and other websites in order to
                provide relevant advertisements about goods and services that you may be interested in. They may also
                employ technology that is used to measure the effectiveness of advertisements. This can be accomplished
                by them using cookies or web beacons to collect information about your visits to this and other sites in
                order to provide relevant advertisements about goods and services of potential interest to you. The
                information collected through this process does not enable us or them to identify your name, contact
                details or other personally identifying details unless you choose to provide these.</p> <p>How often will you update this Cookie Policy?</p> <p>We may update this Cookie Policy from time to time in order to reflect, for example, changes to the
                cookies we use or for other operational, legal or regulatory reasons. Please therefore re-visit this
                Cookie Policy regularly to stay informed about our use of cookies and related technologies.</p> <p>The date at the top of this Cookie Policy indicates when it was last updated.</p> <p>Where can I get further information?</p> <p>If you have any questions about our use of cookies or other technologies, please email us at
                privacy@sprwt.io.Sprwt Cookie Policy
    </p></div></div></div></section></div> <div id="privacy" role="tabpanel" aria-labelledby="privacy-tab" class="tab-pane fade px-4 py-5"><section><div class="container"><div class="row"><div class="col-12"><h1>Privacy Policy</h1> <small>Last Updated on 05/13/2017. This privacy policy is effective immediately.</small></div></div> <div class="row"><div class="col-12 mt-5"><h4>Introduction</h4> <p>At sprwt.io, we respect your privacy and are committed to protecting it through our compliance with this policy.
                <br>
                This policy describes the types of information we may collect from you or that you may provide when you visit this
                website (our “Website”) and our practices for collecting, using, maintaining, protecting and disclosing that
                information.<br> <br>
                This policy applies to information we collect:<br>
                On this Website.<br>
                In e-mail, text and other electronic messages between you and this Website.<br>
                Through mobile and desktop applications you download from this Website, which provide dedicated non-browser-based
                interaction between you and this Website.<br>
                When you interact with our advertising and applications on third-party websites and services, if those applications or
                advertising include links to this policy.<br>
                It does not apply to information collected by:<br>
                us offline or through any other means, including on any other website operated by Company or any third party (including
                our affiliates and subsidiaries); or any third party (including our affiliates and subsidiaries), including through any application or content (including
                advertising) that may link to or be accessible from or on the Website.</p> <p>Please read this policy carefully to understand our policies and practices regarding your information and how we will
                treat it. If you do not agree with our policies and practices, your choice is not to use our Website. By accessing or
                using this Website, you agree to this privacy policy. This policy may change from time to time. Your continued use of
                this Website after we make changes is deemed to be acceptance of those changes, so please check the policy periodically
                for updates.</p> <p>
                Children Under The Age Of 13 <br>
                Our Website is not intended for children under 13 years of age. No one under age 13 may provide any personal information
                to or on the Website. We do not knowingly collect personal information from children under 13. If you are under 13, do
                not use or provide any information on this Website or on or through any of its features/register on the Website, make
                any purchases through the Website, use any of the interactive or public comment features of this Website or provide any
                information about yourself to us, including your name, address, telephone number, e-mail address or any screen name or
                user name you may use. If we learn we have collected or received personal information from a child under 13 without
                verification of parental consent, we will delete that information. If you believe we might have any information from or
                about a child under 13, please contact us via our contact us link.<br> <br><br>
                Information We Collect About You And How We Collect It<br><br>
                We collect several types of information from and about users of our Website, including information:<br>
                which you may be personally identified, such as name, postal address, e-mail address, telephone number or ANY OTHER
                INFORMATION THE WEBSITE COLLECTS THAT IS DEFINED AS PERSONAL OR PERSONALLY IDENTIFIABLE INFORMATION UNDER AN APPLICABLE
                LAW (“personal information”);<br>
                that is about you but individually does not identify you, and/or
                about your internet connection, the equipment you use to access our Website and usage details.<br>
                We collect this information:<br>
                Directly from you when you provide it to us.<br>
                Automatically as you navigate through the site. Information collected automatically may include usage details, IP
                addresses and information collected through cookies, web beacons and other tracking technologies.<br>
                From third parties, for example, our business partners.<br> <br><br>
                Information You Provide To Us.<br>
                The information we collect on or through our Website may include:<br><br>
                Information that you provide by filling in forms on our Website. This includes information provided at the time of
                registering to use our Website, subscribing to our service, posting material or requesting further services. We may also
                ask you for information when you report a problem with our Website.<br>
                Records and copies of your correspondence (including e-mail addresses), if you contact us.<br>
                Your responses to surveys that we might ask you to complete for research purposes.<br>
                Details of transactions you carry out through our Website and of the fulfillment of your orders. You may be required to
                provide financial information before placing an order through our Website.<br>
                Your search queries on the Website.<br>
                Information We Collect Through Automatic Data Collection Technologies.<br>
                As you navigate through and interact with our Website, we may use automatic data collection technologies to collect
                certain information about your equipment, browsing actions and patterns, including:<br>
                Details of your visits to our Website, including traffic data, location data, and other communication data and the
                resources that you access and use on the Website.<br>
                Information about your computer and internet connection, including your IP address, operating system and browser type.
                We also may use these technologies to collect information about your online activities over time and across third-party
                websites or other online services (behavioral tracking). The information we collect automatically is statistical data
                and does not include personal information, but we may maintain it or associate it with personal information we collect
                in other ways or receive from third parties. It helps us to improve our Website and to deliver a better and more
                personalized service, including by enabling us to:<br>
                Estimate our audience size and usage patterns.<br>
                Store information about your preferences, allowing us to customize our Website according to your individual interests.<br>
                Speed up your searches.<br>
                Recognize you when you return to our Website.<br>
                The technologies we use for this automatic data collection may include:<br>
                Cookies (or browser cookies). A cookie is a small file placed on the hard drive of your computer. You may refuse to
                accept browser cookies by activating the appropriate setting on your browser. However, if you select this setting you
                may be unable to access certain parts of our Website. Unless you have adjusted your browser setting so that it will
                refuse cookies, our system will issue cookies when you direct your browser to our Website.<br>
                Flash Cookies. Certain features of our Website may use local stored objects (or Flash cookies) to collect and store
                information about your preferences and navigation to, from and on our Website. Flash cookies are not managed by the same
                browser settings as are used for browser cookies.<br>
                Web Beacons. Pages of our the Website may contain small electronic files known as web beacons (also referred to as clear
                gifs. pixel tags and single-pixel gifs) that permit the Company, for example, to count users who have visited those
                pages or opened an e-mail and for other related website statistics (for example, recording the popularity of certain
                website content and verifying system and server integrity).<br>
                We do not collect personal Information automatically, but we may tie this information to personal information about you
                that we collect from other sources or you provide to us.<br> <br><br>
                Third-Party Use Of Cookies And Other Tracking Technologies.<br>
                Some content or applications, including advertisements, on the Website are served by third-parties, including
                advertisers, ad networks and servers, content providers and application providers. These third parties may use cookies
                alone or in conjunction with web beacons or other tracking technologies to collect information about you when you use
                our website. The information they collect may be associated with your personal information or they may collect
                information, including personal information, about your online activities over time and across different websites and
                other online services. They may use this information to provide you with interest-based (behavioral) advertising or
                other targeted content.<br> <br><br>
                We do not control these third parties’ tracking technologies or how they may be used. If you have any questions about an
                advertisement or other targeted content, you should contact the responsible provider directly.<br> <br><br>
                How We Use Your Information<br>
                We use information that we collect about you or that you provide to us, including any personal information:<br>
                To present our Website and its contents to you.<br>
                To provide you with information, products or services that you request from us.<br>
                To provide you with information about our services<br>&gt;
                To provide you with notices about your account/subscription, including expiration and renewal notices.<br>
                To carry out our obligations and enforce our rights arising from any contracts entered into between you and us,
                including for billing and collection.<br>
                To notify you about changes to our Website or any products or services we offer or provide though it.<br>
                To allow you to participate in interactive features on our Website.<br>
                In any other way we may describe when you provide the information.<br>
                To fulfill any purpose for which you provide it.<br>
                For any other purpose with your consent.<br>
                We may use the information we have collected from you to enable us to display advertisements to our advertisers’ target
                audiences. Even though we do not disclose your personal information for these purposes without your consent, if you
                click on or otherwise interact with an advertisement, the advertiser may assume that you meet its target criteria.
                <br><br>
                Disclosure Of Your Information<br>
                We may disclose aggregated information about our users, and information that does not identify any individual, without
                restriction.<br> <br>
                We may disclose personal information that we collect or you provide as described in this privacy policy:
                To our subsidiaries and affiliates.<br>
                To contractors, service providers and other third parties we use to support our business and who are bound by
                contractual obligations to keep personal information confidential and use it only for the purposes for which we disclose
                it to them.<br>
                To a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution or other
                sale or transfer of some or all of the Company’s assets, whether as a going concern or as part of bankruptcy,
                liquidation or similar proceeding, in which personal information held by the Company about our Website users is among
                the assets transferred.<br>
                To third parties to market their products or services to you if you have consented to these disclosures. We
                contractually require these third parties to keep personal information confidential and use it only for the purposes for
                which we disclose it to them.<br>
                To fulfill the purpose for which you provide it. For example, if you give us an e-mail address to use the “e-mail a
                friend” feature of our Website, we will transmit the contents of that e-mail and your e-mail address to the recipients.
                For any other purpose disclosed by us when you provide the information.
                With your consent.<br>
                We may also disclose your personal information:<br>
                To comply with any court order, law or legal process, including to respond to any government or regulatory request.<br>
                To enforce or apply our terms of use and other agreements, including for billing and collection purposes.<br>
                If we believe disclosure is necessary or appropriate to protect the rights, property, or safety of the Company, our
                customers or others. This includes exchanging information with other companies and organizations for the purposes of
                fraud protection and credit risk reduction.<br>
                
                Choices About How We Use And Disclose Your Information<br>
                We strive to provide you with choices regarding the personal information you provide to us. We have created mechanisms
                to provide you with the following control over your information:<br>
                Tracking Technologies and Advertising. You can set your browser to refuse all or some browser cookies, or to alert you
                when cookies are being sent. To learn how you can manage your Flash cookie settings, visit the Flash player settings
                page on Adobe’s website. If you disable or refuse cookies, please note that some parts of this site may then be
                inaccessible or not function properly.<br>
                We do not control third parties’ collection or use of your information to serve interest-based advertising. However
                these third parties may provide you with ways to choose not to have your information collected or used in this way. You
                can opt out of receiving targeted ads from members of the Network Advertising Initiative (“NAI”) on the NAI’s website.
                <br><br>
                Accessing And Correcting Your Information<br>
                You may send us an e-mail via our contact link to request access to, correct or delete any personal information that you
                have provided to us. We cannot delete your personal information except by also deleting your user account. We may not
                accommodate a request to change information if we believe the change would violate any law or legal requirement or cause
                the information to be incorrect.<br>
                If you delete your User Contributions from the Website, copies of your User Contributions may remain viewable in cached
                and archived pages, or might have been copied or stored by other Website users. Proper access and use of information
                provided on the Website, including User Contributions, is governed by our terms of use.<br> <br><br>
                Your California Privacy Rights<br>
                California Civil Code Section § 1798.83 permits users of our Website that are California residents to request certain
                information regarding our disclosure of personal information to third parties for their direct marketing purposes. To
                make such a request, please contact us via the Contact Us link.<br> <br><br>
                Data Security<br>
                We have implemented measures designed to secure your personal information from accidental loss and from unauthorized
                access, use, alteration and disclosure<br>
                The safety and security of your information also depends on you. Where we have given you (or where you have chosen) a
                password for access to certain parts of our Website, you are responsible for keeping this password confidential. We ask
                you not to share your password with anyone.<br>
                Unfortunately, the transmission of information via the internet is not completely secure. Although we do our best to
                protect your personal information, we cannot guarantee the security of your personal information transmitted to our
                Website. Any transmission of personal information is at your own risk. We are not responsible for circumvention of any
                privacy settings or security measures contained on the Website.<br> <br><br>
                Changes To Our Privacy Policy<br>
                It is our policy to post any changes we make to our privacy policy on this page. If we make material changes to how we
                treat our users’ personal information, we will notify you by e-mail to the e-mail address specified in your account
                and/or through a notice on the Website home page. The date the privacy policy was last revised is identified at the top
                of the page. You are responsible for ensuring we have an up-to-date active and deliverable e-mail address for you, and
                for periodically visiting our Website and this privacy policy to check for any changes.<br> <br><br>
                Contact Information<br>
                To ask questions or comment about this privacy policy and our privacy practices, contact via our Contact Us link.
                 <br></p></div></div></div></section></div></div></div></div></div>
		  
		  
		  
	 
	 </div>
	  
	  </div>
@endsection