@extends( 'front.layouts.innerapp' )
@section( 'content' )
<div class="blogmain">
  <div class="container-fluid ">
    <div class="row">
      <div class="col-sm-6">
        <div class="contushead">
          <h2> <span class="subtxt">Blog</span>
            GET UPDATE
            <span>HERE</span></h2>
        </div>
      </div>
    </div>

    <div class="row">
	  @foreach($posts as $post)
      <div class=" col-sm-12 col-md-12 col-lg-4">
        <div class="blogbox">
		  @if($post->image != '')
          	<img src="{{ asset('images/blogs/middle/'.$post->image) }}" alt="{{$post->title}}" />
		  @endif
          <div class="blgcapt">
            <h2>{{$post->title}}</h2>
            <p>{{$post->getExcerpt($post->content, 0, 100)}}</p>

            <div class="row">
              <div class="col-lg-6  blgauth">Created at: {{$post->created_at->format('M d, Y H:i')}}</div>
              <div class="col-lg-6 text-right"><a href="{{url('/blog/')}}/{{$post->slug}}">Read more <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
              </div>

            </div>


          </div>



        </div>


      </div>
	  @endforeach
    </div>
  </div>
</div>
@endsection