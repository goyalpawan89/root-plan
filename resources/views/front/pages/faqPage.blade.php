@extends('front.layouts.innerapp')
@section('content')	

<div class="contactus">
	  
	  <div class="container-fluid ">
	
		  <div class="row">
			 <div class="col-sm-6"> <div class="contushead">
		 <h2> <span class="subtxt">Faq</span>
			  How can we
			  <span>help?</span></h2>
		  <p class="mb-5">Find advice and awnsers from our support team fast or get in touch</p>

		  </div>
			  
			  
			
			  </div>
			  
			  
			  
		  </div>
		  
		  
		  <div class="row">
		  <div class="col-md-4">
			  
		    <div class="faqbox" faqtype="faqtype_1">
			  <div class="text-center"><img src="{{ asset('front/inner/img/icon1.png') }}" alt=""/></div>
			  <h3>{{$faqtypes[1]}}</h3>
			  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		    </div>
			  
			  </div>
			   <div class="col-md-4">
			  
		    <div class="faqbox" faqtype="faqtype_2">
			  <div class="text-center"><img src="{{ asset('front/inner/img/icon2.png') }}" alt=""/></div>
			  <h3>{{$faqtypes[2]}}</h3>
			  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		    </div>
			  
			  </div>
			   <div class="col-md-4">
			  
		    <div class="faqbox" faqtype="faqtype_3">
			  <div class="text-center"><img src="{{ asset('front/inner/img/icon3.png') }}" alt=""/></div>
			  <h3>{{$faqtypes[3]}}</h3>
			  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		    </div>
			  
			  </div>
		  
		  
		  </div>
		  
<div class="faqtab">
		  <h3>Faq's</h3>
		  
		  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			  @foreach($faqs as $faq)
  <div class="panel panel-default faqblocks faqtype_{{$faq->faqtype}}">
    <div class="panel-heading " role="tab" id="heading_{{$faq->id}}">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{$faq->id}}" aria-expanded="true" aria-controls="collapse_{{$faq->id}}">
          {{$faq->question}}
        </a>
      </h4>
    </div>
    <div id="collapse_{{$faq->id}}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading_{{$faq->id}}">
      <div class="panel-body">
        {!! $faq->answer !!}
      </div>
    </div>
  </div>
			  @endforeach
</div>
		  </div>		  
		  
	 
		  
		  
		  
		 <div class="quesbox">
		  
		  <h3 class="text-center">Didn't find and answer to your question?</h3>
		  
		  <p>Get in touch with us for details on additional services and questions</p>
			 <div class="text-center col-sm-12"><div class="schedule_btn"><a href="{{ url('/contactus') }}">Contact Us</a></div>
			 
		  </div>  
	 
	 </div>
	  
	  </div>
</div>
@endsection

@section('scripts')
<script>
	$(document).ready(function(){
		$('.faqbox').click(function(){
			var faqType = $(this).attr('faqtype');
			$('.faqblocks').addClass('d-none');
			$('.faqblocks.'+faqType).removeClass('d-none');
			$('.faqbox').removeClass('active');
			$(this).addClass('active');
		});
	});
</script>
@endsection