@extends('front.layouts.innerapp')
@section('content')
<div class="contactus">
  <div class="container-fluid ">
    <div class="row">
      <div class="col-sm-6">
        <div class="contushead">
          <h2> <span class="subtxt">Contact us</span> Get in touch <span>With us</span></h2>
          <p>Have a question about our software or not sure which plan is best for your company's needs? Feel free to send us a message by filling out the form and one of our agents will get back to you as soon as possible. We look forward to hearing from you! </br>
            If you'd like to schedule a demo of our product click the button below. </p>
        </div>
        <button type="button" class="bookademo mb-5">Book a Demo</button>
      </div>
    </div>
	@include('front.particals.flash-message')
{{ Form::open(array('url' => 'contactus', 'method' => 'post')) }}
    <div class="row contformdtl">
      <div class="col-sm-6">
        <div class="contform">
          <label>Full Name *</label>
          	<input type="text" name="fullname" class="form-control @error('fullname') is-invalid @enderror" placeholder="Enter your Name">
			@if($errors->has('fullname'))
				<div class="invalid-feedback ml-3">{{ $errors->first('fullname') }}</div>
			@endif
        </div>
      </div>
      <div class="col-sm-6">
        <div class="contform">
          <label>Email *</label>
          	<input type="email" name="email" class="form-control @error('email') is-invalid @enderror"  placeholder="Enter Email">
			@if($errors->has('email'))
				<div class="invalid-feedback ml-3">{{ $errors->first('email') }}</div>
			@endif
        </div>
      </div>
      <div class="col-sm-6">
        <div class="contform">
          <label>Website</label>
          	<input type="url" name="website" class="form-control @error('website') is-invalid @enderror"  placeholder="Website">
			@if($errors->has('website'))
				<div class="invalid-feedback ml-3">{{ $errors->first('website') }}</div>
			@endif
        </div>
      </div>
      <div class="col-sm-6">
        <div class="contform">
          <label >Phone</label>
          	<input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror"  placeholder="Enter Phone Number">
			@if($errors->has('phone'))
				<div class="invalid-feedback ml-3">{{ $errors->first('phone') }}</div>
			@endif
        </div>
      </div>
      <div class="col-sm-6">
        <div class="contform">
          <label >Subject *</label>
		  <select class="custom-select my-1 mr-sm-2 @error('subject') is-invalid @enderror" name="subject">
			<option value="">Select</option>
			<option value="Sales & Billing">Sales & Billing</option>
			<option value="Technical Support">Technical Support</option>
			<option value="Business Development">Business Development</option>
			<option value="General">General</option>
			<option value="Career">Career</option>
			<option value="Other">Other</option>
		  </select>
			@if($errors->has('subject'))
				<div class="invalid-feedback ml-3">{{ $errors->first('subject') }}</div>
			@endif
        </div>
      </div>
	  <div class="col-sm-6">
        <div class="contform">
          <label >How did you hear of us?</label>
          <select class="custom-select my-1 mr-sm-2 @error('howdidhear') is-invalid @enderror" name="howdidhear">
            <option value="">Select</option>
            <option value="Instagram">Instagram</option>
            <option value="Google Search">Google Search</option>
            <option value="Facebook Ad">Facebook Ad</option>
            <option value="Google Ad">Google Ad</option>
            <option value="Referral">Referral</option>
            <option value="Sprwt">Sprwt</option>
            <option value="Other">Other</option>
          </select>
			@if($errors->has('howdidhear'))
				<div class="invalid-feedback ml-3">{{ $errors->first('howdidhear') }}</div>
			@endif
        </div>
      </div>
      <div class="col-sm-12">
        <div class="contform">
          <label >Message</label>
          <textarea class="form-control @error('msg') is-invalid @enderror" name="msg" id="exampleFormControlTextarea1" rows="3"></textarea>
			@if($errors->has('msg'))
				<div class="invalid-feedback ml-3">{{ $errors->first('msg') }}</div>
			@endif
        </div>
      </div>
      <div class="text-right col-sm-12">
        <button type="submit" class="bookademo mt-5 mb-5">Submit Message</button>
      </div>
    </div>
{{ Form::close() }}
  </div>
</div>
@endsection