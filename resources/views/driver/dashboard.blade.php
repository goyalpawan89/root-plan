@extends( 'layouts.app' )
@section( 'content' )
<div class="container-fluid px-xl-5">
  <section class="py-5">
    <div class="row">
      <div class="col-lg-12 mb-4">
        <div class="tabhed">
          <h2>Plan View</h2>
        </div>
        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="col-sm-8">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
				
                  <li class="nav-item" role="presentation"> <a class="nav-link active" id="map-tab" data-toggle="tab" href="#map" role="tab" aria-controls="map" aria-selected="true">Map</a> </li>
                  <li class="nav-item" role="presentation"> <a class="nav-link" id="routes-tab" data-toggle="tab" href="#routes" role="tab" aria-controls="routes" aria-selected="false">Routes</a> </li>                 
                </ul>
              </div>
             <!-- <div class="col-sm-4 plansrch">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i> </div>
                  </div>
                  <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Searching ...">
                </div>
              </div>
            </div>-->
          </div>
          <div class="card-body " style="padding:10px !important;">
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="map" role="tabpanel" aria-labelledby="map-tab">
                <div class="row">
                  <div class="col-sm-8"><img src="{{ asset('admin/img/Light.png') }}" alt="" /> </div>
                  <div class="col-sm-4 drivers">
                    <h2>Stops</h2>
                    <div class="drivers-lst">
					 <div class="circlebig"><div class="circle"><p class="circle-content">1</p></div></div>
                      <h3>Stop 1 - Order #3123</h3>
                      <p>maryelizabeth@gmail.com</p>
                      <p>+381-912-99-22</p>
                      <div class="drivaction"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> 20 <i class="fa fa-clock-o" aria-hidden="true"></i> 120min <i class="fa fa-tachometer" aria-hidden="true"></i> 90miles</div>
                    </div>
                    <div class="drivers-lst">
                      <div class="circlebig"><div class="circle"><p class="circle-content">2</p></div></div>
                      <h3>Stop 2 - Order #3323</h3>
                      <p>maryelizabeth@gmail.com</p>
                      <p>+381-912-99-22</p>
                      <div class="drivaction"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> 20 <i class="fa fa-clock-o" aria-hidden="true"></i> 120min <i class="fa fa-tachometer" aria-hidden="true"></i> 90miles</div>
                    </div>
                    <div class="drivers-lst">
                      <div class="circlebig"><div class="circle"><p class="circle-content">3</p></div></div>
                      <h3>Stop 3 - Order #4323</h3>
                      <p>maryelizabeth@gmail.com</p>
                      <p>+381-912-99-22</p>
                      <div class="drivaction"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> 20 <i class="fa fa-clock-o" aria-hidden="true"></i> 120min <i class="fa fa-tachometer" aria-hidden="true"></i> 90miles</div>
                    </div>
                    <div class="drivers-lst">
                      <div class="circlebig"><div class="circle"><p class="circle-content">4</p></div></div>
                      <h3>Stop 4 - Order #5323</h3>
                      <p>maryelizabeth@gmail.com</p>
                      <p>+381-912-99-22</p>

                      <div class="drivaction"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> 20 <i class="fa fa-clock-o" aria-hidden="true"></i> 120min <i class="fa fa-tachometer" aria-hidden="true"></i> 90miles</div>
                    </div>
                    <div class="drivers-lst">
                      <div class="circlebig"><div class="circle"><p class="circle-content">5</p></div></div>
                      <h3>Stop 5 - Order #6323</h3>
                      <p>maryelizabeth@gmail.com</p>
                      <p>+381-912-99-22</p>
                      <div class="drivaction"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> 20 <i class="fa fa-clock-o" aria-hidden="true"></i> 120min <i class="fa fa-tachometer" aria-hidden="true"></i> 90miles</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade settingtab" id="routes" role="tabpanel" aria-labelledby="routes-tab">
			  	<a href="{{ url('admin/orders/import') }}" name="submitConfig" value="" class="btn btn-success"><i class="fas fa-file-export"></i>&nbsp;Export routes</a>
			  	<table class="table card-text" id="users">
              <thead>
									<tr>
										<th>S No</th>
										<th>Order Id</th>
										<th>Priority</th>
										<th>Scheduled at</th>
										<th>Service start</th>
										<th>Service end</th>
										<th>Actual duration</th>
										<th>Location</th>
										<th>Driver</th>
										<th>Gate Code</th>
										<th>Stop Number</th>
										<th>Address</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th>1</th>
										<th>#1298</th>
										<th>Medium</th>
										<th>12:45pm</th>
										<th>11:48am<p class="grntxt">(57m early)</p></th>
										<th>11:48am<p class="grntxt">(59m early)</p></th>
										<th>1 min <p class="grntxt">(-2)</p></th>
										<th>Ashley Crook</th>
										<th>Mary Elizabeth-3 mercedez</th>
										<th>2345</th>
										<th>1</th>
										<th>390 Fairview Lane Warwic</th>
									</tr>
									<tr>
										<th>1</th>
										<th>#1298</th>
										<th>Medium</th>
										<th>12:45pm</th>
										<th>11:48am<p class="grntxt">(57m early)</p></th>
										<th>11:48am<p class="grntxt">(59m early)</p></th>
										<th>1 min <p class="grntxt">(-2)</p></th>
										<th>Ashley Crook</th>
										<th>Mary Elizabeth-3 mercedez</th>
										<th>2345</th>
										<th>1</th>
										<th>390 Fairview Lane Warwic</th>
									</tr>
									<tr>
										<th>1</th>
										<th>#1298</th>
										<th>Medium</th>
										<th>12:45pm</th>
										<th>11:48am<p class="grntxt">(57m early)</p></th>
										<th>11:48am<p class="grntxt">(59m early)</p></th>
										<th>1 min <p class="grntxt">(-2)</p></th>
										<th>Ashley Crook</th>
										<th>Mary Elizabeth-3 mercedez</th>
										<th>2345</th>
										<th>1</th>
										<th>390 Fairview Lane Warwic</th>
									</tr>
									<tr>
										<th>1</th>
										<th>#1298</th>
										<th>Medium</th>
										<th>12:45pm</th>
										<th>11:48am<p class="grntxt">(57m early)</p></th>
										<th>11:48am<p class="grntxt">(59m early)</p></th>
										<th>1 min <p class="grntxt">(-2)</p></th>
										<th>Ashley Crook</th>
										<th>Mary Elizabeth-3 mercedez</th>
										<th>2345</th>
										<th>1</th>
										<th>390 Fairview Lane Warwic</th>
									</tr>
									<tr>
										<th>1</th>
										<th>#1298</th>
										<th>Medium</th>
										<th>12:45pm</th>
										<th>11:48am<p class="grntxt">(57m early)</p></th>
										<th>11:48am<p class="grntxt">(59m early)</p></th>
										<th>1 min <p class="grntxt">(-2)</p></th>
										<th>Ashley Crook</th>
										<th>Mary Elizabeth-3 mercedez</th>
										<th>2345</th>
										<th>1</th>
										<th>390 Fairview Lane Warwic</th>
									</tr>
									<tr>
										<th>1</th>
										<th>#1298</th>
										<th>Medium</th>
										<th>12:45pm</th>
										<th>11:48am<p class="grntxt">(57m early)</p></th>
										<th>11:48am<p class="grntxt">(59m early)</p></th>
										<th>1 min <p class="grntxt">(-2)</p></th>
										<th>Ashley Crook</th>
										<th>Mary Elizabeth-3 mercedez</th>
										<th>2345</th>
										<th>1</th>
										<th>390 Fairview Lane Warwic</th>
									</tr>
									<tr>
										<th>1</th>
										<th>#1298</th>
										<th>Medium</th>
										<th>12:45pm</th>
										<th>11:48am<p class="grntxt">(57m early)</p></th>
										<th>11:48am<p class="grntxt">(59m early)</p></th>
										<th>1 min <p class="grntxt">(-2)</p></th>
										<th>Ashley Crook</th>
										<th>Mary Elizabeth-3 mercedez</th>
										<th>2345</th>
										<th>1</th>
										<th>390 Fairview Lane Warwic</th>
									</tr>
								</tbody>             
            </table>
			  </div>              
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
<style>
.circle {
  background: #39CF59;
  border-radius: 50%;
  color: #fff;
  height: 1.4em;
  position:absolute;
  width: 1.4em;
  top: 15px;
  left: 15px;  
}

.circlebig {
  background: #3bcc5a73;
  border-radius: 50%;  
  height: 3.4em;
  position:absolute;
  width: 3.4em;
  top: 30px;
  left: 15px;  
}

.circle-content {
  hyphens: auto;
  color:#fff !important	; 
  margin: 0.75em;
  text-align: center;
}

/**
 * Enhanced experience
 *
 * Credit: https://skeate.github.io/2015/07/13/Wrapping-Text-to-Fit-Shaped-Containers-with-CSS.html
 */

@supports (shape-outside: circle(50%)) {
  .circle-content {
    height: 100%;
	color:#fff;
    margin: 0;
  }
  
  .circle::before,
  .circle-content::before {
    content: "";
    height: 100%;
    width: 50%;
  }
  
  .circle::before {
    float: left;
    shape-outside: polygon(
      0 0, 100% 0, 60% 4%, 40% 10%, 20% 20%, 10% 28.2%, 5% 34.4%, 0 50%,
      5% 65.6%, 10% 71.8%, 20% 80%, 40% 90%, 60% 96%, 100% 100%, 0 100%
    );
  }
  
  .circle-content::before {
    float: right;
    shape-outside: polygon(
      100% 0, 0 0, 40% 4%, 60% 10%, 80% 20%, 90% 28.2%, 95% 34.4%, 100% 50%,
      95% 65.6%, 90% 71.8%, 80% 80%, 60% 90%, 40% 96%, 0 100%, 100% 100%
    );
  }
}
</style>
@section('footer_scripts') 
<script>
	$(document).ready(function(){
		$('#users').DataTable({			
			"responsive": true,
			language: {
				paginate: {
				  first: '<i class="fa fa-fw fa-fast-backward">',
				  last: '<i class="fa fa-fw fa-fast-forward">',
				  next: '<i class="fa fa-fw fa-long-arrow-right">',
				  previous: '<i class="fa fa-fw fa-long-arrow-left">'  
				}
			}
		});		
	});
</script>
@endsection