@extends( 'layouts.app' )
@section( 'content' )
<div class="container-fluid px-xl-5">
  <section class="py-2">
    <div class="row">

      <div class="col-lg-12 mb-4">        
        <div class="card">
          <div class="card-header">
            <h6 class="mb-0">Edit Blog</h6>
          </div>

          <div class="card-body settingtab">            
            {{Form::model($post, ['method' => 'PUT', 'enctype' => 'multipart/form-data', 'route' => ['superadmin.blogs.update', $post->id]])}}
			  <div class="row">
			  	<div class="col-md-12">
				  <div class="form-group">
					<label class="form-control-label text-uppercase">Title</label>
					  {!! Form::text('title', old('title'), ['class' => 'form-control ' . $errors->first('title', 'is-invalid')]) !!}
					@if($errors->has('title'))
						<div class="invalid-feedback ml-3">{{ $errors->first('title') }}</div>
					@endif
				  </div>
				</div>
			  </div>
			  <div class="row">
			  	<div class="col-md-6">
				  <div class="form-group">
					<label class="form-control-label text-uppercase">Status</label>
					  {!! Form::select('status', $statulabels, old('status'), ['class' => 'form-control ' . $errors->first('status', 'is-invalid')]) !!}
					@if($errors->has('status'))
						<div class="invalid-feedback ml-3">{{ $errors->first('status') }}</div>
					@endif
				  </div>
				  <div class="form-group">
					<label class="form-control-label text-uppercase">Tags</label>
					  {!! Form::text('tags', old('tags'), ['class' => 'form-control ' . $errors->first('tags', 'is-invalid')]) !!}
					@if($errors->has('tags'))
						<div class="invalid-feedback ml-3">{{ $errors->first('tags') }}</div>
					@endif
				  </div>
				</div>
				  <div class="col-md-6">					  
					  <div class="form-group">
						<label class="form-control-label text-uppercase">Image</label>
						<input type="file" name="image" class="form-control {{$errors->has('image') ? 'is-invalid' : ''}}">
					@if($errors->has('image'))
						<div class="invalid-feedback ml-3">{{ $errors->first('image') }}</div>
					@endif
					  @if($post->image != '')
						<p>
					  		<img src="{{ asset('images/blogs/thumbnail/'.$post->image) }}" class="img-fluid"/>
							<span style="cursor: pointer;" class="deleteImage">DELETE IMAGE</span>
						</p>
					  @endif
					  </div>
				  </div>
			  </div>
			  <div class="row">
			  	<div class="col-md-12">
				  <div class="form-group">
					<label class="form-control-label text-uppercase">Content</label>					  
					  {!! Form::textarea('content', old('content'), ['class' => 'form-control ' . $errors->first('content', 'is-invalid')]) !!}
					@if($errors->has('content'))
						<div class="invalid-feedback ml-3">{{ $errors->first('content') }}</div>
					@endif
				  </div>
				</div>
			  </div>
              <div class="form-group">
                <button type="submit" class="btn btn-success">Update</button>
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>

    </div>
  </section>
</div>
@endsection

@section( 'footer_scripts' )
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('content', {
        filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });	
	$(document).on("click", ".deleteImage", function(e) {
		e.preventDefault();
		bootbox.confirm({
			message: "Are you sure? to delete this image",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if(result){
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': "{{csrf_token()}}"
						}
					});
					$.ajax({
					   type:'GET',
					   url:'{{url("superadmin/blogs/deleteimage/".$post->id)}}',
					   success:function(data){
						  $(".deleteImage").closest('p').remove();
					   }
					});
				}
			}
		});
	});
</script>
@endsection