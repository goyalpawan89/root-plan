@extends( 'layouts.app' )
@section( 'content' )
<div class="container-fluid px-xl-5">
  <section class="py-2">
    <div class="row">

      <div class="col-lg-12 mb-4">        
        <div class="card">
          <div class="card-header">
            <h6 class="mb-0">Add Blog</h6>
          </div>

          <div class="card-body settingtab">            
            {{Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['superadmin.blogs.store']])}}
			  <div class="row">
			  	<div class="col-md-12">
				  <div class="form-group">
					<label class="form-control-label text-uppercase">Title</label>
					<input type="text" placeholder="Blog Title" name="title" class="form-control {{$errors->has('title') ? 'is-invalid' : ''}}">
					@if($errors->has('title'))
						<div class="invalid-feedback ml-3">{{ $errors->first('title') }}</div>
					@endif
				  </div>
				</div>
			  </div>
			  <div class="row">
			  	<div class="col-md-6">
				  <div class="form-group">
					<label class="form-control-label text-uppercase">Status</label>
					{!! Form::select('status', $statulabels, '1', ['class' => 'form-control']) !!}
					@if($errors->has('status'))
						<div class="invalid-feedback ml-3">{{ $errors->first('status') }}</div>
					@endif
				  </div>
				  <div class="form-group">
					<label class="form-control-label text-uppercase">Tags</label>
					<input type="text" name="tags" placeholder="Tags(, Separated)" class="form-control {{$errors->has('tags') ? 'is-invalid' : ''}}">
					@if($errors->has('tags'))
						<div class="invalid-feedback ml-3">{{ $errors->first('tags') }}</div>
					@endif
				  </div>
				</div>
				  <div class="col-md-6">					  
					  <div class="form-group">
						<label class="form-control-label text-uppercase">Image</label>
						<input type="file" name="image" class="form-control {{$errors->has('image') ? 'is-invalid' : ''}}">
						@if($errors->has('image'))
							<div class="invalid-feedback ml-3">{{ $errors->first('image') }}</div>
						@endif
					  </div>
				  </div>
			  </div>
			  <div class="row">
			  	<div class="col-md-12">
				  <div class="form-group">
					<label class="form-control-label text-uppercase">Content</label>
					<textarea name="content" class="form-control {{$errors->has('content') ? 'is-invalid' : ''}}"></textarea>
					@if($errors->has('content'))
						<div class="invalid-feedback ml-3">{{ $errors->first('content') }}</div>
					@endif
				  </div>
				</div>
			  </div>
              <div class="form-group">
                <button type="submit" class="btn btn-success">Save</button>
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>

    </div>
  </section>
</div>
@endsection

@section( 'footer_scripts' )
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('content', {
        filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
</script>
@endsection