@extends('layouts.app')
@section('content')	
<?php $selectedAddons = explode(',', $user->userSubscription->moduleids); ?>
<div class="container-fluid px-xl-5">
  <section class="py-2">
    <div class="row">

      <div class="col-lg-12 mb-4">
        <div class="card">
          <div class="card-header">
            <h6 class="mb-0">Customer Details</h6>
          </div>

          <div class="card-body">
              <div class="row">
				  <div class="col-md-3">
				  	<img src="{{ asset('admin/img/company.svg') }}" class="img-fluid"/>
				  </div>
				  <div class="col-md-9">				  	
					<table class="table table-borderless table-sm">
						<tr>
							<th colspan="5"><h4>{{$user->name}}</h4></th>
						</tr>
						<tr>
							<th>Email</th>
							<th>Phone Number</th>
							<th>Credit Card</th>
							<th>Drivers</th>
							<th>SMS Credits</th>
						</tr>
						<tr>
							<td>{{$user->email}}</td>
							<td>{{$user->phone}}</td>
							<td>************----</td>
							<td>{{$user->userSubscription->drivers}}</td>
							<td>${{$user->userSubscription->sms_credit}}</td>
						</tr>
						<tr>
							<th>Billing Status</th>
							<th>Modules</th>
							<th>Order limit</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<td>
								<span style="color: #f70000;">Failed - $00.00</span>
								
							</td>
							<td>{{count($selectedAddons)}} Activated</td>
							<td>{{$user->userSubscription->order_limit}} Orders</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				  </div>
			  </div>
          </div>
        </div>		
      </div>
	  <div class="col-lg-12 mb-4">
        <div class="card">
          <div class="card-body">
              <div class="row">
					<ul class="nav nav-tabs col-lg-12" id="myTab" role="tablist">
					  <li class="nav-item">
						<a class="nav-link active" id="billng-tab" data-toggle="tab" href="#billng" role="tab" aria-controls="billng" aria-selected="true">Billing</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" id="config-tab" data-toggle="tab" href="#config" role="tab" aria-controls="config" aria-selected="false">Config</a>
					  </li>
					</ul>
					<div class="tab-content col-lg-12" id="myTabContent">
					  <div class="tab-pane fade show active" id="billng" role="tabpanel" aria-labelledby="billng-tab">
{{Form::model($user, ['method' => 'PUT', 'class' => 'form-horizontal pt-2', 'route' => ['superadmin.customers.update', $user->id]])}}
  <div class="form-group row">
    <label class="col-md-2 form-control-label">Drivers</label>
    <div class="col-md-3 ">
		{!! Form::selectRange('drivers', 1, config('global.max_drivers'), $user->userSubscription->drivers, ['class' => 'custom-select']) !!}
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 form-control-label">Current Plan</label>
    <div class="col-md-3 ">  
		{!! Form::select('plan_id', $plans, $user->userSubscription->plan_id, ['placeholder' => 'None', 'class' => 'custom-select']) !!}
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 form-control-label">Invoice</label>
    <div class="col-md-3">
      <input type="submit" name="submitBilling" value="Send" class="btn btn-success">
    </div>
  </div>
{{ Form::close() }}
						</div>
					  <div class="tab-pane fade" id="config" role="tabpanel" aria-labelledby="config-tab">
	{{Form::model($user, ['method' => 'PUT', 'class' => 'form-horizontal pt-2', 'route' => ['superadmin.customers.update', $user->id]])}}
  <div class="row">
    <div class="col-md-6">
	  <div class="form-group row">
        <label class="col-md-4 form-control-label">First Name</label>
        <div class="col-md-6">
			{!! Form::text('user[first_name]', $user->first_name, ['class' => 'form-control']) !!}
        </div>
      </div>
	  <div class="form-group row">
        <label class="col-md-4 form-control-label">Last Name</label>
        <div class="col-md-6">
			{!! Form::text('user[last_name]', $user->last_name, ['class' => 'form-control']) !!}
        </div>
      </div>
	  <div class="form-group row">
        <label class="col-md-4 form-control-label">Company</label>
        <div class="col-md-6">
			{!! Form::text('userdetail[company]', $user->userDetail->company, ['class' => 'form-control']) !!}
        </div>
      </div>
	  <div class="form-group row">
		<label class="col-md-4 form-control-label">Password</label>
		<div class="col-md-6">
		  {!! Form::password('user[password]', ['class' => 'form-control']) !!}
		</div>
	  </div>	 
      <div class="form-group row">
        <label class="col-md-4 form-control-label">Current Plan</label>
        <div class="col-md-6">
			{!! Form::select('subscription[plan_id]', $plans, $user->userSubscription->plan_id, ['placeholder' => 'None', 'class' => 'custom-select']) !!}
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-4 form-control-label">Modules</label>
        <div class="col-md-6">
			{!! Form::text('subscription[modules]', $user->userSubscription->modules, ['class' => 'form-control modules', 'readonly' => 'readonly']) !!}
			{!! Form::hidden('subscription[moduleids]', $user->userSubscription->moduleids, ['class' => 'form-control moduleIds']) !!}
        </div>
      </div>    
      <div class="form-group row">
        <label class="col-md-4 form-control-label">Balance: <span style="color:#39CF59">$00.00</span></label>
        <div class="col-md-6">
          <label class="form-control-label tobalance">Add </label>
			<input type="number" min="0" style="width:35%" name="addbal" class="form-control tobalance"> 
			<label class="form-control-label tobalance">to Balance </label>			
        </div>
      </div>		
	  <div class="form-group row">
    <label class="col-md-4 form-control-label">Orders Limit</label>
    <div class="col-md-6">
		{!! Form::selectRange('subscription[order_limit]', 1, config('global.max_orders'), $user->userSubscription->order_limit, ['class' => 'custom-select']) !!}
    </div>
  </div>
    </div>
	  <div class="col-md-6">	  
	  <div class="form-group row">
        <label class="col-md-4 form-control-label">Driver Amount</label>
        <div class="col-md-6">
			{!! Form::text('subscription[driver_amount]', $user->userSubscription->driver_amount, ['class' => 'form-control']) !!}
        </div>
      </div>
	  <div class="form-group row">
    <label class="col-md-4 form-control-label">Mailerlite email</label>
    <div class="col-md-6">
		{!! Form::text('userdetail[mailerliteemail]', $user->userDetail->mailerliteemail, ['class' => 'form-control']) !!}
    </div>
  </div>
	  
	  <div class="form-group row">
    <label class="col-md-4 form-control-label">Mailerlite ID</label>
    <div class="col-md-6">
      {!! Form::text('userdetail[mailerliteid]', $user->userDetail->mailerliteid, ['class' => 'form-control']) !!}
    </div>
  </div>
      <div class="form-group row">
    <label class="col-md-4 form-control-label">Twilio account ID</label>
    <div class="col-md-6">
      {!! Form::text('userdetail[twilloaccountid]', $user->userDetail->twilloaccountid, ['class' => 'form-control']) !!}
    </div>
  </div>
	  <div class="form-group row">
    <label class="col-md-4 form-control-label">Twilio app ID</label>
    <div class="col-md-6">
      {!! Form::text('userdetail[twilloapptid]', $user->userDetail->twilloapptid, ['class' => 'form-control']) !!}
    </div>
  </div>
		  <div class="form-group row">
    <label class="col-md-4 form-control-label">Twilio SMS Rate</label>
    <div class="col-md-6">
      {!! Form::text('userdetail[twillosmsrate]', $user->userDetail->twillosmsrate, ['class' => 'form-control']) !!}
    </div>
  </div>
		  <div class="form-group row">
    <label class="col-md-4 form-control-label">Freshdesk email</label>
    <div class="col-md-6">
      {!! Form::text('userdetail[freshdeskemail]', $user->userDetail->freshdeskemail, ['class' => 'form-control']) !!}
    </div>
  </div>
		  <div class="form-group row">
    <label class="col-md-4 form-control-label">Freshdesk id</label>
    <div class="col-md-6">
      {!! Form::text('userdetail[freshdeskid]', $user->userDetail->freshdeskid, ['class' => 'form-control']) !!}
    </div>
  </div>		  
    </div>
	<div class="form-group col-md-12">   
		<p>&nbsp;</p>
		<input type="submit" name="submitConfig" value="Update" class="btn btn-success"/>
	</div>
  </div>
{{ Form::close() }}
						</div>
					</div>
			  </div>
          </div>
        </div>		
      </div>
    </div>
  </section>
</div>
<!-- The Modal -->
<div class="modal" id="myModalOfModules">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
		  <p><button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button></p>
        	<h4 class="modal-title">Modules</h4>
		  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		  <div class="row">
		  	@foreach($modules as $modId => $modName)
			    <div class="col-sm-6">
					<div class="custom-control custom-checkbox">
						<input name="modules" id="mod_{{$modId}}" {{(in_array($modId, $selectedAddons) ? 'checked' : '')}} type="checkbox" value="{{$modId}}" class="custom-control-input modulesCheckbox">
						<label for="mod_{{$modId}}" class="custom-control-label">{{$modName}}</label>
					</div>
				</div>
			@endforeach
			  <p>&nbsp;</p>
		  <div class="col-sm-12 text-right">
			<button type="button" class="btn btn-primaryprimary" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>Cancel</button>
			<button type="button" class="btn btn-success updateModules"><i class="fa fa-check" aria-hidden="true"></i>Success</button>
		  </div>
		  </div>
      </div>

    </div>
  </div>
</div>
@endsection

@section('footer_scripts') 
<script>
	$(document).ready(function(){
		$('.modules').click(function(){
			$('#myModalOfModules').modal();
		});	
		$('.updateModules').click(function(){			
			var checkedValLabels = $('.modulesCheckbox:checkbox:checked').map(function() {
				return $(this).siblings('label').text();
			}).get();
			var allMods = checkedValLabels.join(", ");		
			var checkedVals = $('.modulesCheckbox:checkbox:checked').map(function() {
				return $(this).attr('value');
			}).get();
			var allModIds = checkedVals.join(",");
			$('.modules').val(allMods);
			$('.moduleIds').val(allModIds);
			$('#myModalOfModules').modal('hide');
		});
	});
</script>
@endsection