@extends('layouts.app')
@section('content')	
<div class="container-fluid px-xl-5">
  <section class="py-2">
    <div class="row">

      <div class="col-lg-12 mb-4">
        <!--div class="tabhed">
          <h2>Customers</h2>
        </div>-->
        <div class="card">
          <div class="card-header">
            <h6 class="mb-0">Customers</h6>
          </div>

          <div class="card-body settingtab">
            <!--<div class="crdcapt">
              <h5>Customers details</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget eros efficitur, efficitur urna tincidunt, ultrices dolor. Sed tristique ex et lacus ultricies vestibulum.</p>
            </div>-->
            <table class="table card-text" id="users">
              <thead>
                <tr>
                  <th>S No.</th>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Company</th>
                  <th>Drivers</th>
                  <th>Email</th>
                  <th>Current Plan</th>
                  <th>Montly fee</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>

    </div>
  </section>
</div>
@endsection

@section('footer_scripts') 
<script>
$(document).ready(function() {
    var dataTable = $('#users').DataTable({
		"responsive": true,
        "ajax": {
            url: "{{ url('superadmin/customers') }}",
			"dataType": "json",
			"type": "GET",
			"data":{ _token: "{{csrf_token()}}"}
        },
        "processing": true,
        "serverSide": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "columns": [
            { "data": "sno" },
            { "data": "id" },
            { "data": "name" },
            { "data": "company" },
            { "data": "drivers" },
            { "data": "email" },
            { "data": "currentplan" },
            { "data": "monthlyfee" }
        ],
        columnDefs: [
            { orderable: false, targets: [ 3, 4, 6, 7] } //This part is ok now
        ],
		order: [[ 1, "desc" ]],
		language: {
			paginate: {
			  first: '<i class="fa fa-fw fa-fast-backward">',
			  last: '<i class="fa fa-fw fa-fast-forward">',
			  next: '<i class="fa fa-fw fa-long-arrow-right">',
			  previous: '<i class="fa fa-fw fa-long-arrow-left">'  
			}
		}
    });	           
	dataTable.columns().every( function () {
		var that = this;
		$( 'select', this.header() ).on( 'keyup change', function () {
			if ( that.search() !== this.value ) {
				that
					.search( this.value )
					.draw();
			}
		} );
	} );
	$(document).delegate('#users tbody tr', 'click', function() {
		window.location = $(this).find('.editLink').attr('href');
	  //console.log('API row values : ', table.row(this).data());
	});
} );
</script>
@endsection