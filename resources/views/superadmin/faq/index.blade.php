@extends('layouts.app')
@section('content')	
<div class="container-fluid px-xl-5">
  <section class="py-2">
    <div class="row">

      <div class="col-lg-12 mb-4">
        <!--<div class="tabhed">
          <h2>FAQ</h2>
        </div>-->
		
        <div class="card">
          <div class="card-header col-md-12">
            <div class="card-headerT"><h6 class="mb-0">FAQ</h6></div>
			<div class="card-headerB" ><a href="{{route('superadmin.faqs.create')}}" class="btn btn-info pull-right">Add New</a></div>
          </div>

          <div class="card-body settingtab">
            <!--<div class="crdcapt">
              <h5>Blogs details</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget eros efficitur, efficitur urna tincidunt, ultrices dolor. Sed tristique ex et lacus ultricies vestibulum.</p>
            </div>-->
            <table class="table card-text" id="users">
              <thead>
                <tr>
				  <th>S No</th>
                  <th>					  
					<select name="filterbytype" class="custom-select  mr-sm-2">
					   <option value="">All Type</option>
					   @foreach ($faqtypes as $typeId => $type)
					   <option value="{{ $typeId }}">{{ $type }}</option>
					   @endforeach
				    </select>
				  </th>
                  <th>Question</th>
                  <th>					  
					<select name="filterbystatus" class="custom-select  mr-sm-2">
					   <option value="">All Status</option>
					   @foreach ($statulabels as $statusId => $status)
					   <option value="{{ $statusId }}">{{ $status }}</option>
					   @endforeach
				    </select>
				  </th>
                  <th>Actions</th>
                </tr>
              </thead>              
            </table>
          </div>
        </div>
      </div>

    </div>
  </section>
</div>
@endsection

@section('footer_scripts') 
<script>
$(document).ready(function() {
    var dataTable = $('#users').DataTable({
		"responsive": true,
        "ajax": {
            url: "{{ url('superadmin/faqs') }}",
			"dataType": "json",
			"type": "GET",
			"data":{ _token: "{{csrf_token()}}"}
        },
        "processing": true,
        "serverSide": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "columns": [
            { "data": "sno" },
            { "data": "faqtype" },
            { "data": "question" },
            { "data": "status" },
            { "data": "options" }
        ],
        columnDefs: [
            { orderable: false, targets: [ 4 ] } //This part is ok now
        ],
		language: {
			paginate: {
			  first: '<i class="fa fa-fw fa-fast-backward">',
			  last: '<i class="fa fa-fw fa-fast-forward">',
			  next: '<i class="fa fa-fw fa-long-arrow-right">',
			  previous: '<i class="fa fa-fw fa-long-arrow-left">'  
			}
		}
    });	           
	dataTable.columns().every( function () {
		var that = this;
		$( 'select', this.header() ).on( 'keyup change', function () {
			if ( that.search() !== this.value ) {
				that
					.search( this.value )
					.draw();
			}
		} );
	} );
} );
</script>
@endsection