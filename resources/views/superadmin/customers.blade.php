@extends('layouts.app')
@section('content')	
<div class="container-fluid px-xl-5">
  <section class="py-2">
    <div class="row">

      <div class="col-lg-12 mb-4">
        <div class="tabhed">
          <h2>Customers</h2>
        </div>
        <div class="card">
          <div class="card-header">
            <h6 class="mb-0">Contacts</h6>
          </div>

          <div class="card-body settingtab">
            <div class="crdcapt">
              <h5>Customers details</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget eros efficitur, efficitur urna tincidunt, ultrices dolor. Sed tristique ex et lacus ultricies vestibulum.</p>
            </div>
            <table class="table card-text">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Company</th>
                  <th>Credit Card</th>
                  <th>Email</th>
                  <th>Current Plan</th>
                  <th>Montly fee</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">Lauren</th>
                  <td>Meal prep</td>
                  <td>***************3444</td>
                  <td class="emiltxt"> lauren@mealprep.com</td>
                  <td>Sprwt Plan</td>
                  <td>$30.56</td>
                </tr>
                <tr>
                  <th scope="row">Lauren</th>
                  <td>Meal prep</td>
                  <td>***************3444</td>
                  <td class="emiltxt"> lauren@mealprep.com</td>
                  <td>Sprwt Plan</td>
                  <td>$30.56</td>
                </tr>
                <tr>
                  <th scope="row">Lauren</th>
                  <td>Meal prep</td>
                  <td>***************3444</td>
                  <td class="emiltxt"> lauren@mealprep.com</td>
                  <td>Sprwt Plan</td>
                  <td>$30.56</td>
                </tr>
                <tr>
                  <th scope="row">Lauren</th>
                  <td>Meal prep</td>
                  <td>***************3444</td>
                  <td class="emiltxt"> lauren@mealprep.com</td>
                  <td>Sprwt Plan</td>
                  <td>$30.56</td>
                </tr>
                <tr>
                  <th scope="row">Lauren</th>
                  <td>Meal prep</td>
                  <td>***************3444</td>
                  <td class="emiltxt"> lauren@mealprep.com</td>
                  <td>Sprwt Plan</td>
                  <td>$30.56</td>
                </tr>
                <tr>
                  <th scope="row">Lauren</th>
                  <td>Meal prep</td>
                  <td>***************3444</td>
                  <td class="emiltxt"> lauren@mealprep.com</td>
                  <td>Sprwt Plan</td>
                  <td>$30.56</td>
                </tr>
                <tr>
                  <th scope="row">Lauren</th>
                  <td>Meal prep</td>
                  <td>***************3444</td>
                  <td class="emiltxt"> lauren@mealprep.com</td>
                  <td>Sprwt Plan</td>
                  <td>$30.56</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </section>
</div>
@endsection