@extends('layouts.app')
@section('content')	
<div class="container-fluid px-xl-5">
  <section class="py-2">
    <div class="row">

      <div class="col-lg-12 mb-4">
        <!--<div class="tabhed">
          <h2>Config</h2>
        </div>-->
        <div class="card">
          <div class="card-header">
            <h6 class="mb-0">Config</h6>
          </div>

          <div class="card-body settingtab">           
			{{Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['superadmin.configuration']])}}
            <div class="row">
				<div class="col-md-6">
					@foreach($configs as $config)
						@if($config->ftype != 'textarea')
						<div class="row form-group">
						<div class="col-md-4">
							<label class="form-control-label">{!! $config->name !!}</label>
						</div>
						<div class="col-md-8">
							@switch($config->ftype)
								@case('text')
									{!! Form::text('config['.$config->id.']', $config->value, ['class' => 'form-control ']) !!}
									@break
								@case('password')
									{!! Form::password('config['.$config->id.']', $config->value, ['class' => 'form-control ']) !!}
									@break
								@case('email')
									{!! Form::email('config['.$config->id.']', $config->value, ['class' => 'form-control ']) !!}
									@break
								@case('select')
									{!! Form::select('config['.$config->id.']', [], $config->value, ['class' => 'form-control ']) !!}
									@break
								@case('radio')
									{!! Form::radio('config['.$config->id.']', [], $config->value, ['class' => 'form-control ']) !!}
									@break
								@case('checkbox')
									{!! Form::checkbox('config['.$config->id.']', [], $config->value, ['class' => 'form-control ']) !!}
									@break
								@case('textarea')
									{!! Form::textarea('config['.$config->id.']', $config->value, ['class' => 'form-control', 'rows' => 5]) !!}
									@break
								@default
									{!! Form::text('config['.$config->id.']', $config->value, ['class' => 'form-control ']) !!}
									@break
							@endswitch
						</div>
					</div>
						@endif
					@endforeach
				</div>
				<div class="col-md-6">
					@foreach($configs as $config)
						@if($config->ftype == 'textarea')
						<div class="row form-group">
						<div class="col-md-4">
							<label class="form-control-label">{!! $config->name !!}</label>
						</div>
						<div class="col-md-8">
							@switch($config->ftype)
								@case('text')
									{!! Form::text('config['.$config->id.']', $config->value, ['class' => 'form-control ']) !!}
									@break
								@case('password')
									{!! Form::password('config['.$config->id.']', $config->value, ['class' => 'form-control ']) !!}
									@break
								@case('email')
									{!! Form::email('config['.$config->id.']', $config->value, ['class' => 'form-control ']) !!}
									@break
								@case('select')
									{!! Form::select('config['.$config->id.']', [], $config->value, ['class' => 'form-control ']) !!}
									@break
								@case('radio')
									{!! Form::radio('config['.$config->id.']', [], $config->value, ['class' => 'form-control ']) !!}
									@break
								@case('checkbox')
									{!! Form::checkbox('config['.$config->id.']', [], $config->value, ['class' => 'form-control ']) !!}
									@break
								@case('textarea')
									{!! Form::textarea('config['.$config->id.']', $config->value, ['class' => 'form-control', 'rows' => 3]) !!}
									@break
								@default
									{!! Form::text('config['.$config->id.']', $config->value, ['class' => 'form-control ']) !!}
									@break
							@endswitch
						</div>
					</div>
						@endif
					@endforeach
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<button type="submit" class="btn btn-success">Save</button>
				  	</div>				
				</div>
			</div>
			{{ Form::close() }}
          </div>
        </div>
      </div>

    </div>
  </section>
</div>
@endsection