@extends('layouts.app')
@section('content')	
<div class="container-fluid px-xl-5">
  <section class="py-2">
    <div class="row">

      <div class="col-lg-12 mb-4">
        <div class="card">
          <div class="card-header">
		  <div class="card-headerT"><h6 class="mb-0">Coupon</h6></div>
			<div class="card-headerB" ><a href="{{route('superadmin.coupons.create')}}" class="btn btn-info pull-right">Add New</a></div>	
			
          </div>

          <div class="card-body settingtab">
            <table class="table card-text" id="users">
              <thead>
                <tr>
				  <th>S No</th>
				  <th>Code</th>
                  <th>Discount</th>
                  <th>					  
					<select name="filterbydiscounttype" class="custom-select  mr-sm-2">
					   <option value="">All</option>
					   @foreach ($discount_types as $statusId => $status)
					   <option value="{{ $statusId }}">{{ $status }}</option>
					   @endforeach
				    </select>
				  </th>
                  <th>					  
					<select name="filterbyapplyon" class="custom-select  mr-sm-2">
					   <option value="">All</option>
					   @foreach ($applyontypes as $typeId => $type)
					   <option value="{{ $typeId }}">{{ $type }}</option>
					   @endforeach
				    </select>
				  </th>
                  <th>					  
					<select name="filterbystatus" class="custom-select  mr-sm-2">
					   <option value="">All</option>
					   @foreach ($applyontypes as $typeId => $type)
					   <option value="{{ $typeId }}">{{ $type }}</option>
					   @endforeach
				    </select>
				  </th>
                  <th>Actions</th>
                </tr>
              </thead>              
            </table>
          </div>
        </div>
      </div>

    </div>
  </section>
</div>
@endsection

@section('footer_scripts') 
<script>
$(document).ready(function() {
    var dataTable = $('#users').DataTable({
		"responsive": true,
        "ajax": {
            url: "{{ url('superadmin/coupons') }}",
			"dataType": "json",
			"type": "GET",
			"data":{ _token: "{{csrf_token()}}"}
        },
        "processing": true,
        "serverSide": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "columns": [
            { "data": "sno" },
            { "data": "code" },
            { "data": "discount" },
            { "data": "discount_type" },
            { "data": "applyon" },
            { "data": "status" },
            { "data": "created_at" }
        ],
        columnDefs: [
            { orderable: false, targets: [ 6 ] } //This part is ok now
        ],
		language: {
			paginate: {
			  first: '<i class="fa fa-fw fa-fast-backward">',
			  last: '<i class="fa fa-fw fa-fast-forward">',
			  next: '<i class="fa fa-fw fa-long-arrow-right">',
			  previous: '<i class="fa fa-fw fa-long-arrow-left">'  
			}
		}
    });	           
	dataTable.columns().every( function () {
		var that = this;
		$( 'select', this.header() ).on( 'keyup change', function () {
			if ( that.search() !== this.value ) {
				that
					.search( this.value )
					.draw();
			}
		} );
	} );
} );
</script>
@endsection