@extends( 'layouts.app' )
@section( 'content' )
<div class="container-fluid px-xl-5">
  <section class="py-2">
    <div class="row">

      <div class="col-lg-12 mb-4">
         <div class="card">
          <div class="card-header">
            <h6 class="mb-0">Edit Coupon</h6>
          </div>

          <div class="card-body settingtab">
            
            {{Form::model($coupon, ['method' => 'PUT', 'enctype' => 'multipart/form-data', 'route' => ['superadmin.coupons.update', $coupon->id]])}}
			  <div class="row">
			  	<div class="col-md-6">
				  <div class="form-group">
					<label class="form-control-label text-uppercase">Coupon Code</label>
					{!! Form::text('code', old('code'), ['class' => 'form-control ' . $errors->first('code', 'is-invalid')]) !!}
					@if($errors->has('code'))
						<div class="invalid-feedback ml-3">{{ $errors->first('code') }}</div>
					@endif
				  </div>
				</div>
			  	<div class="col-md-6">
				  <div class="form-group">
					<label class="form-control-label text-uppercase">Status</label>
					{!! Form::select('status', $statuses, old('status'), ['class' => 'form-control ' . $errors->first('status', 'is-invalid')]) !!}
					@if($errors->has('status'))
						<div class="invalid-feedback ml-3">{{ $errors->first('status') }}</div>
					@endif
				  </div>
				</div>
			  </div>
			  <div class="row">
			  	<div class="col-md-6">
				  <div class="form-group">
					<label class="form-control-label text-uppercase">Applicable On</label>
					{!! Form::select('applyon', $applyontypes, old('applyon'), ['class' => 'form-control ' . $errors->first('applyon', 'is-invalid')]) !!}
					@if($errors->has('applyon'))
						<div class="invalid-feedback ml-3">{{ $errors->first('applyon') }}</div>
					@endif
				  </div>				  
				</div>
				  <div class="col-md-6">					  
					  <div class="form-group">
						<label class="form-control-label text-uppercase">Status</label>
					{!! Form::select('discount_type', $discount_types, old('discount_type'), ['class' => 'form-control ' . $errors->first('discount_type', 'is-invalid')]) !!}
					@if($errors->has('discount_type'))
						<div class="invalid-feedback ml-3">{{ $errors->first('discount_type') }}</div>
					@endif
					  </div>
				  </div>
			  </div>
			  <div class="row">
			  	<div class="col-md-6">					  
					  <div class="form-group">
						<label class="form-control-label text-uppercase">Discount</label>
					{!! Form::text('discount', old('discount'), ['class' => 'form-control ' . $errors->first('discount', 'is-invalid')]) !!}
					@if($errors->has('discount'))
						<div class="invalid-feedback ml-3">{{ $errors->first('discount') }}</div>
					@endif
					  </div>
				  </div>
				<div class="col-md-6">					  
					  <div class="form-group">
						  <label class="form-control-label text-uppercase">&nbsp;</label>
						<p class="text-right"><button type="submit" class="btn btn-success">Update</button></p>
					  </div>
				  </div>
			  </div>              
            {{ Form::close() }}
          </div>
        </div>
      </div>

    </div>
  </section>
</div>
@endsection

@section( 'footer_scripts' )
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('answer', {
        filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
</script>
@endsection