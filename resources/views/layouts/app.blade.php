<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
		@include('particals.head')
    </head>
    <body>		
       	
		
	<div class="d-flex align-items-stretch">
	<?php $roleSlug = config('global.userroleslug'); ?>
	<?php $fileName = 'particals.'.$roleSlug[auth()->user()->role].'-sidebar'; ?>
       @include($fileName)
      <div class="page-holder w-100 d-flex flex-wrap">
	   
        <div class="container-fluid">
		
		  @include('particals.header')
          @include('particals.flash-message')
		 	
          @yield('content') 
        </div>        
      </div>
    </div>
	@include('particals.scripts')
	@yield ('footer_scripts')
    </body>
</html>
