<?php
if ( !function_exists('root_time_zones') )
{
	function root_time_zones(){
		$zones = array();
		$timestamp = time();	
		foreach(timezone_identifiers_list(\DateTimeZone::ALL) as $key => $t) {
				date_default_timezone_set($t);
				$zones[$t] =  '(UTC/GMT ' . date('P', $timestamp). ') ' . $t;
		}
		return $zones;
	}
}
		