<?php

namespace App\Http\Controllers\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DriverController extends Controller
{   
    public function __construct()
    {
        $this->middleware(['auth','verified']); 
    }
	
    public function dashboard()
    {
        return view('driver.dashboard');
    }
	
	public function show()
    {
        return view('driver.show');
    }
}
