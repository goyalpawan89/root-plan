<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Plan;
use App\Models\BlogPost;
use App\Models\Faq;

class FrontController extends Controller
{	
    public function __construct()
    {
        //$this->middleware('auth');
    }
	
    public function indexPage()
    {
		$data = [];
		$data['bodyid'] = 'home_bg';
        return view('front.pages.indexPage', $data);
    }
	
    public function aboutPage()
    {
        return view('front.pages.aboutPage');
    }
	
	public function thankyouPage()
    {
        return view('front.pages.thankyouPage');
    }
	
	public function bookPage()
    {
        return view('front.pages.bookPage');
    }
	
    public function planPage()
    {
		$plans = Plan::where('status', 1)->where('plantype', 1)->orderBy('planorder', 'asc')->get();
		$modules = Plan::where('status', 1)->where('plantype', 2)->orderBy('planorder', 'asc')->pluck('planname', 'id');
        return view('front.pages.planPage', compact('plans', 'modules'));
    }
	
	public function faqPage()
    {
		//$configs = config('configs');
		//echo config('configs.Style_header');
		//dd($configs);
		$faqs = Faq::where('status', 1)->orderBy('created_at', 'desc')->get();
		$faqtypes = array_filter(config('global.faq_types'));
        return view('front.pages.faqPage', ['faqs' => $faqs, 'faqtypes' => $faqtypes]);
    }
	
	public function blogPage()
    {
		$posts = BlogPost::where('status', 1)->orderBy('created_at', 'desc')->take(6)->get();
        return view('front.pages.blogPage', ['posts' => $posts]);
    }
	
	public function blogdetailPage($slug)
    {
		$post = BlogPost::where('status', 1)->where('slug', $slug)->first();
        return view('front.pages.blogdetailPage', ['post' => $post]);
    }
	
	public function contactusPage()
    {
        return view('front.pages.contactusPage');
    }
	
	public function contactusSubmit(Request $request)
    {   
        $postData = $request->all();
		$validData = Validator::make($postData, [
            'fullname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'website' => ['required', 'string', 'url', 'max:255'],
            'phone' => ['required', 'digits_between:8,12'],
            'subject' => ['required', 'string'],
            'howdidhear' => ['required', 'string'],
            'msg' => ['required', 'string']
        ]);
		if($validData->passes())
		{
			$emailData['to_email'] = 'goyalpawan89@gmail.com';
            $emailData['subject'] = 'Contact';
            \Mail::send(['html' => 'email.contact'], $postData, function($message) use ($emailData){
                $message->from(env('MAIL_FROM_ADDRESS', 'pkgwph@gmail.com'), config('app.name', 'Root Planner'));
                $message->to($emailData['to_email'])->subject($emailData['subject']);
            });
			return redirect('contactus')->withSuccess('Contact Details Submited Successfully!!!');
		} else {
            $messages = $validData->messages();
            return redirect('contactus')->withErrors($messages);			
		}
	}
	
	public function termsPage()
    {
        return view('front.pages.termsPage');
    }
	
	public function companyPage()
    {
        return view('front.pages.companyPage');
    }
}
