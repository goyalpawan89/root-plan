<?php
   
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use Session;
use Stripe;
   
class StripePaymentController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {
        return view('stripe');
    }
  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $stripeData = Stripe\Charge::create ([
                "amount" => $request->get('totalpayable') * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from rootplanner.io." 
        ]);
  
        Session::flash('success', 'Payment successful!');
        //dd($stripeData);
        return back();
    }
}