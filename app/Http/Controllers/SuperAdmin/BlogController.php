<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\BlogPost;
use Cviebrock\EloquentSluggable\Services\SlugService;
use ImageResize;
use Image;


class BlogController extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index(Request $request)
    {		
		$statulabels = array_filter(config('global.blog_status'));
		if($request->ajax()){
			$columns = array('id', 'title', 'tags', 'status', 'created_at', 'created_at');
			$postData = $request->all();
			$limit = $request->get('length');
        	$start = $request->get('start');
			$orderData = $request->get('order');
			$searchData = $request->get('search');
			$columnsData = $request->get('columns');
        	$order = $columns[$orderData[0]['column']];
        	$dir = $orderData[0]['dir'];
			$searchByRole = '';
			$searchByStatus = trim($columnsData[2]['search']['value']);
			$totalFiltered = $usercount = BlogPost::count();
			$search = $searchData['value'];
			$postsResult = BlogPost::when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                               
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('title', 'LIKE',"%{$search}%")
                                                  ->orWhere('tags', 'LIKE',"%{$search}%");
                               });
                           })
                           ->offset($start)->limit($limit)->orderBy('id', 'desc')->orderBy($order, $dir)->get();
			$totalFiltered = BlogPost::when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                           
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('title', 'LIKE',"%{$search}%")
                                                  ->orWhere('tags', 'LIKE',"%{$search}%");
                               });
                           })
                           ->count();
			$posts = [];
			$sno = $start;
			foreach($postsResult as $ky => $post){
				$subData = [];
				$subData['sno'] = ++$sno;
				$subData['title'] = $post->title;
				$subData['tags'] = $post->tags;
				$subData['status'] = $statulabels[$post->status];
				$subData['created_at'] = $post->created_at->format('M d, Y H:i');
				$editUrl = route('superadmin.blogs.edit', $post->id);
				$deleteUrl = route('superadmin.blogs.destroy', $post->id);
                $optionsData = '<a class="" href="'.$editUrl.'">EDIT</a>&nbsp;&nbsp;|&nbsp;&nbsp;';
				$optionsData .= '<form action="'.$deleteUrl.'" method="post" style="display:inline-block;">';
				$optionsData .= '<a class="deleteitem" href="">DELETE</a>';
				$optionsData .= '<input name="_method" type="hidden" value="DELETE">';
				$optionsData .= '<input name="_token" type="hidden" value="'.csrf_token().'">';
				$optionsData .= '</form>';
				$subData['options'] = $optionsData;
				$posts[] = $subData;
			}			
			$json_data = array(
				"draw"            => intval($request->get('draw')),  
				"recordsTotal"    => intval($usercount),  
				"recordsFiltered" => intval($totalFiltered), 
				"data"            => $posts   
			);
			return json_encode($json_data);
		}		
		$posts = BlogPost::paginate(config('global.pagination_records'));
        return view('superadmin.blog.index', ['posts' => $posts, 'statulabels' => $statulabels]);
    }
	
    public function create()
    {
		$statulabels = array_filter(config('global.blog_status'));
        return view('superadmin.blog.create', compact('statulabels'));
    }
	
	public function store(Request $request)
    {
		$postData = $request->all();
		$validData = Validator::make($postData, [
            'title' => ['required', 'string', 'max:255'],
            'status' => ['required'],
            'tags' => ['required', 'string', 'max:255'],
            'image' => ['image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'content' => ['required', 'string']
        ]);
		if($validData->passes())
		{
			$postData['image'] = '';
			if($request->hasFile('image')) {
				$image = $request->file('image');
				$fileName = time().'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('images/blogs');
				$img = Image::make($image->getRealPath());
				$img->resize(100, 100, function ($constraint) {
					$constraint->aspectRatio();
				})->save($destinationPath.'/thumbnail/'.$fileName);
				$img->resize(500, 500, function ($constraint) {
					$constraint->aspectRatio();
				})->save($destinationPath.'/middle/'.$fileName);
				$image->move(public_path('images/blogs/main'), $fileName);
				$postData['image'] = $fileName;
			}
			$postData['slug'] = SlugService::createSlug(BlogPost::class, 'slug', $request->title);
			$blog = BlogPost::create($postData);
			return redirect()->route('superadmin.blogs.index')->withSuccess('Blog Added Successfully!!!');
		} else {
            $messages = $validData->messages();
            return back()->withInput()->withErrors($messages);			
		}
	}
	
	public function edit($id)
    {
        $post = BlogPost::findOrFail($id);
		$statulabels = array_filter(config('global.blog_status'));
        return view('superadmin.blog.edit', compact('post', 'statulabels'));
    }
	
	public function update(Request $request, $id)
    {
		$postData = $request->all();
		$validData = Validator::make($postData, [
            'title' => ['required', 'string', 'max:255'],
            'status' => ['required'],
            'tags' => ['required', 'string', 'max:255'],
            'image' => ['image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'content' => ['required', 'string']
        ]);
		if($validData->passes())
		{
			if($request->hasFile('image')) {
				$image = $request->file('image');
				$fileName = time().'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('images/blogs');
				$img = Image::make($image->getRealPath());
				$img->resize(100, 100, function ($constraint) {
					$constraint->aspectRatio();
				})->save($destinationPath.'/thumbnail/'.$fileName);
				$img->resize(500, 500, function ($constraint) {
					$constraint->aspectRatio();
				})->save($destinationPath.'/middle/'.$fileName);
				$image->move(public_path('images/blogs/main'), $fileName);
				$postData['image'] = $fileName;
			}
			else {
				unset($postData['image']);
			}
			$postData['slug'] = SlugService::createSlug(BlogPost::class, 'slug', $request->title);
			$blog = BlogPost::findOrFail($id);
        	$blog->update($postData);
			return redirect()->route('superadmin.blogs.index')->withSuccess('Blog Update Successfully!!!');
		} else {
            $messages = $validData->messages();
            return back()->withInput()->withErrors($messages);			
		}
	}
	
    public function destroy($id)
    {
        $blog = BlogPost::findOrFail($id);
        $blog->delete();
        return redirect()->route('superadmin.blogs.index')->withSuccess('Blog Deleted Successfully!!!');
    }
	
    public function deleteImage($id)
    {
        $blog = BlogPost::findOrFail($id);
		$imageName = $blog->image;
		$postData['image'] = '';
        $blog->update($postData);
		die('1');
    }
}
