<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\SubscriptionHistory;
use App\Models\Subscription;
use App\Models\UserDetail;
use App\Models\Plan;
use App\User;
use DB;

class CustomerController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index(Request $request)
    {
        $users = User::where('role', 2)->paginate(config('global.pagination_records'));
		$statulabels = array_filter(config('global.faq_status'));
		if($request->ajax())
		{
			$columns = array("sno", "id", "name", "company", "drivers", "email", "currentplan", "monthlyfee");
			$postData = $request->all();
			$limit = $request->get('length');
        	$start = $request->get('start');
			$orderData = $request->get('order');
			$searchData = $request->get('search');
			$columnsData = $request->get('columns');
        	$order = $columns[$orderData[0]['column']];
        	$dir = $orderData[0]['dir'];
			$searchByStatus = '';
			$totalFiltered = $usercount = User::where('role', 2)->count();
			$search = $searchData['value'];
			$postsResult = User::where('role', 2)
						   ->when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                               
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('name','LIKE',"%{$search}%")
                                                  ->orWhere('first_name', 'LIKE',"%{$search}%")
                                                  ->orWhere('email', 'LIKE',"%{$search}%")
                                                  ->orWhere('last_name', 'LIKE',"%{$search}%");
                               });
                           })
                           ->offset($start)->limit($limit)->orderBy($order, $dir)->get();
			$totalFiltered = User::where('role', 2)
						   ->when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                            
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('name','LIKE',"%{$search}%")
                                                  ->orWhere('first_name', 'LIKE',"%{$search}%")
                                                  ->orWhere('email', 'LIKE',"%{$search}%")
                                                  ->orWhere('last_name', 'LIKE',"%{$search}%");
                               });
                           })
                           ->count();
			$posts = [];
			$sno = $start;
			foreach($postsResult as $ky => $post){
				$nullPlan = (is_null($post->userSubscription->plan) ? 0 : 1);
				$editUrl = route('superadmin.customers.edit', $post->id);
				$subData = [];
				$subData['sno'] = ++$sno;
				$subData['id'] = $post->id;
				$subData['name'] = $post->name;
				$subData['company'] = (is_null($post->userDetail->company) ? '--' : $post->userDetail->company);
				$subData['drivers'] = $post->userSubscription->drivers;
				$subData['email'] = $post->email;
				$subData['currentplan'] = ($nullPlan ? $post->userSubscription->plan->planname : 'N/A');
				$subData['monthlyfee'] = '$'.($nullPlan ? $post->userSubscription->plan->amount : '0.00').'<span class="d-none"><a class="editLink" href="'.$editUrl.'"></a></span>';
				$posts[] = $subData;
			}			
			$json_data = array(
				"draw"            => intval($request->get('draw')),  
				"recordsTotal"    => intval($usercount),  
				"recordsFiltered" => intval($totalFiltered), 
				"data"            => $posts   
			);
			return json_encode($json_data);
		}	
        return view('superadmin.customer.index', ['users' => $users, 'statulabels' => $statulabels]);
    }
	
    public function create()
    {
        //
    }
	
    public function store(Request $request)
    {
        //
    }
	
    public function show($id)
    {
        //
    }
	
    public function edit($id)
    {
        $user = User::findOrFail($id);
		//$accessToken = $user->createToken('Token Name')->accessToken;
		//echo $accessToken;
		//dd($user->tokens);
		$plans = Plan::where('status', 1)->where('plantype', 1)->orderBy('planorder', 'asc')->pluck('planname', 'id');
		$modules = Plan::where('status', 1)->where('plantype', 2)->orderBy('planorder', 'asc')->pluck('planname', 'id');
		return view('superadmin.customer.edit', compact('user', 'plans', 'modules'));
    }
	
    public function update(Request $request, $id)
    {
		if($request->get('submitConfig'))
		{
			$postData = $request->all();
			$userData = $request->get('user');
			$userDetailData = $request->get('userdetail');
			$subscriptionData = $request->get('subscription');
			//dd($subscriptionData);
			if(trim($userData['password']) == NULL || trim($userData['password']) == ''){
				unset($userData['password']);
			} else {
				Hash::make($userData['password']);
			}
			$user = User::findOrFail($id);
			$user->update($userData);
			$userDetail = UserDetail::where('user_id', $id)->firstOrFail();
			$userDetail->update($userDetailData);
			$subscriptionDetail = Subscription::where('user_id', $id)->firstOrFail();
			$oldSubscriptionData = [];
			$oldSubscriptionData['plan_id'] = $subscriptionDetail->plan_id;
			$oldSubscriptionData['modules'] = $subscriptionDetail->modules;
			$oldSubscriptionData['moduleids'] = $subscriptionDetail->moduleids;
			$oldSubscriptionData['order_limit'] = $subscriptionDetail->order_limit;
			$oldSubscriptionData['driver_amount'] = $subscriptionDetail->driver_amount;
			$subscriptionDetail->update($subscriptionData);
			$subscriptionCompareResult = array_diff($oldSubscriptionData, $subscriptionData);
			if(!empty($subscriptionCompareResult)){
				//dd($subscriptionData);
				$subscriptionData['user_id'] = $id;
				/*$subscriptionData['drivers'] = $subscriptionDetail->drivers;
				$subscriptionData['sms_credit'] = $subscriptionDetail->sms_credit;
				$subscriptionData['billing_status'] = $subscriptionDetail->billing_status;
				$subscriptionData['amount'] = $subscriptionDetail->amount;*/
				SubscriptionHistory::create($subscriptionData);
			}
			return redirect()->route('superadmin.customers.edit', $id)->withSuccess('Customer Config Update Successfully!!!');
		}
		else if($request->get('submitBilling')){
			$subscriptionDetail = Subscription::where('user_id', $id)->firstOrFail();
			$subscriptionData = [];
			$subscriptionData['plan_id'] = $request->get('plan_id');
			$subscriptionData['drivers'] = $request->get('drivers');
			$oldSubscriptionData = [];
			$oldSubscriptionData['plan_id'] = $subscriptionDetail->plan_id;
			$oldSubscriptionData['drivers'] = $subscriptionDetail->drivers;
			$subscriptionDetail->update($subscriptionData);
			$subscriptionCompareResult = array_diff($oldSubscriptionData, $subscriptionData);
			if(!empty($subscriptionCompareResult)){
				$subscriptionData['user_id'] = $id;
				SubscriptionHistory::create($subscriptionData);
			}
			return redirect()->route('superadmin.customers.edit', $id)->withSuccess('Customer Billing Update Successfully!!!');
		}
		else {
			return redirect()->route('superadmin.customers.edit', $id);
		}
    }
	
    public function destroy($id)
    {
        //
    }
}
