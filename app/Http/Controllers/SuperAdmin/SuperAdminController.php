<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Config;

class SuperAdminController extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function dashboard()
    {
		return redirect()->route('superadmin.customers.index');
        return view('superadmin.dashboard');
    }
	
    public function customers()
    {
        return view('superadmin.customers');
    }
	
    public function configuration()
    {
		$configs = Config::where('status', 1)->orderBy('ftype', 'asc')->get();
        return view('superadmin.configuration', ['configs' => $configs]);
    }
	
    public function configurationSubmit(Request $request)
    {
		$configs = $request->get('config');
		foreach($configs as $configId => $configValue){
			Config::where('id', $configId)->update(['value' => $configValue]);
		}
        return redirect()->route('superadmin.configuration')->withSuccess('Config Update Successfully!!!');
    }
}
