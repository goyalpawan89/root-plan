<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Coupon;
use Illuminate\Support\Facades\Validator;

class CouponController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index(Request $request)
    {
        $coupons = Coupon::paginate(config('global.pagination_records'));
		$applyontypes = array_filter(config('global.applyontypes'));
		$discount_types = array_filter(config('global.discount_types'));
		$statuses = array_filter(config('global.statuses'));
		if($request->ajax())
		{
			$columns = array('id', 'code', 'discount', 'discount_type', 'applyon', 'status', 'id');
			$postData = $request->all();
			$limit = $request->get('length');
        	$start = $request->get('start');
			$orderData = $request->get('order');
			$searchData = $request->get('search');
			$columnsData = $request->get('columns');
        	$order = $columns[$orderData[0]['column']];
        	$dir = $orderData[0]['dir'];
			$searchByRole = '';
			$searchByStatus = trim($columnsData[2]['search']['value']);
			$totalFiltered = $usercount = Coupon::count();
			$search = $searchData['value'];
			$postsResult = Coupon::when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                               
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('question', 'LIKE',"%{$search}%")
                                                  ->orWhere('answer', 'LIKE',"%{$search}%");
                               });
                           })
                           ->offset($start)->limit($limit)->orderBy($order, $dir)->get();
			$totalFiltered = Coupon::when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                            
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('question', 'LIKE',"%{$search}%")
                                                  ->orWhere('answer', 'LIKE',"%{$search}%");
                               });
                           })
                           ->count();
			$posts = [];
			$sno = $start;
			foreach($postsResult as $ky => $coupon){
				$subData = [];
				$subData['sno'] = ++$sno;
				$subData['code'] = $coupon->code;
				$subData['discount'] = $coupon->discount;
				$subData['discount_type'] = $discount_types[$coupon->discount_type];
				$subData['applyon'] = $applyontypes[$coupon->applyon];
				$subData['status'] = $statuses[$coupon->status];
				$editUrl = route('superadmin.coupons.edit', $coupon->id);
				$deleteUrl = route('superadmin.coupons.destroy', $coupon->id);
                $optionsData = '<a class="" href="'.$editUrl.'">EDIT</a>&nbsp;&nbsp;|&nbsp;&nbsp;';
				$optionsData .= '<form action="'.$deleteUrl.'" method="post" style="display:inline-block;">';
				$optionsData .= '<a class="deleteitem" href="">DELETE</a>';
				$optionsData .= '<input name="_method" type="hidden" value="DELETE">';
				$optionsData .= '<input name="_token" type="hidden" value="'.csrf_token().'">';
				$optionsData .= '</form>';
				$subData['created_at'] = $optionsData;
				$posts[] = $subData;
			}			
			$json_data = array(
				"draw"            => intval($request->get('draw')),  
				"recordsTotal"    => intval($usercount),  
				"recordsFiltered" => intval($totalFiltered), 
				"data"            => $posts   
			);
			return json_encode($json_data);
		}	
        return view('superadmin.coupon.index', compact('coupons', 'statuses', 'applyontypes', 'discount_types'));
    }
	
    public function create()
    {
		$statuses = array_filter(config('global.statuses'));
		$applyontypes = array_filter(config('global.applyontypes'));
		$discount_types = array_filter(config('global.discount_types'));
        return view('superadmin.coupon.create', compact('statuses', 'applyontypes', 'discount_types'));
    }
	
    public function store(Request $request)
    {
        $postData = $request->all();
		$validData = Validator::make($postData, [
            'code' => ['required', 'string', 'max:50', 'unique:coupons,code'],
            'status' => ['required'],
            'applyon' => ['required'],
            'discount_type' => ['required'],
            'discount' => ['required', 'numeric', 'min:1', 'max:9999']
        ]);
		if($validData->passes())
		{
			$blog = Coupon::create($postData);
			return redirect()->route('superadmin.coupons.index')->withSuccess('Coupon Added Successfully!!!');
		} else {
            $messages = $validData->messages();
            return back()->withInput()->withErrors($messages);			
		}
    }
	
    public function show($id)
    {
        //
    }
	
    public function edit($id)
    {
        $coupon = Coupon::findOrFail($id);
		$statuses = array_filter(config('global.statuses'));
		$applyontypes = array_filter(config('global.applyontypes'));
		$discount_types = array_filter(config('global.discount_types'));
        return view('superadmin.coupon.edit', compact('coupon', 'statuses', 'applyontypes', 'discount_types'));
    }
	
    public function update(Request $request, $id)
    {
        $postData = $request->all();
		$validData = Validator::make($postData, [
            'code' => ['required', 'string', 'max:50', 'unique:coupons,code,'.$id],
            'status' => ['required'],
            'applyon' => ['required'],
            'discount_type' => ['required'],
            'discount' => ['required', 'numeric', 'min:1', 'max:9999']
        ]);
		if($validData->passes())
		{
			$coupon = Coupon::findOrFail($id);
        	$coupon->update($postData);
			return redirect()->route('superadmin.coupons.index')->withSuccess('Coupon Update Successfully!!!');
		} else {
            $messages = $validData->messages();
            return back()->withInput()->withErrors($messages);			
		}
    }
	
    public function destroy($id)
    {
        $coupon = Coupon::findOrFail($id);
        $coupon->delete();
        return redirect()->route('superadmin.coupons.index')->withSuccess('Coupon Deleted Successfully!!!');
    }
}
