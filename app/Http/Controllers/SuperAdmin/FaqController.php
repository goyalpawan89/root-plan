<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Faq;
use Illuminate\Support\Facades\Validator;

class FaqController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index(Request $request)
    {
        $faqs = Faq::paginate(config('global.pagination_records'));
		$statulabels = array_filter(config('global.faq_status'));
		$faqtypes = array_filter(config('global.faq_types'));
		if($request->ajax())
		{
			$columns = array('id', 'faqtype', 'question', 'status', 'created_at');
			$postData = $request->all();
			$limit = $request->get('length');
        	$start = $request->get('start');
			$orderData = $request->get('order');
			$searchData = $request->get('search');
			$columnsData = $request->get('columns');
        	$order = $columns[$orderData[0]['column']];
        	$dir = $orderData[0]['dir'];
			$searchByRole = '';
			$searchByStatus = trim($columnsData[2]['search']['value']);
			$totalFiltered = $usercount = Faq::count();
			$search = $searchData['value'];
			$postsResult = Faq::when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                               
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('question', 'LIKE',"%{$search}%")
                                                  ->orWhere('answer', 'LIKE',"%{$search}%");
                               });
                           })
                           ->offset($start)->limit($limit)->orderBy($order, $dir)->get();
			$totalFiltered = Faq::when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                            
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('question', 'LIKE',"%{$search}%")
                                                  ->orWhere('answer', 'LIKE',"%{$search}%");
                               });
                           })
                           ->count();
			$posts = [];
			$sno = $start;
			foreach($postsResult as $ky => $post){
				$subData = [];
				$subData['sno'] = ++$sno;
				$subData['faqtype'] = $faqtypes[$post->faqtype];
				$subData['question'] = $post->question;
				$subData['status'] = $statulabels[$post->status];
				$editUrl = route('superadmin.faqs.edit', $post->id);
				$deleteUrl = route('superadmin.faqs.destroy', $post->id);
                $optionsData = '<a class="" href="'.$editUrl.'">EDIT</a>&nbsp;&nbsp;|&nbsp;&nbsp;';
				$optionsData .= '<form action="'.$deleteUrl.'" method="post" style="display:inline-block;">';
				$optionsData .= '<a class="deleteitem" href="">DELETE</a>';
				$optionsData .= '<input name="_method" type="hidden" value="DELETE">';
				$optionsData .= '<input name="_token" type="hidden" value="'.csrf_token().'">';
				$optionsData .= '</form>';
				$subData['options'] = $optionsData;
				$posts[] = $subData;
			}			
			$json_data = array(
				"draw"            => intval($request->get('draw')),  
				"recordsTotal"    => intval($usercount),  
				"recordsFiltered" => intval($totalFiltered), 
				"data"            => $posts   
			);
			return json_encode($json_data);
		}	
        return view('superadmin.faq.index', ['faqs' => $faqs, 'statulabels' => $statulabels, 'faqtypes' => $faqtypes]);
    }
	
    public function create()
    {
        $statulabels = array_filter(config('global.faq_status'));
		$faqtypes = array_filter(config('global.faq_types'));
        return view('superadmin.faq.create', ['statulabels' => $statulabels, 'faqtypes' => $faqtypes]);
    }
	
    public function store(Request $request)
    {
        $postData = $request->all();
		$validData = Validator::make($postData, [
            'question' => ['required', 'string', 'max:255'],
            'status' => ['required'],
            'faqtype' => ['required'],
            'answer' => ['required', 'string']
        ]);
		if($validData->passes())
		{
			$blog = Faq::create($postData);
			return redirect()->route('superadmin.faqs.index')->withSuccess('Faq Added Successfully!!!');
		} else {
            $messages = $validData->messages();
            return back()->withInput()->withErrors($messages);			
		}
    }
	
    public function show($id)
    {
        //
    }
	
    public function edit($id)
    {
        $faq = Faq::findOrFail($id);
		$statulabels = array_filter(config('global.faq_status'));
		$faqtypes = array_filter(config('global.faq_types'));
        return view('superadmin.faq.edit', compact('faq', 'faqtypes', 'statulabels'));
    }
	
    public function update(Request $request, $id)
    {
        $postData = $request->all();
		$validData = Validator::make($postData, [
            'question' => ['required', 'string', 'max:255'],
            'status' => ['required'],
            'faqtype' => ['required'],
            'answer' => ['required', 'string']
        ]);
		if($validData->passes())
		{
			$faq = Faq::findOrFail($id);
        	$faq->update($postData);
			return redirect()->route('superadmin.faqs.index')->withSuccess('Faq Update Successfully!!!');
		} else {
            $messages = $validData->messages();
            return back()->withInput()->withErrors($messages);			
		}
    }
	
    public function destroy($id)
    {
        $faq = Faq::findOrFail($id);
        $faq->delete();
        return redirect()->route('superadmin.faqs.index')->withSuccess('Faq Deleted Successfully!!!');
    }
}
