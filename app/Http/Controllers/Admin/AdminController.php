<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SubscriptionHistory;
use App\Models\PaymentHistory;
use App\Models\Subscription;
use App\Models\UserDetail;
use App\Models\Location;
use App\Models\Country;
use App\Models\Invoice;
use App\Models\Plan;
use App\User;
use Stripe;

class AdminController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function dashboard()
    {
		$drivers = User::where('role', 3)->where('status_id', 1)
						   ->whereHas('driver', function($q){
								$q->where('parent_id', auth()->user()->id);
						   })->get();
        return view('admin.dashboard', compact('drivers'));
    }
	
	
	
    public function billing()
    {
		$plans = Plan::where('status', 1)->where('plantype', 1)->orderBy('planorder', 'asc')->get();
		$modules = Plan::where('status', 1)->where('plantype', 2)->orderBy('planorder', 'asc')->get();
		$countries = Country::orderBy('countryName', 'asc')->pluck('countryName', 'isoAlpha3');
		//$phistories = PaymentHistory::where('user_id', auth()->user()->id)->orderBy('id', 'desc')->get();
		$phistories = [];
		$autoOptns = [10 =>10, 20 => 20, 25 => 25, 50 => 50];
		$credtVals = [10 =>10, 25 => 25, 50 => 50, 75 => 75, 100 => 100, 200 => 200, 250 => 250, 500 => 500, 1000 => 1000 ];
		$cards = [];
		$cards['data'] = [];
		$fontawsom = ['visa', 'mastercard', 'amex', 'discover'];
		if(auth()->user()->userDetail->stripe_customer_id != ''){
			Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));		
			$customerId = auth()->user()->userDetail->stripe_customer_id;
			$cards = Stripe\PaymentMethod::all([
											'customer' => $customerId,
							  				'type' => 'card'
											])->toArray();	
			$stripeCustomer = Stripe\Customer::retrieve($customerId);
		}
        return view('admin.billing', compact('plans', 'modules', 'countries', 'phistories', 'autoOptns', 'credtVals', 'cards', 'fontawsom', 'stripeCustomer'));
    }
	
	public function statusUpdate(Request $request)
	{
		$postData = $request->all();
		$id = $request->get('id');
		$table = $request->get('table');
		$status = $request->get('status');
		//unset($postData['_token']);
		if($table == 'location'){
			Location::where('id', $id)->update(['status' => $status]);
		}
		if($table == 'user'){
			User::where('id', $id)->update(['status_id' => $status]);
		}
		die;
	}
	
    public function billingInvoices(Request $request)
    { 
		if($request->ajax())
		{
			$columns = array('payment_type', 'id', 'invoice_id', 'amount', 'payment_status');
			$postData = $request->all();
			//dd($postData);
			$limit = $request->get('length');
        	$start = $request->get('start');
			$orderData = $request->get('order');
			$searchData = $request->get('search');
			$columnsData = $request->get('columns');
        	$order = $columns[$orderData[0]['column']];
        	$dir = $orderData[0]['dir'];
			$searchByRole = '';
			$searchByStatus = trim($columnsData[2]['search']['value']);
			$userId = auth()->user()->id;
			//$driverId = $postData['driver_id'];
			$totalFiltered = $usercount = PaymentHistory::where('user_id', $userId)->count();
			$search = $searchData['value'];
			$postsResult = PaymentHistory::where('user_id', $userId)
						   ->when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                               
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('amount', 'LIKE',"%{$search}%")
                                                  ->orWhere('payment_type', 'LIKE',"%{$search}%");
                               });
                           })
                           ->offset($start)->limit($limit)->orderBy($order, $dir)->get();
			$totalFiltered = PaymentHistory::where('user_id', $userId)
						   ->when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                            
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('amount', 'LIKE',"%{$search}%")
                                                  ->orWhere('payment_type', 'LIKE',"%{$search}%");
                               });
                           })
                           ->count();
			$posts = [];
			$sno = $start;
			foreach($postsResult as $ky => $invoice){
				$invoveData = unserialize($invoice->stripe_invoice);
				if($invoveData['object'] == 'payment_intent'){
					$invoveData = $invoveData['charges']['data'][0];
				}
				$invoiceUrl = (isset($invoveData['hosted_invoice_url']) ? $invoveData['hosted_invoice_url'] : $invoveData['receipt_url']);
				$subData = [];
				$subData['type'] = $invoice->payment_type;
				$subData['date'] = $invoice->created_at->format('d/m/Y');
				$subData['invoice_num'] = (is_null($invoveData['receipt_number']) ? '-' : $invoveData['receipt_number']);
				$subData['amount'] = '$'.(($invoice->amount)/100);
				$subData['invoice'] = '<a target="_blank" href="'.$invoiceUrl.'">View Invoice</a>';
				$posts[] = $subData;
			}			
			$json_data = array(
				"draw"            => intval($request->get('draw')),  
				"recordsTotal"    => intval($usercount),  
				"recordsFiltered" => intval($totalFiltered), 
				"data"            => $posts   
			);
			return json_encode($json_data);
		}	
    }
}
