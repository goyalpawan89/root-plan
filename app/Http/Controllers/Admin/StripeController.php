<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SubscriptionHistory;
use App\Models\BillingAddress;
use App\Models\PaymentHistory;
use App\Models\Autorecharge;
use App\Models\Subscription;
use App\Models\UserDetail;
use App\Models\Location;
use App\Models\Invoice;
use App\Models\Plan;
use App\User;
use Stripe;

class StripeController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function stripePay(Request $request)
    {
		Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
		if(auth()->user()->userDetail->stripe_customer_id == ''){
			$stripe_payment_id = $request->get('payment_method_id');
			$customerData = [
				'email'            => auth()->user()->email,
				'name'			   => auth()->user()->name,
				'payment_method'   => $stripe_payment_id,
				'invoice_settings' => ['default_payment_method' => $stripe_payment_id]
			];
			$customer = Stripe\Customer::create($customerData);
			$customerId = $customer->id;
			$paymentMethod = Stripe\PaymentMethod::retrieve( $stripe_payment_id );
			$paymentMethod->attach( array( 'customer' => $customerId ) );
			UserDetail::where('user_id', auth()->user()->id)->update(['stripe_customer_id' => $customerId, 'stripe_payment_id' => $stripe_payment_id]);
		} else {
			$stripe_payment_id = $request->get('payment_method_id');
			$customerId = auth()->user()->userDetail->stripe_customer_id;
			$paymentMethod = Stripe\PaymentMethod::retrieve( $stripe_payment_id );
			$paymentMethod->attach( array( 'customer' => $customerId ) );
			$customerData = [
				'invoice_settings' => ['default_payment_method' => $stripe_payment_id]
			];
			$customer = Stripe\Customer::update($customerId, $customerData);
			UserDetail::where('user_id', auth()->user()->id)->update(['stripe_customer_id' => $customerId, 'stripe_payment_id' => $stripe_payment_id]);
		}
		parse_str($request->get('formData'), $formData);
		$planData = Plan::findOrFail($formData['planId']);
		$planAmount = $formData['planAmt']*100;
		$subscritionData = [
			'customer' => $customerId,
			'items' => [
				[
					'plan' => $planData->stripe_plan_id, 
					'quantity' => $formData['nosdriver']
				]
			],
		];
		if(isset($formData['couponcode']) && trim($formData['couponcode']) != '') {
			$subscritionData['coupon'] = strtolower(trim($formData['couponcode']));
		}
		//dd($formData);
		$intent = \Stripe\Subscription::create($subscritionData)->toArray();
		//dd($intent);
		if($intent['status'] == 'active'){
			$addons = $formData['addons'];
			$modIds = implode(',', array_keys($addons));
			$subscribeData = [
				'user_id' 				=> auth()->user()->id,
				'plan_id' 				=> $planData->id,
				'order_limit' 			=> $planData->order_limit,
				'plan_order_limit' 		=> $planData->order_limit,
				'available_order_limit' => $planData->order_limit,
				'drivers' 				=> $formData['nosdriver'],
				'stripe_subscribe_id' 	=> $intent['id'],
				'sms_credit' 			=> 20,
				'billing_status' 		=> 1,
				'modules' 				=> serialize($addons),
				'moduleids' 			=> $modIds,
				'amount' 				=> $planAmount,
				'driver_amount' 		=> 0
			];
			SubscriptionHistory::create($subscribeData);
			Subscription::where('user_id', auth()->user()->id)->update($subscribeData);	
			$invoiceStripe = \Stripe\Invoice::retrieve($intent['latest_invoice'])->toArray();
			//dd($invoiceStripe);
			$invoiceData = [
				'user_id' 		=> auth()->user()->id,
				'amount' 		=> $planAmount,
				'details' 		=> serialize($formData),
				'status' 		=> 1,
				'transction_id' => $intent['id']
			];
			$invoice = Invoice::create($invoiceData);
			$paymentHistoryData = [
				'user_id' 		 => auth()->user()->id,
				'invoice_id' 	 => $invoice->id,
				'amount' 		 => $planAmount,
				'payment_type' 	 => 'Subscription Created',
				'payment_status' => 1,
				'response' 		 => serialize($intent),
				'stripe_invoice' => serialize($invoiceStripe),
			];
			PaymentHistory::create($paymentHistoryData);
			$billingData = [
				'user_id' => auth()->user()->id,
				'address' => $formData['address'],
				'postcode' => $formData['postcode'],
				'city' => $formData['city'],
				'state' => $formData['state'],
				'country_id' => $formData['country']
			]; 
			if(is_null(auth()->user()->billing)){
				BillingAddress::create($billingData);
			} else {
				BillingAddress::where('user_id', auth()->user()->id)->update($billingData);
			}
			$request->session()->flash('success', 'Subscription Successfully!!!'); 
			$responseData = ['status' => 'success', 'msg' => 'Subscription Successfully!!!'];
		} else {
			$responseData = ['status' => 'error', 'msg' => 'Please Try Again!!!'];
		}
		echo json_encode($responseData);
		die;
    }
	
    public function stripePayUpdateSub(Request $request)
    {
		Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
		$customerId = auth()->user()->userDetail->stripe_customer_id;
		$stripe_payment_id = auth()->user()->userDetail->stripe_payment_id;
		$stripe_subscribe_id = auth()->user()->userSubscription->stripe_subscribe_id;
		$subscription = \Stripe\Subscription::retrieve($stripe_subscribe_id);
		$formData = $request->all();
		$planData = Plan::findOrFail($formData['planId']);
		$planAmount = ($planData->amount)*100;
		$subscritionData = [
				'cancel_at_period_end' => false,
  				'proration_behavior' => 'always_invoice',
				'items' => [
					[
						'id' => $subscription->items->data[0]->id,
						'plan' => $planData->stripe_plan_id, 
						'quantity' => $formData['nosDriver']
					]
				]
		];
		$intent = \Stripe\Subscription::update($stripe_subscribe_id, $subscritionData)->toArray();
		if($intent['status'] == 'active'){
			$subscribeData = [
				'user_id' 				=> auth()->user()->id,
				'plan_id' 				=> $planData->id,
				'order_limit' 			=> $planData->order_limit,
				'plan_order_limit' 		=> $planData->order_limit,
				'available_order_limit' => $planData->order_limit,
				'drivers' 				=> $formData['nosDriver'],
				'stripe_subscribe_id' 	=> $intent['id'],
				'billing_status' 		=> 1,
				'modules' 				=> $planData->addons,
				'moduleids' 			=> $planData->addons,
				'amount' 				=> $planAmount,
				'driver_amount' 		=> 0
			];
			SubscriptionHistory::create($subscribeData);
			Subscription::where('user_id', auth()->user()->id)->update($subscribeData);	
			$invoiceStripe = \Stripe\Invoice::retrieve($intent['latest_invoice'])->toArray();
			//dd($invoiceStripe);
			$invoiceData = [
				'user_id' 		=> auth()->user()->id,
				'amount' 		=> $planAmount,
				'details' 		=> serialize($formData),
				'status' 		=> 1,
				'transction_id' => $intent['id']
			];
			$invoice = Invoice::create($invoiceData);
			$paymentHistoryData = [
				'user_id' 		 => auth()->user()->id,
				'invoice_id' 	 => $invoice->id,
				'amount' 		 => $planAmount,
				'payment_type' 	 => 'Subscription Updated',
				'payment_status' => 1,
				'response' 		 => serialize($intent),
				'stripe_invoice' => serialize($invoiceStripe),
			];
			PaymentHistory::create($paymentHistoryData);
			return back()->withSuccess('Subscription updated successfully!!!'); 
		} else {
			return back()->withError('Please Try Again!!!'); 
		}
		return back()->withError('Please Try Again!!!'); 
    }
	
    public function stripePayUpdateCard(Request $request)
    {
		Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
		if(auth()->user()->userDetail->stripe_customer_id == '' || auth()->user()->userDetail->stripe_payment_id == '')
		{
			$stripe_payment_id = $request->get('payment_method_id');
			$customerData = [
				'email'            => auth()->user()->email,
				'name'			   => auth()->user()->name,
				'payment_method'   => $stripe_payment_id,
				'invoice_settings' => ['default_payment_method' => $stripe_payment_id]
			];
			$customer = Stripe\Customer::create($customerData);
			$customerId = $customer->id;
		} else {
			$stripe_payment_id = $request->get('payment_method_id');
			$customerId = auth()->user()->userDetail->stripe_customer_id;
			$paymentMethod = Stripe\PaymentMethod::retrieve( $stripe_payment_id );
			$paymentMethod->attach( array( 'customer' => $customerId ) );
			$customerData = [
				'invoice_settings' => ['default_payment_method' => $stripe_payment_id]
			];
			$customer = Stripe\Customer::update($customerId, $customerData);
		}
		if($customerId){
			UserDetail::where('user_id', auth()->user()->id)->update(['stripe_customer_id' => $customerId, 'stripe_payment_id' => $stripe_payment_id]);
			$request->session()->flash('success', 'Card Update Successfully!!!'); 
			$responseData = ['status' => 'success', 'msg' => 'Card Update Successfully!!!'];
		} else {
			$responseData = ['status' => 'error', 'msg' => 'Please Try Again!!!'];
		}
		echo json_encode($responseData);
		die;
    }
	
    public function stripePayCancel(Request $request)
    {
		Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
		$stripe_customer_id = auth()->user()->userDetail->stripe_customer_id;
		$stripe_payment_id = auth()->user()->userDetail->stripe_payment_id;
		$stripe_subscribe_id = auth()->user()->userSubscription->stripe_subscribe_id;
		$userId = auth()->user()->id;
		if($stripe_subscribe_id){
			$cu = \Stripe\Customer::retrieve( $stripe_customer_id );
			$subscription = $cu->subscriptions->retrieve( $stripe_subscribe_id );
			if(!$subscription){
				return redirect()->route('admin.billing', $id)->withError('Please Try Again!!!');
			}
			$intent = $subscription->cancel()->toArray();
			//dd($intent);
			if($intent['status'] == 'canceled'){
				$subscribeData = [
					'user_id' 				=> auth()->user()->id,
					'plan_id' 				=> 0,
					'order_limit' 			=> 0,
					'plan_order_limit' 		=> 0,
					'available_order_limit' => 0,
					'drivers' 				=> 0,
					'stripe_subscribe_id' 	=> '',
					'sms_credit' 			=> 0,
					'billing_status' 		=> 1,
					'modules' 				=> '',
					'moduleids' 			=> '',
					'amount' 				=> 0,
					'driver_amount' 		=> 0
				];
				SubscriptionHistory::create($subscribeData);
				Subscription::where('user_id', $userId)->update($subscribeData);
				$invoiceData = [
					'user_id' 		=> $userId,
					'amount' 		=> 0,
					'details' 		=> serialize(['']),
					'status' 		=> 1,
					'transction_id' => $intent['id']
				];
				$invoice = Invoice::create($invoiceData);
				$paymentHistoryData = [
					'user_id' 		 => $userId,
					'invoice_id' 	 => $invoice->id,
					'amount' 		 => 0,
					'payment_status' => 1,
					'response' 		 => serialize($intent),
				];
				PaymentHistory::create($paymentHistoryData);
				return redirect()->route('admin.billing')->withSuccess('Subscription Cancel Successfully!!!');
				$responseData = ['status' => 'success', 'msg' => 'Subscription Cancel Successfully!!!'];
			} else {
				return redirect()->route('admin.billing')->withError('Please Try Again!!!');
				$responseData = ['status' => 'error', 'msg' => 'Please Try Again!!!'];
			}
		} else {
			return redirect()->route('admin.billing')->withError('Please Try Again!!!');
			$responseData = ['status' => 'error', 'msg' => 'Please Try Again!!!'];
		}
		echo json_encode($responseData);
		die;
    }
	
    public function stripePayCredit(Request $request)
    {
		$postData = $request->all();
		$userId = auth()->user()->id;
		$validData = Validator::make($postData, [
            'creditamount' => ['required', 'numeric', 'between:0,1000']
        ]);
		if($validData->passes())
		{
			Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
			$amount = $postData['creditamount']*100;
			$stripe_customer_id = auth()->user()->userDetail->stripe_customer_id;
			$stripe_payment_id = auth()->user()->userDetail->stripe_payment_id;
			$paymentIntentParameters = array(
				//'payment_method'      => $stripe_payment_id,
				'amount'              => $amount,
				'currency'            => 'usd',
				'description' 		  => 'Manual SMS Credit Purchase',
				//'confirmation_method' => 'manual',
				//'confirm'             => true,
				'customer'            => $stripe_customer_id,
			);
			$intent = \Stripe\Charge::create( $paymentIntentParameters )->toArray();
			if($intent['status'] == 'succeeded'){
				$subscribeData = [
					'user_id' 				=> auth()->user()->id,
					'sms_credit' 			=> (auth()->user()->userSubscription->sms_credit)+$postData['creditamount'],
				];
				SubscriptionHistory::create($subscribeData);
				Subscription::where('user_id', $userId)->update($subscribeData);
				$invoiceData = [
					'user_id' 		=> $userId,
					'amount' 		=> $amount,
					'details' 		=> serialize($postData),
					'status' 		=> 1,
					'transction_id' => $intent['id']
				];
				$invoice = Invoice::create($invoiceData);
				$paymentHistoryData = [
					'user_id' 		 => $userId,
					'invoice_id' 	 => $invoice->id,
					'amount' 		 => $amount,
					'payment_type' 	 => 'Manual SMS Credit Purchase',
					'payment_status' => 1,
					'response' 		 => serialize($intent),
					'stripe_invoice'=> serialize($intent),
				];
				PaymentHistory::create($paymentHistoryData);
			} else {								
				$invoiceData = [
					'user_id' 		=> $userId,
					'amount' 		=> $amount,
					'details' 		=> serialize($postData),
					'status' 		=> 0,
					'transction_id' => $intent['id']
				];
				$invoice = Invoice::create($invoiceData);
				$paymentHistoryData = [
					'user_id' 		 => $userId,
					'invoice_id' 	 => $invoice->id,
					'amount' 		 => $amount,
					'payment_status' => 0,
					'response' 		 => serialize($intent),
				];
				PaymentHistory::create($paymentHistoryData);
			}
			return back()->withSuccess('Add Credits Successfully!!!');
		} else {
            $messages = $validData->messages();
            return back()->withInput()->withErrors($messages);			
		}
    }
	
    public function stripePayAutorc(Request $request)
    {
		$postData = $request->all();
		$userId = auth()->user()->id;
		$validData = Validator::make($postData, [
            'min_balance' => ['required', 'numeric', 'between:0,1000'],
            'creditamount' => ['required', 'numeric', 'between:0,1000']
        ]);
		if($validData->passes())
		{
			$userId = auth()->user()->id;
			$rcData = [
				'user_id' 		=> $userId,
				'status' 		=> (isset($postData['status']) ? $postData['status'] : 0),
				'amount' 		=> $postData['creditamount'],
				'min_balance' 	=> $postData['min_balance']
			];	
			if(is_null(auth()->user()->autorc)){
				Autorecharge::create($rcData);
			} else {
				Autorecharge::where('user_id', $userId)->update($rcData);
			}
			return back()->withSuccess('Auto Recharge Request Successfully!!!');
		} else {
            $messages = $validData->messages();
            return back()->withInput()->withErrors($messages);			
		}
    }
	
    public function stripePayRemoveCard(Request $request)
    {
		$postData = $request->all();
		Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
		/*$customerId = auth()->user()->userDetail->stripe_customer_id;
		$customer = Stripe\Customer::retrieve($customerId);
		dd($customer);*/
		$paymentMethod = Stripe\PaymentMethod::retrieve($postData['card_id']);
		$cards = $paymentMethod->detach();
		/*$customerData = [
			'invoice_settings' => ['default_payment_method' => $stripe_payment_id]
		];
		$customer = Stripe\Customer::update($customerId, $customerData);*/
		//$cards = Stripe\PaymentMethod::detach($postData['card_id'])->toArray();
		return back()->withSuccess('Card Removed Successfully!!!');
		dd($cards);
    }
}
