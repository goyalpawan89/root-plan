<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserDetail;
use App\Models\Location;
use App\Models\Address;
use App\Models\Driver;
use App\Models\Order;
use Carbon\Carbon;
use App\User;
use File;
use App\Imports\OrdersImport;
use Excel;
use Maatwebsite\Excel\HeadingRowImport;

class OrderController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }	  
	
    public function import()
    { 
		$rowStatuses = [];
        return view('admin.order.import', compact('rowStatuses'));
    } 
	
    public function importSubmit(Request $request)
    {
		if($request->ajax())
		{
			$postData = $request->all();
			$responseData = [];
			$orders = [];
			$headings = [];
			$haddingOptions = [];
			if($request->hasFile('import')){
				$orderDate = Carbon::parse($postData['orderdate'])->format('Y-m-d H:i:s'); 
				$postData['orderdate'] = $orderDate;
				$postData['titleRow'] = [];
				$passData = $postData;
				unset($passData['_token']);
				unset($passData['import']);
				$extension = File::extension(request()->file('import')->getClientOriginalName());
   				if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
					$orders = (new OrdersImport($passData))->toArray(request()->file('import'));
					$headings = (new HeadingRowImport)->toArray(request()->file('import'));
					$haddingOptions = [
										'Ignore' 	=> 'Ignore', 
										'Address' 	=> 'Address', 
										'Priority' 	=> 'Priority', 
										'Name' 		=> 'Name', 
										'Email' 	=> 'Email', 
										'Phone' 	=> 'Phone', 
										'Gate Code' => 'Gate Code', 
										'Notes' 	=> 'Notes'
									];
    				if(!empty($orders))
					{
						$headings = $headings[0][0];
						$orders = $orders[0];
					}
					//dd($orders);
				}
			}
			return view('admin.order.importpreview', compact('orders', 'headings', 'haddingOptions'));
			dd($postData);
		} else {
			$postData = $request->all();
			$orderDate = Carbon::parse($postData['orderdate'])->format('Y-m-d H:i:s'); 
			$postData['orderdate'] = $orderDate;
			$postData['titleRow'] = array_diff( $postData['titleRow'], ['Ignore' 	=> 'Ignore'] );
			$passData = $postData;
			unset($passData['_token']);
			unset($passData['import']);
			//dd($passData);
			$import = new OrdersImport($passData);
			$sheetData = Excel::import($import, request()->file('import'));
			$rowStatuses = $import->getRowStatus();
			//dd($import->getRowStatus());
			//return back()->withSuccess('Import Successfully!!!');
			return view('admin.order.import', compact('rowStatuses'));
		}
    }
	
    public function showRoutesOrders(Request $request)
    { 
		if($request->ajax())
		{
			$columns = array('sno', 'id', 'priority', 'schduled', 'service_start', 'service_end', 'duration', 'location_id', 'name', 'gatecode', 'stoup_num', 'address');
			$postData = $request->all();
			//dd($postData);
			$limit = $request->get('length');
        	$start = $request->get('start');
			$orderData = $request->get('order');
			$searchData = $request->get('search');
			$columnsData = $request->get('columns');
        	$order = $columns[$orderData[0]['column']];
        	$dir = $orderData[0]['dir'];
			$searchByRole = '';
			$searchByStatus = trim($columnsData[2]['search']['value']);
			$userId = auth()->user()->id;
			//$driverId = $postData['driver_id'];
			$totalFiltered = $usercount = Order::where('user_id', $userId)->count();
			$search = $searchData['value'];
			$postsResult = Order::where('user_id', $userId)
						   ->when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                               
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('schduled', 'LIKE',"%{$search}%")
                                                  ->orWhere('service_start', 'LIKE',"%{$search}%")
                                                  ->orWhere('service_end', 'LIKE',"%{$search}%");
                               });
                           })
                           ->offset($start)->limit($limit)->orderBy($order, $dir)->get();
			$totalFiltered = Order::where('user_id', $userId)
						   ->when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                            
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('schduled', 'LIKE',"%{$search}%")
                                                  ->orWhere('service_start', 'LIKE',"%{$search}%")
                                                  ->orWhere('service_end', 'LIKE',"%{$search}%");
                               });
                           })
                           ->count();
			$posts = [];
			$sno = $start;
			foreach($postsResult as $ky => $post){
				$nullDriver = (is_null($post->driver) ? 0 : 1);
				$ststusTxt = ($post->status_id == 1 ? '<i class="fa fa-toggle-on fa-2x grntxt" aria-hidden="true"></i>' : '<i class="fa fa-toggle-off fa-2x graytxt" aria-hidden="true"></i>');
				$themeTxt = ($nullDriver ? '<i class="fa fa-square fa-2x" aria-hidden="true" style="color:'.$post->driver->colortheme.';"></i>' : '<i class="fa fa-square fa-2x" aria-hidden="true"></i>');
				$editUrl = route('admin.drivers.edit', $post->id);
				$showUrl = route('admin.drivers.show', $post->id);
				$subData = [];
				$subData['sno'] = ++$sno;
				$subData['id'] = $post->id;
				$subData['priority'] = $post->priority;
				$subData['schduled'] = Carbon::parse($post->schduled)->format('M d, Y');				
				$subData['service_start'] = (is_null($post->service_start) ? 'N/A' : Carbon::parse($post->service_start)->format('M d, Y H:i'));
				$subData['service_end'] = (is_null($post->service_end) ? 'N/A' : Carbon::parse($post->service_end)->format('M d, Y H:i'));
				$diff_in_minutes = 'N/A';
				if(!is_null($post->service_start) && !is_null($post->service_end)){
					$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $post->service_end);
					$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $post->service_start);
					$diff_in_minutes = $to->diffInMinutes($from);
				}
				$subData['duration'] = $post->duration.' ('.$diff_in_minutes.')';
				$subData['location_id'] = $post->location->name;
				$subData['name'] = (is_null($post->driver) ? 'N/A' : $post->driver->name);
				$subData['gatecode'] = $post->location->gatecode;
				$subData['stoup_num'] = $post->stoup_num;
				$subData['address'] = $post->location->address;
				$posts[] = $subData;
			}			
			$json_data = array(
				"draw"            => intval($request->get('draw')),  
				"recordsTotal"    => intval($usercount),  
				"recordsFiltered" => intval($totalFiltered), 
				"data"            => $posts   
			);
			return json_encode($json_data);
		}	
    }
}
