<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserDetail;
use App\Models\Address;
use App\Models\Driver;
use App\Models\Order;
use Carbon\Carbon;
use App\User;

class DriverController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }	
    
    public function index(Request $request)
    {
		if($request->ajax())
		{
			$columns = array('id', 'colortheme', 'status_id', 'name', 'worktime', 'email', 'vehicle', 'phone');
			$postData = $request->all();
			$limit = $request->get('length');
        	$start = $request->get('start');
			$orderData = $request->get('order');
			$searchData = $request->get('search');
			$columnsData = $request->get('columns');
        	$order = $columns[$orderData[0]['column']];
        	$dir = $orderData[0]['dir'];
			$searchByRole = '';
			$searchByStatus = trim($columnsData[2]['search']['value']);
			$totalFiltered = $usercount = User::where('role', 3)
											   ->whereHas('driver', function($q){
													$q->where('parent_id', auth()->user()->id);
											   })->count();
			$search = $searchData['value'];
			$postsResult = User::where('role', 3)
						   ->whereHas('driver', function($q){
								$q->where('parent_id', auth()->user()->id);
						   })
						   ->when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                               
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('name', 'LIKE',"%{$search}%")
                                                  ->orWhere('email', 'LIKE',"%{$search}%")
                                                  ->orWhere('phone', 'LIKE',"%{$search}%");
                               });
                           })
                           ->offset($start)->limit($limit)->orderBy($order, $dir)->get();
			$totalFiltered = User::where('role', 3)
						   ->whereHas('driver', function($q){
								$q->where('parent_id', auth()->user()->id);
						   })
						   ->when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                            
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('name', 'LIKE',"%{$search}%")
                                                  ->orWhere('email', 'LIKE',"%{$search}%")
                                                  ->orWhere('phone', 'LIKE',"%{$search}%");
                               });
                           })
                           ->count();
			$posts = [];
			$sno = $start;
			foreach($postsResult as $ky => $post){
				//dd($post->driver);
				//$nullPlan = (is_null($post->userSubscription->plan) ? 0 : 1);
				$nullDriver = (is_null($post->driver) ? 0 : 1);
				$ststusTxt = ($post->status_id == 1 ? '<i class="fa fa-toggle-on fa-1x grntxt" aria-hidden="true"></i>' : '<i class="fa fa-toggle-off fa-1x graytxt" aria-hidden="true"></i>');
				$themeTxt = ($nullDriver ? '<i class="fa fa-square fa-1x" aria-hidden="true" style="color:'.$post->driver->colortheme.';"></i>' : '<i class="fa fa-square fa-1x" aria-hidden="true"></i>');
				$subData = [];
				$subData['id'] = ++$sno;
				$subData['colortheme'] = $themeTxt;
				$subData['status_id'] = '<div class="custom-control custom-switch"><input type="checkbox" '.($post->status_id == 1  ? 'checked' : '').' class="custom-control-input" id="customSwitch'.$post->id.'" name="'.$post->id.'"><label class="custom-control-label" for="customSwitch'.$post->id.'">&nbsp;</label></div>';
				//$subData['serial_no'] = ($nullDriver ? $post->driver->serial_no : 'N/A');
				$subData['name'] = $post->name;
				$subData['worktime'] = ($nullDriver ? $post->driver->worktime_start.'-'.$post->driver->worktime_end : 'N/A');
				$subData['email'] = $post->email;
				$subData['vehicle'] = ($nullDriver ? $post->driver->vehicle : 'N/A');
				$subData['phone'] = $post->phone;
				$editUrl = route('admin.drivers.edit', $post->id);
				$showUrl = route('admin.drivers.show', $post->id);
				$deleteUrl = route('admin.drivers.destroy', $post->id);
                $optionsData = '<a class="" href="'.$showUrl.'"><i class="fa fa-info-circle"></i></a>&nbsp;&nbsp;|&nbsp;&nbsp;';
                $optionsData .= '<a class="" href="'.$editUrl.'"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;|&nbsp;&nbsp;';
				$optionsData .= '<form action="'.$deleteUrl.'" method="post" style="display:inline-block;">';
				$optionsData .= '<a class="deleteitem" href=""><i class="fa fa-trash"></i></a>';
				$optionsData .= '<input name="_method" type="hidden" value="DELETE">';
				$optionsData .= '<input name="_token" type="hidden" value="'.csrf_token().'">';
				$optionsData .= '</form>';
				$subData['options'] = $optionsData;
				$posts[] = $subData;
			}			
			$json_data = array(
				"draw"            => intval($request->get('draw')),  
				"recordsTotal"    => intval($usercount),  
				"recordsFiltered" => intval($totalFiltered), 
				"data"            => $posts   
			);
			return json_encode($json_data);
		}	
		$status = ['Deactive', 'Active'];
		$customFields = unserialize(auth()->user()->userDetail->customfields);
		$hubs = Address::where('user_id', auth()->user()->id)->get()->toArray();
        return view('admin.driver.index', compact('status', 'customFields', 'hubs'));
    }
	
	public function store(Request $request)
    {
        $postData = $request->all();
		//dd($postData);
		$validData = Validator::make($postData, [
            'name' => ['required', 'string', 'max:255'],
            'vehicle' => ['required'],
            'worktime_start' => ['required'],
            'worktime_end' => ['required'],
            'start_location' => ['required'],
            'end_location' => ['required'],
            'colortheme' => ['required'],
            'image' => ['image'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required']
        ]);
		if($validData->passes())
		{
			$user = User::create([
				'first_name' => $postData['name'],
				'last_name' => '',
				'state' => '',
				'role' => '3',
				'name' => $postData['name'],
				'phone' => $postData['phone'],
				'status_id' => $postData['status_id'],
				'email' => $postData['email'],
				'password' => Hash::make($postData['email']),
			]);	
			$driverData = [
				'user_id' => $user->id,
				'parent_id' => auth()->user()->id,
				'serial_no' => $postData['serial_no'],
				'vehicle' => $postData['vehicle'],
				'worktime_start' => $postData['worktime_start'],
				'worktime_end' => $postData['worktime_end'],
				'fix_cost' => $postData['fix_cost'],
				'cost_per_mile' => $postData['cost_per_mile'],
				'cost_per_hour' => $postData['cost_per_hour'],
				'cost_per_hour_overtime' => $postData['cost_per_hour_overtime'],
				'start_location' => $postData['start_location'],
				'end_location' => $postData['end_location'],
				'colortheme' => $postData['colortheme'],
				'home_lat_lng_poly' => $postData['home_lat_lng_poly'],
				'home_lat_lng' => $postData['home_lat_lng'],
				'home_address' => $postData['home_address'],
				'customfields' => serialize($postData['customfields'])
			];
			if($request->hasFile('image')) {
				$image = $request->file('image');
				$fileName = time().'.'.$image->getClientOriginalExtension();
				$image->move(public_path('images/driver/logo'), $fileName);
				$driverData['image'] = $fileName;
			}	
			
			$hubData = [];
			$startHub = 'My Address';
			if($postData['start_location'] != 'My Address'){
				$startHub = Address::where('user_id', auth()->user()->id)->where('name', $postData['start_location'])->first()->toArray();
			}
			$endHub = 'My Address';
			if($postData['end_location'] != 'My Address'){
				$endHub = Address::where('user_id', auth()->user()->id)->where('name', $postData['end_location'])->first()->toArray();
			}
			$hubData['start_hub'] = $startHub;
			$hubData['end_hub'] = $endHub;
			$driverData['hub'] = serialize($hubData);
			$driver = Driver::create($driverData);	
			$user->sendEmailVerificationNotification();
			return redirect()->route('admin.drivers.index')->withSuccess('Driver Added Successfully!!!');
		} else {
            $messages = $validData->messages();
			//dd($messages);
            return back()->withInput()->withErrors($messages);			
		}
    }     
	
    public function create()
    {       	 
		$status = ['Deactive', 'Active'];
		$customFields = unserialize(auth()->user()->userDetail->customfields);
		$ehubs = (Address::where('user_id', auth()->user()->id)->get()->pluck('name', 'name')->toArray());
		$shubs = $ehubs+['My Address' => 'My Address'];
        return view('admin.driver.create', compact('status', 'customFields', 'shubs', 'ehubs'));
    }    
	
    public function edit($id)
    {
        $user = User::findOrFail($id);
		$user->driver->customfields = unserialize($user->driver->customfields);
		$status = ['Deactive', 'Active'];
		$customFields = unserialize(auth()->user()->userDetail->customfields);		
		$ehubs = (Address::where('user_id', auth()->user()->id)->get()->pluck('name', 'name')->toArray());
		$shubs = $ehubs+['My Address' => 'My Address'];
        return view('admin.driver.edit', compact('user', 'status', 'customFields', 'shubs', 'ehubs'));
    }
	
    public function update(Request $request, $id)
    {
        $postData = $request->all();
		//dd($postData);
		$validData = Validator::make($postData, [
            'name' => ['required', 'string', 'max:255'],
            'vehicle' => ['required'],
            'worktime_start' => ['required'],
            'worktime_end' => ['required'],
            'start_location' => ['required'],
            'end_location' => ['required'],
            'colortheme' => ['required'],
            'image' => ['image'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],
            'phone' => ['required']
        ]);
		if($validData->passes())
		{
			$user = User::where('id', $id)->update([
				'first_name' => $postData['name'],
				'last_name' => '',
				'name' => $postData['name'],
				'phone' => $postData['phone'],
				'status_id' => '1',
				'email' => $postData['email']
			]);		
			$driverData = [
				'serial_no' => $postData['serial_no'],
				'vehicle' => $postData['vehicle'],
				'worktime_start' => $postData['worktime_start'],
				'worktime_end' => $postData['worktime_end'],
				'fix_cost' => $postData['fix_cost'],
				'cost_per_mile' => $postData['cost_per_mile'],
				'cost_per_hour' => $postData['cost_per_hour'],
				'cost_per_hour_overtime' => $postData['cost_per_hour_overtime'],
				'start_location' => $postData['start_location'],
				'end_location' => $postData['end_location'],
				'colortheme' => $postData['colortheme'],
				'home_lat_lng_poly' => $postData['home_lat_lng_poly'],
				'home_lat_lng' => $postData['home_lat_lng'],
				'home_address' => $postData['home_address'],
				'customfields' => serialize($postData['customfields'])
			];
			$hubData = [];
			$startHub = 'My Address';
			if($postData['start_location'] != 'My Address'){
				$startHub = Address::where('user_id', auth()->user()->id)->where('name', $postData['start_location'])->first()->toArray();
			}
			$endHub = 'My Address';
			if($postData['end_location'] != 'My Address'){
				$endHub = Address::where('user_id', auth()->user()->id)->where('name', $postData['end_location'])->first()->toArray();
			}
			$hubData['start_hub'] = $startHub;
			$hubData['end_hub'] = $endHub;
			$driverData['hub'] = serialize($hubData);
			if($request->hasFile('image')) {
				$image = $request->file('image');
				$fileName = time().'.'.$image->getClientOriginalExtension();
				$image->move(public_path('images/driver/logo'), $fileName);
				$driverData['image'] = $fileName;
			}	
			$driver = Driver::where('user_id', $id)->update($driverData);	
			return redirect()->route('admin.drivers.index')->withSuccess('Driver Update Successfully!!!');
		} else {
            $messages = $validData->messages();
            return back()->withInput()->withErrors($messages);			
		}
    }
	
    public function show($id)
    { 
		$user = User::findOrFail($id);
        return view('admin.driver.show', compact('user'));
    } 
	
    public function destroy($id)
    {
		Driver::where('user_id', $id)->delete();
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('admin.drivers.index')->withSuccess('Driver Deleted Successfully!!!');
    }
	
    public function showDriverOrders(Request $request)
    { 
		if($request->ajax())
		{
			$columns = array('sno', 'id', 'priority', 'schduled', 'service_start', 'service_end', 'duration', 'location_id', 'name', 'gatecode', 'stoup_num', 'address');
			$postData = $request->all();
			//dd($postData);
			$limit = $request->get('length');
        	$start = $request->get('start');
			$orderData = $request->get('order');
			$searchData = $request->get('search');
			$columnsData = $request->get('columns');
        	$order = $columns[$orderData[0]['column']];
        	$dir = $orderData[0]['dir'];
			$searchByRole = '';
			$searchByStatus = trim($columnsData[2]['search']['value']);
			$userId = auth()->user()->id;
			$driverId = $postData['driver_id'];
			$totalFiltered = $usercount = Order::where('driver_id', $driverId)->where('user_id', $userId)->count();
			$search = $searchData['value'];
			$postsResult = Order::where('driver_id', $driverId)->where('user_id', $userId)
						   ->when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                               
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('schduled', 'LIKE',"%{$search}%")
                                                  ->orWhere('service_start', 'LIKE',"%{$search}%")
                                                  ->orWhere('service_end', 'LIKE',"%{$search}%");
                               });
                           })
                           ->offset($start)->limit($limit)->orderBy($order, $dir)->get();
			$totalFiltered = Order::where('driver_id', $driverId)->where('user_id', $userId)
						   ->when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                            
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('schduled', 'LIKE',"%{$search}%")
                                                  ->orWhere('service_start', 'LIKE',"%{$search}%")
                                                  ->orWhere('service_end', 'LIKE',"%{$search}%");
                               });
                           })
                           ->count();
			$posts = [];
			$sno = $start;
			foreach($postsResult as $ky => $post){
				$nullDriver = (is_null($post->driver) ? 0 : 1);
				$ststusTxt = ($post->status_id == 1 ? '<i class="fa fa-toggle-on fa-2x grntxt" aria-hidden="true"></i>' : '<i class="fa fa-toggle-off fa-2x graytxt" aria-hidden="true"></i>');
				$themeTxt = ($nullDriver ? '<i class="fa fa-square fa-2x" aria-hidden="true" style="color:'.$post->driver->colortheme.';"></i>' : '<i class="fa fa-square fa-2x" aria-hidden="true"></i>');
				$editUrl = route('admin.drivers.edit', $post->id);
				$showUrl = route('admin.drivers.show', $post->id);
				$subData = [];
				$subData['sno'] = ++$sno;
				$subData['id'] = $post->id;
				$subData['priority'] = $post->priority;
				$subData['schduled'] = Carbon::parse($post->schduled)->format('M d, Y');				
				$subData['service_start'] = (is_null($post->service_start) ? 'N/A' : Carbon::parse($post->service_start)->format('M d, Y H:i'));
				$subData['service_end'] = (is_null($post->service_end) ? 'N/A' : Carbon::parse($post->service_end)->format('M d, Y H:i'));
				$diff_in_minutes = 'N/A';
				if(!is_null($post->service_start) && !is_null($post->service_end)){
					$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $post->service_end);
					$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $post->service_start);
					$diff_in_minutes = $to->diffInMinutes($from);
				}
				$subData['duration'] = $post->duration.' ('.$diff_in_minutes.')';
				$subData['location_id'] = $post->location->name;
				$subData['name'] = $post->driver->name;
				$subData['gatecode'] = $post->location->gatecode;
				$subData['stoup_num'] = $post->stoup_num;
				$subData['address'] = $post->location->address;
				$posts[] = $subData;
			}			
			$json_data = array(
				"draw"            => intval($request->get('draw')),  
				"recordsTotal"    => intval($usercount),  
				"recordsFiltered" => intval($totalFiltered), 
				"data"            => $posts   
			);
			return json_encode($json_data);
		}	
    }
}
