<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserDetail;
use App\Models\Location;
use App\Models\Address;
use App\Models\Driver;
use App\Models\Order;
use Carbon\Carbon;
use App\User;
use App\Imports\OrdersImport;
use Maatwebsite\Excel\Facades\Excel;

class PlanController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }	
	
    public function setOrdersPlaning(Request $request)
    {
		if($request->ajax())
		{
			$postData = $request->all();
			$planDate = Carbon::parse($postData['planDate'])->format('Y-m-d H:i:s');
			$planDay = Carbon::parse($postData['planDate'])->format('l');
			$pDrivers = $postData['pDrivers'];
			$driversData = [];
			$orderssData = [];
			$responseData = [];
			$availableDriversForOrder = [];
			foreach($pDrivers as $did){
				$driversData[$did] = User::where('role', 3)
											->where('id', $did)
										   	->whereHas('driver', function($q){
												$q->where('parent_id', auth()->user()->id);
										   	})->first();
			}
			//dd($driversData);
			$ordrData = Order::where('user_id', auth()->user()->id)
									->where('status', 1)
									->whereDate('schduled', $planDate)
									->select('id')
									->get();
			$countDrivers = count($pDrivers);
			$ordersResponseData = [];
			foreach($ordrData as $ops => $order)
			{
				$orderData = Order::findOrFail($order->id);
				if(is_null($orderData->location) || is_null($orderData->location->addresses))
				{					
					continue;
				}
				if(is_null($orderData->location->dayaddress) && is_null($orderData->location->defaultaddress))
				{
					continue;
				}
				$deliveryAddress = (!is_null($orderData->location->dayaddress) ? $orderData->location->dayaddress : $orderData->location->defaultaddress);
				$orderssData[$order->id] = [];
				$orderssData[$order->id]['orderData'] = $orderData;
				$orderssData[$order->id]['deliveryAddress'] = $deliveryAddress;
				//dd($deliveryAddress);
				/*$driverPos = ($ops < $countDrivers ? $ops : fmod($ops, $countDrivers));
				$orderData = Order::findOrFail($order->id);
				$orderData->update(['driver_id' => $pDrivers[$driverPos]]);
				//dd($order->driver);
				$orderResponseData = [];
				$orderResponseData['id'] = $orderData->id;
				$orderResponseData['drivername'] = $orderData->driver->name;
				$orderResponseData['schduled'] = Carbon::parse($orderData->schduled)->format('M d, Y');
				$orderResponseData['vehicle'] = $orderData->driver->driver->vehicle;
				$ordersResponseData[] = $orderResponseData;*/
				foreach($driversData as $did => $driverData)
				{
					if(empty($driverData->driver->home_lat_lng_poly)){
						continue;
					}
					$hubData = json_decode($driverData->driver->home_lat_lng_poly, true);
					// x-coordinates of the vertices of the polygon
					$vertices_x = array_column($hubData, 'lng');
					// y-coordinates of the vertices of the polygon
					$vertices_y = array_column($hubData, 'lat');
					//dd($hubData['latlng']);
					$points_polygon = count($vertices_x) - 1;  // number vertices - zero-based array
					$longitude_x = $deliveryAddress->longitude;  // x-coordinate of the point to test
					$latitude_y = $deliveryAddress->latitude;    // y-coordinate of the point to test
					if ($this->isInPolygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y))
					{
						$availableDriversForOrder[$order->id][] = $did;
						//break;
					}
				}
			}
			dd($vertices_x);
			if(empty($availableDriversForOrder)) {
				$responseData = ['status' => false, 'message' => 'No Drivers Available For Order Routes. Please choose another driver and try again.'];
			} else {
				$responseData = ['status' => false, 'message' => 'No Drivers Available For Order Routes'];
				$responseData = $availableDriversForOrder;
			}
			echo json_encode($responseData);
			die;
			dd($ordrData);
		}
    }
	
    public function getOrdersDriversByDate(Request $request)
    { 
		if($request->ajax())
		{
			$postData = $request->all();
			$planDate = Carbon::parse($request->get('date'))->format('Y-m-d');
			$driversData = Driver::where('drivers.parent_id', auth()->user()->id)
									->join('users', 'drivers.user_id', '=', 'users.id')
									->where('users.status_id', 1)
									->select('users.id', 'users.name', 'drivers.vehicle', 'drivers.worktime_start', 'drivers.worktime_end')
									->get()->toArray();
			$ordersData = Order::where('user_id', auth()->user()->id)
									->where('status', 1)
									->whereDate('schduled', $planDate)
									->count();
			$responseData = [];
			$responseData['drivers'] = $driversData;
			$responseData['countdrivers'] = count($driversData);
			$responseData['countorders'] = $ordersData;
			echo json_encode($responseData);
			die;
			dd($ordersData);
		}	
    }
	
	protected function isInPolygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)
	{
		$i = $j = $c = 0;
	  	for ($i = 0, $j = $points_polygon ; $i < $points_polygon; $j = $i++) {
			if ( (($vertices_y[$i]  >  $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
		 	($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]) ) )
		   		$c = !$c;
	  	}
	  	return $c;
	}
}
