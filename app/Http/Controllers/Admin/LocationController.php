<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LocationAddress;
use App\Models\UserDetail;
use App\Models\Location;

class LocationController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    
    public function index(Request $request)
    {
		if($request->ajax())
		{
			$columns = array('id', 'status', 'name', 'address', 'notes', 'gatecode', 'created_at');
			$postData = $request->all();
			$limit = $request->get('length');
        	$start = $request->get('start');
			$orderData = $request->get('order');
			$searchData = $request->get('search');
			$columnsData = $request->get('columns');
        	$order = $columns[$orderData[0]['column']];
        	$dir = $orderData[0]['dir'];
			$searchByRole = '';
			$searchByStatus = trim($columnsData[2]['search']['value']);
			$totalFiltered = $usercount = Location::where('user_id', auth()->user()->id)->count();
			$search = $searchData['value'];
			$postsResult = Location::where('user_id', auth()->user()->id)
						   ->when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                               
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('name', 'LIKE',"%{$search}%")
                                                  ->orWhere('email', 'LIKE',"%{$search}%");
                               });
                           })
                           ->offset($start)->limit($limit)->orderBy($order, $dir)->get();
			$totalFiltered = Location::where('user_id', auth()->user()->id)
						   ->when($searchByStatus != '', function($query) use ($searchByStatus){
                                return $query->where('status', 'LIKE', "{$searchByStatus}");
                           })                            
                           ->when(!empty($searchData['value']), function($query) use ($search){
                               return $query->where(function ($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                  ->orWhere('name', 'LIKE',"%{$search}%")
                                                  ->orWhere('email', 'LIKE',"%{$search}%");
                               });
                           })
                           ->count();
			$posts = [];
			$sno = $start;
			foreach($postsResult as $ky => $post){
				$subData = [];
				$ststusTxt = ($post->status == 1 ? '<i class="fa fa-toggle-on fa-2x grntxt" aria-hidden="true"></i>' : '<i class="fa fa-toggle-off fa-2x graytxt" aria-hidden="true"></i>');
				$subData['id'] = ++$sno;
				$subData['status'] = '<div class="custom-control custom-switch"><input type="checkbox" '.($post->status == 1  ? 'checked' : '').' class="custom-control-input" id="customSwitch'.$post->id.'" name="'.$post->id.'"><label class="custom-control-label" for="customSwitch'.$post->id.'">&nbsp;</label></div>';
				$subData['name'] = $post->name;
				$subData['address'] = (is_null($post->defaultaddress) ? 'N/A' : $post->defaultaddress->address);
				$subData['notes'] = $post->notes;
				$subData['gatecode'] = $post->gatecode;
				//$subData['check_in_time'] = $post->check_in_time;
				$editUrl = route('admin.customers.edit', $post->id);
				$deleteUrl = route('admin.customers.destroy', $post->id);
                $optionsData = '<a class="" href="'.$editUrl.'"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;|&nbsp;&nbsp;';
				$optionsData .= '<form action="'.$deleteUrl.'" method="post" style="display:inline-block;">';
				$optionsData .= '<a class="deleteitem" href=""><i class="fa fa-trash"></i></a>';
				$optionsData .= '<input name="_method" type="hidden" value="DELETE">';
				$optionsData .= '<input name="_token" type="hidden" value="'.csrf_token().'">';
				$optionsData .= '</form>';
				$subData['created_at'] = $optionsData;
				$posts[] = $subData;
			}			
			$json_data = array(
				"draw"            => intval($request->get('draw')),  
				"recordsTotal"    => intval($usercount),  
				"recordsFiltered" => intval($totalFiltered), 
				"data"            => $posts   
			);
			return json_encode($json_data);
		}	
         return view('admin.customer.index');
    }
	
	public function store(Request $request)
    {
        $postData = $request->all();
		//dd($postData);
		$validData = Validator::make($postData, [            
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
        ]);
		if($validData->passes())
		{
			$locationData = [];
			$locationData['user_id'] = auth()->user()->id;
			$locationData['name'] = $request->get('name');
			$locationData['email'] = $request->get('email');
			$locationData['phone'] = $request->get('phone');
			$locationData['check_in_time'] = $request->get('check_in_time');
			$locationData['gatecode'] = $request->get('gatecode');
			$locationData['defaultvehicle'] = $request->get('defaultvehicle');
			$locationData['notes'] = $request->get('notes');
			$location = Location::create($locationData);
			if(isset($postData['aname'])){
				foreach($postData['aname'] as $pos => $name){
					$addressData = [];
					$addressData['location_id'] = $location->id;
					$addressData['name'] = $postData['aname'][$pos];
					$addressData['address'] = $postData['address'][$pos];
					$addressData['latitude'] = $postData['latitude'][$pos];
					$addressData['longitude'] = $postData['longitude'][$pos];
					$addressData['is_default'] = (isset($postData['is_default']) && $postData['is_default'] == $pos ? 1 : 0);
					$addressData['sunday'] = (isset($postData['sunday']) && $postData['sunday'] == $pos ? 1 : 0);
					$addressData['monday'] = (isset($postData['monday']) && $postData['monday'] == $pos ? 1 : 0);
					$addressData['tuesday'] = (isset($postData['tuesday']) && $postData['tuesday'] == $pos ? 1 : 0);
					$addressData['wednesday'] = (isset($postData['wednesday']) && $postData['wednesday'] == $pos ? 1 : 0);
					$addressData['thursday'] = (isset($postData['thursday']) && $postData['thursday'] == $pos ? 1 : 0);
					$addressData['friday'] = (isset($postData['friday']) && $postData['friday'] == $pos ? 1 : 0);
					$addressData['saturday'] = (isset($postData['saturday']) && $postData['saturday'] == $pos ? 1 : 0);
					LocationAddress::create($addressData);
				}
			}
			return redirect()->route('admin.customers.index')->withSuccess('Customer Added Successfully!!!');
		} else {
            $messages = $validData->messages();
            return back()->withInput()->withErrors($messages);			
		}
    }    
	
    public function create()
    {		
        return view('admin.customer.create');
    }    
	
    public function edit($id)
    {
        $location = Location::findOrFail($id);
        return view('admin.customer.edit', compact('location'));
    }
	
    public function update(Request $request, $id)
    {
        $postData = $request->all();
		$validData = Validator::make($postData, [            
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
        ]);
		if($validData->passes())
		{			
			$locationData = [];
			$locationData['user_id'] = auth()->user()->id;
			$locationData['name'] = $request->get('name');
			$locationData['email'] = $request->get('email');
			$locationData['phone'] = $request->get('phone');
			$locationData['check_in_time'] = $request->get('check_in_time');
			$locationData['gatecode'] = $request->get('gatecode');
			$locationData['defaultvehicle'] = $request->get('defaultvehicle');
			$locationData['notes'] = $request->get('notes');
			$location = Location::findOrFail($id);
        	$location->update($locationData);
			LocationAddress::where('location_id', $id)->delete();
			if(isset($postData['aname'])){
				foreach($postData['aname'] as $pos => $name){
					$addressData = [];
					$addressData['location_id'] = $location->id;
					$addressData['name'] = $postData['aname'][$pos];
					$addressData['address'] = $postData['address'][$pos];
					$addressData['latitude'] = $postData['latitude'][$pos];
					$addressData['longitude'] = $postData['longitude'][$pos];
					$addressData['is_default'] = (isset($postData['is_default']) && $postData['is_default'] == $pos ? 1 : 0);
					$addressData['sunday'] = (isset($postData['sunday']) && $postData['sunday'] == $pos ? 1 : 0);
					$addressData['monday'] = (isset($postData['monday']) && $postData['monday'] == $pos ? 1 : 0);
					$addressData['tuesday'] = (isset($postData['tuesday']) && $postData['tuesday'] == $pos ? 1 : 0);
					$addressData['wednesday'] = (isset($postData['wednesday']) && $postData['wednesday'] == $pos ? 1 : 0);
					$addressData['thursday'] = (isset($postData['thursday']) && $postData['thursday'] == $pos ? 1 : 0);
					$addressData['friday'] = (isset($postData['friday']) && $postData['friday'] == $pos ? 1 : 0);
					$addressData['saturday'] = (isset($postData['saturday']) && $postData['saturday'] == $pos ? 1 : 0);
					LocationAddress::create($addressData);
				}
			}
			return redirect()->route('admin.customers.index')->withSuccess('Customer Update Successfully!!!');
		} else {
            $messages = $validData->messages();
            return back()->withInput()->withErrors($messages);			
		}
    }
	
    public function destroy($id)
    {
        $location = Location::findOrFail($id);
        $location->delete();
		LocationAddress::where('location_id', $id)->delete();
        return redirect()->route('admin.customers.index')->withSuccess('Customer Deleted Successfully!!!');
    }
	
	public function getLocationsList(Request $request)
	{
		$postData = $request->all();
		$searchData = $postData['term'];
		$filteredData =	Location::select('id', 'name', 'email', 'phone', 'notes')
								->where('user_id', auth()->user()->id)
								->where('status', 1)
								->where(function($q) use ($searchData) {
									 $q->where('name', 'LIKE',"%{$searchData}%")
									   ->orWhere('address', 'LIKE',"%{$searchData}%")
									   ->orWhere('email', 'LIKE',"%{$searchData}%");
								})
                           		->get()->toArray();
		$responseData = [];
		//$responseData['results'] = [];
		foreach($filteredData as $filtered){
			$responseData[] = [
				'value' => $filtered['id'], 
				'label' => $filtered['name']. '('.$filtered['email'].')',
				'name' => $filtered['name'],
				'email' => $filtered['email'],
				'phone' => $filtered['phone'],
				'notes' => $filtered['notes']
			];
		}
		echo json_encode($responseData);
		die;
	}
}
