<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Emailtemplte;
use App\Models\UserDetail;
use App\Models\Address;
use App\Models\Country;
use App\User;

class SettingController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    
    public function index(Request $request)
    {
		$userId = auth()->user()->id;
		$user = User::findOrFail($userId);
		$countries = Country::orderBy('countryName', 'asc')->pluck('countryName', 'isoAlpha3');
		//$zones = Zone::orderBy('zone_name', 'asc')->groupBy('zone_name')->pluck('zone_name', 'zone_name');
		$zones = root_time_zones();
		$emailTemplsts = Emailtemplte::where('user_id', $userId)->get()->keyBy('templatetype')->toArray();
		$addresses = Address::where('user_id', $userId)->get()->toArray();
		$emailTypes = ['alert' => 'Alert Message Template', 'deliverd' => 'Send Delivered Message Template '];
		$defaultmsg = 'alert';
		$user->userDetail->customfields = unserialize($user->userDetail->customfields);
        return view('admin.setting.index', compact('user', 'countries', 'zones', 'defaultmsg', 'emailTemplsts', 'emailTypes', 'addresses'));
    }
	
    public function update(Request $request, $id)
    {
		//dd($request->all()); 
		$postData = $request->all();
		if($request->get('submitConfig'))
		{
			$postDataDetail = $postData['userDetail'];
			$customFields = (isset($postDataDetail['customfields']) ? $postDataDetail['customfields'] : []);
			$postDataDetail['customfields'] = serialize(array_filter($customFields));
			if($request->hasFile('companylogo')) {
				$image = $request->file('companylogo');
				$fileName = time().'.'.$image->getClientOriginalExtension();
				$image->move(public_path('images/customers/logo'), $fileName);
				$postDataDetail['companylogo'] = $fileName;
			}
			$userDetail = UserDetail::where('user_id', $id)->first();
        	$userDetail->update($postDataDetail);
			$postDataUser = $postData['user'];
			unset($postDataUser['current_password']);
			unset($postDataUser['password']);
			unset($postDataUser['confirm-password']);
			$user = User::where('id', $id)->first();
        	$user->update($postDataUser);
			Address::where('user_id', $id)->delete();
			if(isset($postData['hub'])){
				foreach($postData['hub'] as $addressData){
					$addressData['user_id'] = $id;
					Address::create($addressData);
				}
			}
			return redirect()->route('admin.settings.index')->withSuccess('Customer Config Update Successfully!!!');
		}
		else if($request->get('submitBilling'))
		{
			$subscriptionDetail = Subscription::where('user_id', $id)->firstOrFail();
			$subscriptionData = [];
			$subscriptionData['plan_id'] = $request->get('plan_id');
			$subscriptionData['drivers'] = $request->get('drivers');
			$oldSubscriptionData = [];
			$oldSubscriptionData['plan_id'] = $subscriptionDetail->plan_id;
			$oldSubscriptionData['drivers'] = $subscriptionDetail->drivers;
			$subscriptionDetail->update($subscriptionData);
			$subscriptionCompareResult = array_diff($oldSubscriptionData, $subscriptionData);
			if(!empty($subscriptionCompareResult)){
				$subscriptionData['user_id'] = $id;
				SubscriptionHistory::create($subscriptionData);
			}
			return redirect()->route('superadmin.customers.edit', $id)->withSuccess('Customer Billing Update Successfully!!!');
		}
		else {
			return redirect()->route('superadmin.customers.edit', $id);
		}
    }
	
	public function zoneUpdate(Request $request)
	{
		$postData = $request->all();
		$userId = $request->get('user_id');
		unset($postData['_token']);
		UserDetail::where('user_id', $userId)->update($postData);
		die;
	}
	
	public function emailTemplateUpdate(Request $request)
	{
		$postData = $request->all();
		//dd($postData);
		$userId = auth()->user()->id;
		$templateType = $postData['userDetail']['templatetype'];
		$emailTemplate = Emailtemplte::where('user_id', $userId)->where('templatetype', $templateType)->first();
		if($emailTemplate){
			Emailtemplte::where('user_id', $userId)->where('templatetype', $templateType)->update($postData['userDetail']);
		} else {
			$postData['userDetail']['user_id'] = $userId;
			Emailtemplte::create($postData['userDetail']);
		}
		return redirect()->route('admin.settings.index')->withSuccess('Customer Config Update Successfully!!!');
	}	
}
