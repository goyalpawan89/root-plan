<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
		if(Auth::user()->role == 2){
			$allowedRoute = ['admin.billing', 'admin.stripe-pay'];
			if(Auth::user()->userSubscription->plan_id > 0 || in_array($request->route()->getName(), $allowedRoute)){
				return $next($request);
			} else {
				return redirect()->route('admin.billing');
			}            
        }
		return redirect('/')->with('error',"You don't have admin access.");
    }
}
