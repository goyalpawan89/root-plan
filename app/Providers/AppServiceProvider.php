<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Config;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadHelpers();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		$configs = Config::where('status', 1)->orderBy('id', 'asc')->pluck('value', 'name')->toArray();
		$arrKeys = str_replace(' ', '_', array_keys($configs));
		$configs = array_combine($arrKeys, array_values($configs));
		config()->set('configs', $configs);
		//config(['config', $configs]);
    }
	
	protected function loadHelpers()
    {
        foreach (glob(__DIR__.'/../Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }
}
