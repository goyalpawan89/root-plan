<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'first_name', 'last_name', 'phone', 'state', 'role', 'email', 'password', 'status_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];	
	
    public function userDetail()
    {
        return $this->hasOne('App\Models\UserDetail');
    }	
	
    public function userSubscription()
    {
        return $this->hasOne('App\Models\Subscription');
    }	
	
    public function driver()
    {
        return $this->hasOne('App\Models\Driver');
    }
	
    public function autorc()
    {
        return $this->hasOne('App\Models\Autorecharge');
    }	
	
    public function billing()
    {
        return $this->hasOne('App\Models\BillingAddress');
    }	
}
