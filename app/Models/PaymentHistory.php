<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model
{
    protected $fillable = ['user_id', 
						   'invoice_id', 
						   'amount', 
						   'payment_type', 
						   'payment_status', 
						   'stripe_invoice', 
						   'response'];	
	
    public function user()
    {
        return $this->belongsTo('App\User');
    }	
	
}