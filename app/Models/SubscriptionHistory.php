<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionHistory extends Model
{
    protected $fillable = ['user_id', 
						   'plan_id', 
						   'plan_order_limit', 
						   'available_order_limit', 
						   'drivers', 
						   'sms_credit', 
						   'billing_status', 
						   'modules',
						   'moduleids',
						   'stripe_subscribe_id',
						   'order_limit',
						   'driver_amount',
						   'amount'];	
	
    public function user()
    {
        return $this->belongsTo('App\User');
    }	
	
    public function plan()
    {
        return $this->belongsTo('App\Models\Plan');
    }
}
