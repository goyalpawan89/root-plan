<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocationAddress extends Model
{
    protected $fillable = ['location_id', 
						   'name',
						   'address', 
						   'latitude', 
						   'longitude',
						   'sunday',
						   'monday',
						   'tuesday',
						   'wednesday',
						   'thursday',
						   'friday',
						   'saturday',
						   'status',
						   'is_default'];
	
    public function location()
    {
        return $this->belongsTo('App\Models\Location');
    }		
}
