<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = ['user_id', 
						   'amount', 
						   'details', 
						   'status', 
						   'transction_id'];	
	
    public function user()
    {
        return $this->belongsTo('App\User');
    }		
}
