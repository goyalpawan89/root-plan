<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = ['plantype', 'planname', 'plandescription', 'isperdriver', 'amount', 'status', 'planorder', 'stripe_plan_id'];	
}
