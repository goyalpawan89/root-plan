<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $fillable = ['user_id', 
						   'payment_details', 
						   'paymentstatus', 
						   'mailerliteemail', 
						   'mailerliteid', 
						   'twilloaccountid', 
						   'twilloapptid',
						   'twillosmsrate',
						   'freshdeskemail',
						   'freshdeskid',
						   'amazonsestoken',
						   'company',
						   'address',
						   'city',
						   'state',
						   'postalcode',
						   'country',
						   'vatid',
						   'companylogo',
						   'timezone',
						   'customfields',
						   'stripe_customer_id',
						   'zone',
						   'zone_info',
						   'emailas',
						   'emailsubject',
						   'emailtemplate',
						   'smstemplate',
						   'stripe_payment_id',
						   'hub'];	
	
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
