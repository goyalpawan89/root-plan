<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id', 
						   'driver_id', 
						   'location_id', 
						   'priority', 
						   'schduled',
						   'service_start',
						   'service_end',
						   'duration',
						   'stoup_num',
						   'location_data',
						   'customfield_1',
						   'customfield_2',
						   'customfield_3',
						   'customfield_4',
						   'customfield_5',
						   'signature',
						   'comments',
						   'notes',
						   'status'];	
	
    public function user()
    {
        return $this->belongsTo('App\User');
    }			
	
    public function location()
    {
        return $this->belongsTo('App\Models\Location');
    }			
	
    public function driver()
    {
        return $this->belongsTo('App\User', 'driver_id');
    }		
}
