<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Emailtemplte extends Model
{
    protected $fillable = ['user_id', 
						   'templatetype',
						   'emailas',
						   'emailsubject',
						   'emailtemplate',
						   'smstemplate',
						   'extra'];
}
