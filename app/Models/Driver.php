<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = ['user_id', 
						   'parent_id', 
						   'serial_no', 
						   'vehicle', 
						   'worktime_start',
						   'worktime_end',
						   'fix_cost',
						   'cost_per_mile',
						   'cost_per_hour',
						   'cost_per_hour_overtime',
						   'start_location',
						   'end_location', 	
						   'customfields', 	
						   'hub', 	
						   'home_address', 	
						   'home_lat_lng', 	
						   'home_lat_lng_poly', 	
						   'image', 	
						   'colortheme'];	
	
    public function user()
    {
        return $this->belongsTo('App\User');
    }		
}
