<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillingAddress extends Model
{
    protected $fillable = ['user_id', 'address', 'postcode', 'city', 'state', 'country_id'];	
}
