<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class BlogPost extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;
    protected $fillable = ['title', 'content', 'status', 'blogtype', 'blogorder', 'image', 'tags', 'slug'];	
	
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }	
	
    public function getExcerpt($str, $startPos=0, $maxLength=100) 
	{
		$str = strip_tags($str);
		if(strlen($str) > $maxLength) {
			$excerpt   = substr($str, $startPos, $maxLength-3);
			$lastSpace = strrpos($excerpt, ' ');
			$excerpt   = substr($excerpt, 0, $lastSpace);
			$excerpt  .= '...';
		} else {
			$excerpt = $str;
		}

		return $excerpt;
	}
}
