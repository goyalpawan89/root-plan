<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Location extends Model
{
    protected $fillable = ['user_id', 
						   'location_id', 
						   'name', 
						   'email', 
						   'phone', 
						   'address', 
						   'gatecode',
						   'defaultvehicle',
						   'check_in_time',
						   'status',
						   'latitude',
						   'longitude',
						   'notes',
						   'details'];	
	
    public function user()
    {
        return $this->belongsTo('App\User');
    }		
	
    public function defaultaddress()
    {
        return $this->hasOne('App\Models\LocationAddress')->where('is_default', 1);
    }		
	
    public function dayaddress()
    {
		$planDay = strtolower(Carbon::parse(request('planDate'))->format('l'));
        return $this->hasOne('App\Models\LocationAddress')->where($planDay, 1);
    }			
	
    public function addresses()
    {
        return $this->hasMany('App\Models\LocationAddress');
    }		
}
