<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Autorecharge extends Model
{
    protected $fillable = ['user_id', 'min_balance', 'amount', 'status'];	
}
