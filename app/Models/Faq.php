<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable = ['faqtype', 'question', 'answer', 'faqorder', 'status'];	
}
