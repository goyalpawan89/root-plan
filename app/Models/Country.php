<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['countryCode', 
						   'countryName', 
						   'currencyCode', 
						   'fipsCode', 
						   'isoNumeric',
						   'capital',
						   'isoAlpha3'];
}
